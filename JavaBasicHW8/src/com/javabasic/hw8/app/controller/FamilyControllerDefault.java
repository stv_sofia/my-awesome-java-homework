package com.javabasic.hw8.app.controller;

import com.javabasic.hw8.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw8.app.domain.family.Family;
import com.javabasic.hw8.app.domain.human.Human;
import com.javabasic.hw8.app.domain.pet.Pet;
import com.javabasic.hw8.app.service.FamilyServiceDefault;

import java.util.List;
import java.util.Map;

public class FamilyControllerDefault implements FamilyController{
    // - содержит поле(ссылку) FamilyServiceDefault для доступа к методам бизнес-логики приложения.
    private FamilyServiceDefault familyServiceDefault;

    public FamilyControllerDefault(FamilyServiceDefault familyServiceDefault){
        this.familyServiceDefault = familyServiceDefault;
    }

    // - Класс FamilyControllerDefault - должен иметь все те же методы, которые есть в FamilyControllerDefault.

    //1. Возвращает список всех семей List<Family>.
    public List<Family> getAllFamiliesList(){
        return familyServiceDefault.getAllFamiliesList();
    }


    //2. Выводит на экран все семьи (в индексированном списке) со всеми членами семьи.
    public void displayAllFamilies(){
        familyServiceDefault.displayAllFamilies();
    }


    //3. Находит семью по указанному индексу, - возвращает семью по указанному индексу,
    // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.
    public Family getFamilyByIndex(int index) {
        return familyServiceDefault.getFamilyByIndex(index);
    }


    //4. Удаляет семью с заданным индексом,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    public boolean deleteFamilyByIndex(int index) {
        return familyServiceDefault.deleteFamilyByIndex(index);
    }


    //5. Удаляет семью если такая существует в списке,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    public boolean deleteFamily(Family family) {
        return  familyServiceDefault.deleteFamily(family);
    }


    //6. Обновляет существующую семью в БД если такая уже существует,
    // - сохраняет семью в конец списка - если семьи в БД нет.
    public void saveFamily(Family family) {
        familyServiceDefault.saveFamily(family);
    }



    //7. Возвращает количество семей в БД.
    public int count(){
        return familyServiceDefault.count();
    }

    //8.  Находит семьи с количеством людей больше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей больше чем указанное
    public List<Family> getFamiliesBiggerThan(int members) {
        return familyServiceDefault.getFamiliesBiggerThan(members);
    }


    //9.  Находит семьи с количеством людей меньше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей меньше чем указанное
    public List<Family> getFamiliesLessThan(int members) {
        return familyServiceDefault.getFamiliesLessThan(members);
    }


    //10.  Подсчитывает число семей с количеством людей равное переданному числу.
    public int countFamiliesWithMemberNumber(int members){
        return familyServiceDefault.countFamiliesWithMemberNumber(members);
    }


    //11.  Создает новую семью (принимает 2 параметра типа Human) - создает новую семью, - сохраняет в БД.
    public void createNewFamily(Human woman, Human man){
        familyServiceDefault.createNewFamily(woman, man);
    }


    //12. Создает ребенка семьей (принимает Family и 2 типа String: мужское и женское имя)
    //  - в данной семье появляется новый ребенок с учетом данных родителей,
    //  - если рожденный ребенок мальчик - ему присваивается мужское имя, если девочка - женское,
    //  - информация о семье обновляется в БД, - метод возвращает обновленную семью.
    public Family bornChild(Family family, String womanName, String manName){
        return familyServiceDefault.bornChild(family, womanName, manName);
    }


    //13. Усыновляет ребенка (принимает 2 параметра: Family, Human) - метод возвращает обновленную семью,
    //  - в данной семье сохраняется данный ребенок, - информация о семье обновляется в БД.
    public Family adoptChild(Family family, Human child) {
        return familyServiceDefault.adoptChild(family, child);
    }

    //14. Удаляет детей старше чем (принимает int)
    // - во всех семьях удаляются дети, которые старше указанно возраста, - информация обновляется в БД.
    public void deleteAllChildrenOlderThen(int yearsOld){
        familyServiceDefault.deleteAllChildrenOlderThen(yearsOld);
    }


    //15.  Возвращает список домашних животных, которые живут в семье (принимает индекс семьи),
    public PetsTreeSet getPets(int index){
        return familyServiceDefault.getPets(index);
    }


    //16. Добавляет питомца в семью (принимает индекс семьи и параметр Pet)
    // - добавляет нового питомца в семью,  - обновляет данные в БД.
    public boolean addPet(int index, Pet pet){
        return familyServiceDefault.addPet(index, pet);
    }


    //17. Удаляет питомца (принимает индекс семьи и параметр Pet) - удаляет питомца из семьи,  - обновляет данные в БД.
    public boolean deletePet(int index, Pet pet){
        return familyServiceDefault.deletePet(index, pet);
    }


}