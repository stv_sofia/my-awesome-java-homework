package com.javabasic.hw8.app.service;

import com.javabasic.hw8.app.dao.CollectionFamilyDao;
import com.javabasic.hw8.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw8.app.domain.family.Family;
import com.javabasic.hw8.app.domain.human.Human;
import com.javabasic.hw8.app.domain.pet.Pet;

import java.util.List;


public class FamilyServiceDefault implements FamilyService{
    private CollectionFamilyDao familyDao;

    public FamilyServiceDefault(CollectionFamilyDao familyDao) {
        this.familyDao = familyDao;            // FamilyServiceDefault familyDao содержит ссылку на FamilyDaoCollection (источник данных)
    }


    //1. Возвращает список всех семей List<Family>.
    @Override
    public List<Family> getAllFamiliesList(){
        return familyDao.getAllFamiliesList();
    }


    //2. Выводит на экран все семьи (в индексированном списке) со всеми членами семьи.
    @Override
    public void displayAllFamilies(){
        familyDao.displayAllFamilies();
    }


    //3. Находит семью по указанному индексу, - возвращает семью по указанному индексу,
    // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.
    @Override
    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }


    //4. Удаляет семью с заданным индексом,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamilyByIndex(int index) {
       return familyDao.deleteFamilyByIndex(index);
    }


    //5. Удаляет семью если такая существует в списке,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamily(Family family) {
        return  familyDao.deleteFamily(family);
    }


    //6. Обновляет существующую семью в БД если такая уже существует,
    // - сохраняет семью в конец списка - если семьи в БД нет.
    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }



    //7. Возвращает количество семей в БД.
    public int count(){
        return familyDao.count();
    }


    //8.  Находит семьи с количеством людей больше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей больше чем указанное
    public List<Family> getFamiliesBiggerThan(int members) {
        return familyDao.getFamiliesBiggerThan(members);
    }


    //9.  Находит семьи с количеством людей меньше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей меньше чем указанное
    public List<Family> getFamiliesLessThan(int members) {
        return familyDao.getFamiliesLessThan(members);
    }


    //10.  Подсчитывает число семей с количеством людей равное переданному числу.
    public int countFamiliesWithMemberNumber(int members){
        return familyDao.countFamiliesWithMemberNumber(members);
    }


    //11.  Создает новую семью (принимает 2 параметра типа Human) - создает новую семью, - сохраняет в БД.
    public void createNewFamily(Human woman, Human man){
        familyDao.createNewFamily(woman, man);
    }


    //12. Создает ребенка семьей (принимает Family и 2 типа String: мужское и женское имя)
    //  - в данной семье появляется новый ребенок с учетом данных родителей,
    //  - если рожденный ребенок мальчик - ему присваивается мужское имя, если девочка - женское,
    //  - информация о семье обновляется в БД, - метод возвращает обновленную семью.
    public Family bornChild(Family family, String womanName, String manName){
        return familyDao.bornChild(family, womanName, manName);
    }


    //13. Усыновляет ребенка (принимает 2 параметра: Family, Human) - метод возвращает обновленную семью,
    //  - в данной семье сохраняется данный ребенок, - информация о семье обновляется в БД.
    public Family adoptChild(Family family, Human child) {
        return familyDao.adoptChild(family, child);
    }


    //14. Удаляет детей старше чем (принимает int)
    // - во всех семьях удаляются дети, которые старше указанно возраста, - информация обновляется в БД.
    public void deleteAllChildrenOlderThen(int yearsOld){
        familyDao.deleteAllChildrenOlderThen(yearsOld);
    }


    //15.  Возвращает список домашних животных, которые живут в семье (принимает индекс семьи),
    public PetsTreeSet getPets(int index){
        return familyDao.getPets(index);
    }


    //16. Добавляет питомца в семью (принимает индекс семьи и параметр Pet)
    // - добавляет нового питомца в семью,  - обновляет данные в БД.
    public boolean addPet(int index, Pet pet){
        return familyDao.addPet(index, pet);
    }


    //17. Удаляет питомца (принимает индекс семьи и параметр Pet) - удаляет питомца из семьи,  - обновляет данные в БД.
    public boolean deletePet(int index, Pet pet){
        return familyDao.deletePet(index, pet);
    }


}