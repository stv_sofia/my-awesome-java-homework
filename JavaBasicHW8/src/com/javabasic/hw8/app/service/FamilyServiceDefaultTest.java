package com.javabasic.hw8.app.service;

import com.javabasic.hw8.app.dao.CollectionFamilyDao;
import com.javabasic.hw8.app.domain.family.Family;
import com.javabasic.hw8.app.domain.human.Man;
import com.javabasic.hw8.app.domain.human.Woman;
import com.javabasic.hw8.app.domain.pet.Cat;
import com.javabasic.hw8.app.domain.pet.Dog;
import com.javabasic.hw8.app.domain.pet.Fish;
import com.javabasic.hw8.app.domain.pet.RoboCat;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyServiceDefaultTest {
    private FamilyServiceDefault familyServiceModule;
    private List<Family> familiesModule;
    private Family newFamily1, newFamily2, newFamily3, newFamily4, newFamily5;

    @Before
    public void setUp() throws Exception {
        familyServiceModule = new FamilyServiceDefault(new CollectionFamilyDao());
        familiesModule = new ArrayList<>();
        newFamily1 = new Family(new Woman("Eve", "Firstwoman", 1965),
                new Man("Adam", "Firstman",1960));

        newFamily2 = new Family(new Woman("Rowan", "Secondwoman", 1967),
                new Man("Cain", "Secondman",1962));

        newFamily3 = new Family(new Woman("Adel", "Thirdwoman", 1969),
                new Man("Brad", "Thirdtman",1964));

        newFamily4 = new Family(new Woman("Bella", "Forthwoman", 1963),
                new Man("Fred", "Forthman",1961));

        newFamily5 = new Family(new Woman("Leyla", "Fifthwoman", 1964),
                new Man("John", "Fifthman",1963));

        familyServiceModule.saveFamily(newFamily1);
        familyServiceModule.saveFamily(newFamily2);
        familyServiceModule.saveFamily(newFamily3);
        familyServiceModule.saveFamily(newFamily4);
        familyServiceModule.saveFamily(newFamily5);
    }


//  test Check Family getAllFamiliesList() - should Return List Of All Families
    @Test
    public void testCheckGetAllFamiliesListSuccess() {
        List<Family> expected =  new ArrayList<>();
        expected.add(newFamily1);
        expected.add(newFamily2);
        expected.add(newFamily3);
        expected.add(newFamily4);
        expected.add(newFamily5);
        List<Family> actual = familyServiceModule.getAllFamiliesList();
        assertEquals(expected, actual);
    }

    @Test
    public void displayAllFamilies() {
    }


//  test Check Family getFamilyByIndex() - should Return newFamily1
    @Test
    public void testCheckGetFamilyByIndexSuccess() {
        Family actual = familyServiceModule.getFamilyByIndex(0);
        assertEquals(newFamily1, actual);
    }
    //  should Return null
    @Test
    public void testCheckGetFamilyByIndexWrongIndexSuccess() {
        Family familyExpected = familyServiceModule.getFamilyByIndex(10);
        assertNull(familyExpected);
    }


//  test Check Family deleteFamilyByIndex(int ind) - should Remove Family By Index
    @Test
    public void testCheckDeleteFamilyByIndexSuccess() {
        familyServiceModule.deleteFamilyByIndex(0);
        int actual = familyServiceModule.getAllFamiliesList().indexOf(newFamily1);
        assertEquals(-1, actual);
    }
    // - should Return False
    @Test
    public void testCheckDeleteFamilyByIndexWrongIndexSuccess() {
        boolean actual = familyServiceModule.deleteFamilyByIndex(10);
        assertFalse(actual);
    }



//  test Check Family deleteFamily(Family fam) - should Return True if Deleted
    @Test
    public void testCheckDeleteFamilySuccess() {
        boolean actual = familyServiceModule.deleteFamily(newFamily1);
        assertTrue(actual);
    }
    //  - should Return False if Not Deleted
    @Test
    public void testCheckDeleteFamilyNoSuchFamilySuccess() {
        familyServiceModule.deleteFamily(newFamily1);
        boolean actual = familyServiceModule.deleteFamily(newFamily1);
        assertFalse(actual);
    }


//  test Check Family saveFamily(Family fam) - should Update Family if In the List
    @Test
    public void testCheckSaveFamilyUpdateSuccess() {
        newFamily1.getMother().setSchedule();
        familyServiceModule.saveFamily(newFamily1);
        Family actualFamily = familyServiceModule.getFamilyByIndex(0);
        assertEquals(newFamily1, actualFamily);
    }
    //  - should Add Family if Not In the List, to The End of the List
    @Test
    public void testCheckSaveFamilyAddNewToTheEndSuccess() {
        familyServiceModule.deleteFamily(newFamily1);
        familyServiceModule.saveFamily(newFamily1);
        int actualIndex = familyServiceModule.getAllFamiliesList().indexOf(newFamily1);
        assertEquals(4, actualIndex);
    }


//  test Check Family count(count) - should Return Number of Families In the List
    @Test
    public void testCheckCountSuccess() {
        int actualNumberOfFamily = familyServiceModule.count();
        assertEquals(5, actualNumberOfFamily);
    }


//  test Check Family getFamiliesBiggerThan(int n) - should Return List of Families With Members Bigger Then n
    @Test
    public void testCheckGetFamiliesBiggerThanSuccess() {
        familyServiceModule.bornChild(newFamily1, "Michaella", "Michel");
        familyServiceModule.bornChild(newFamily1, "Ivanna", "Ivan");
        familyServiceModule.bornChild(newFamily2, "Alexandra", "Alexandr");
        int actualNumbersFamiliesMoreThen2 = familyServiceModule.getFamiliesBiggerThan(2).size();
        assertEquals(2, actualNumbersFamiliesMoreThen2);
    }


//  test Check Family getFamiliesLessThan(int n) - should Return List of Families With Members Less Then n
    @Test
    public void testCheckGetFamiliesLessThanSuccess() {
        familyServiceModule.bornChild(newFamily1, "Michaella", "Michel");
        familyServiceModule.bornChild(newFamily1, "Ivanna", "Ivan");
        int actualNumbersFamiliesMoreThen2 = familyServiceModule.getFamiliesLessThan(3).size();
        assertEquals(4, actualNumbersFamiliesMoreThen2);
    }


//  test Check Family countFamiliesWithMemberNumber(int n) - should Count Families With Members equals to n
    @Test
    public void testCheckCountFamiliesWithMemberNumberSuccess() {
        familyServiceModule.bornChild(newFamily1, "Michaella", "Michel");
        familyServiceModule.bornChild(newFamily1, "Ivanna", "Ivan");
        int actualNumbersFamiliesMoreThen2 = familyServiceModule.countFamiliesWithMemberNumber(4);
        assertEquals(1, actualNumbersFamiliesMoreThen2);
    }


//  test Check Family createNewFamily() - should Create New Family
    @Test
    public void testCheckCountCreateNewFamilySuccess() {
        familyServiceModule.deleteFamily(newFamily1);
        newFamily1.getFather().setIqLevel(100);
        newFamily1.getMother().setIqLevel(100);
        familyServiceModule.createNewFamily(new Woman("Eve", "Firstwoman", 1965),
                new Man("Adam", "Firstman",1960));
        Family actual = familyServiceModule.getFamilyByIndex(4);
        actual.getFather().setIqLevel(100);
        actual.getMother().setIqLevel(100);
        assertEquals(newFamily1, actual);
    }


//  test Check Family bornChild() - should Born New Child in the Family
    @Test
    public void testCheckCountBornChildSuccess() {
        familyServiceModule.bornChild(newFamily1, "Michaella", "Michel");
        int actual = familyServiceModule.getFamilyByIndex(0).countFamilyMembers();
        assertEquals(3, actual);
    }


//  test Check Family adoptChild() - should Adopt New Child in the Family And Change Surname for the Child
    @Test
    public void testCheckAdoptChildSuccess() {
        familyServiceModule.adoptChild(newFamily1, new Woman("Ivanna", "Ivanova", 2010));
        int actual = familyServiceModule.getFamilyByIndex(0).countFamilyMembers();
        assertEquals(3, actual);
        String actualSurname = familyServiceModule.getFamilyByIndex(0).getChildrenList().getChildrenList().get(0).getSurname();
        assertEquals(familyServiceModule.getFamilyByIndex(0).getFather().getSurname(), actualSurname);
    }


//  test Check Family deleteAllChildrenOlderThen(int n) - should Delete All Children OlderThan 20
    @Test
    public void testCheckDeleteAllChildrenOlderThenSuccess() {
        familyServiceModule.bornChild(newFamily1, "Michaella", "Michel");
        familyServiceModule.getFamilyByIndex(0).getChildrenList().getChildrenList().get(0).setYearBirth(1998);
        familyServiceModule.bornChild(newFamily1, "Ivanna", "Ivan");
        familyServiceModule.getFamilyByIndex(0).getChildrenList().getChildrenList().get(0).setYearBirth(1999);
        familyServiceModule.bornChild(newFamily1, "Alla", "Alan");
        familyServiceModule.getFamilyByIndex(0).getChildrenList().getChildrenList().get(0).setYearBirth(2009);
        familyServiceModule.bornChild(newFamily2, "Alexandra", "Alexandr");
        familyServiceModule.getFamilyByIndex(1).getChildrenList().getChildrenList().get(0).setYearBirth(2010);

        familyServiceModule.deleteAllChildrenOlderThen(20);
        assertEquals(2, familyServiceModule.countFamiliesWithMemberNumber(3));
    }


//  test Check Family getPets() - should Return Pets Set of The Family (if no pets - null)
    @Test
    public void testCheckGetPetsNoPetsInFamilySuccess() {
        assertNull(familyServiceModule.getPets(0));
    }
    // - should Return Pets Set of The Family
    @Test
    public void testCheckGetPetsFamilyHasPetsSuccess() {
        familyServiceModule.addPet(4, new Dog("Rocky", 3));
        familyServiceModule.addPet(4, new Cat("Tom", 2));
        familyServiceModule.addPet(4, new RoboCat("Robo", 10));
        familyServiceModule.addPet(4, new Fish("Nemo", 1));
        assertEquals(4, familyServiceModule.getPets(4).getPetsTreeSet().size());

    }

    
//  test Check Family addPet() - should Return Pets Set of The Family (if no pets - null)
    @Test
    public void testCheckAddPetSuccess() {
        assertTrue(familyServiceModule.addPet(0, new Fish("Nemo", 1)));
        assertEquals(1, familyServiceModule.getPets(0).getPetsTreeSet().size());
    }

}