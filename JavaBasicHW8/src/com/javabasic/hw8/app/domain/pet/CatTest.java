package com.javabasic.hw8.app.domain.pet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CatTest {
    private Cat module;

    @Before
    public void setUp() throws Exception {
        module = new Cat("Tomas");
        module.setTrickLevel(95);
    }


//  test Check Cat() no initialize
    @Test
    public void testCheckCatEmptyGenderWomanSuccess(){
        Cat module = new Cat();
        assertEquals("domestic cat", module.getSpecies().species);
    }


//  test Check Cat ToString()
    @Test
    public void testCheckCatToStringSuccess() {
        assertEquals("\n\t\tPet{species='domestic cat', nickname='Tomas', " +
                "can fly='false', number of legs='4', has fur='true', " +
                "age='no info', trickLevel='95', habits='no info'}", module.toString());
    }


//  test Check Cat Equals()
    @Test
    public void testCheckCatEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckCatNotEqualsSuccess() {
        assertNotEquals(new Cat("Cat"), module);
    }


//  test Check Cat HashCode()
    @Test
    public void testCheckCatHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckCatHashCodeNotEqualsSuccess() {
        assertNotEquals(new Cat().hashCode(), module.hashCode());
    }


//  test Check Cat (Pet) abstract methods petResponding()
    @Test
    public void testCheckCatPetRespondingSuccess(){
        assertEquals("Hello, master! I am your domestic cat Tomas. And I miss you so!", module.petResponding());
    }


//  test Check Cat implement (PetFoulable) abstract methods petDidFoul()
    @Test
    public void testCheckCatPetDidFoulSuccess(){
        assertEquals("I am domestic cat Tomas. I did foul - I dug all the plants)) Needs to cover tracks well...",
                module.petDidFoul());
    }
}