package com.javabasic.hw8.app.domain.pet;

public abstract interface PetFoulable {
    public abstract String petDidFoul();
}
