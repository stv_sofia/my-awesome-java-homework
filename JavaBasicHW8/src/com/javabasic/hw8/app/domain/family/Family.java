package com.javabasic.hw8.app.domain.family;

import com.javabasic.hw8.app.domain.human.Human;
import com.javabasic.hw8.app.domain.human.Woman;
import com.javabasic.hw8.app.domain.human.Man;
import com.javabasic.hw8.app.domain.collectionChildren.HumanChildrenList;
import com.javabasic.hw8.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw8.app.domain.pet.Pet;

import java.util.Objects;

public class Family implements HumanCreator {
    private static final int MEMBERS_STATIC = 2;
    private Woman mother;
    private Man father;
    private int membersTotal;

    private HumanChildrenList childrenList;
    private PetsTreeSet petsSet;

//  Constructors
public Family(Woman mother, Man father){
        this.mother = mother;
        this.father = father;
        this.childrenList = null;
        this.membersTotal = Family.MEMBERS_STATIC;
        // добавляем ссылку для mother(firstWoman) и для father(firstMan) на их семью(на эту семью)
        mother.setFamily(this);
        father.setFamily(this);
        // изменяем фамилию женщины на фамилию мужчины
        mother.setSurname(father.getSurname());
    }

//  Getters & Setters
    public Woman getMother() { return mother; }
    public void setMother(Woman mother) { this.mother = mother; }

    public Man getFather() { return father; }
    public void setFather(Man father) { this.father = father; }

    public HumanChildrenList getChildrenList() { return childrenList; }
    public void setChildrenList(HumanChildrenList childrenList) { this.childrenList = childrenList; }

    public PetsTreeSet getPetsSet() { return petsSet; }
    public void setPetsSet(PetsTreeSet petsSet) { this.petsSet = petsSet; }

    public int getMembersTotal() { return Family.MEMBERS_STATIC  +
                                    (this.childrenList == null ? 0 : this.childrenList.getChildrenList().size()); }
    public void setMembersTotal() { this.membersTotal = Family.MEMBERS_STATIC +
                                    (this.childrenList == null ? 0 : this.childrenList.getChildrenList().size()); }


//  @Override method toString, equals & hashCode
    @Override
    public String toString() {
        return "\nFamily{" +
                "\n\tmother = " + this.mother.toString() + "," +
                "\n\tfather = " + this.father.toString() + "," +
                "\n\tchildren = " + (this.childrenList == null ? "'no children'" : this.childrenList.getChildrenList()) + "," +
                "\n\tpets = " + (this.petsSet == null ? "'no pet'" : this.petsSet.getPetsTreeSet()) + "," +
                "\n\ttotal human members of family = '" + membersTotal + '\'' +
                "\n}";
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Family family = (Family) obj;
        return this.mother.equals(family.mother) && this.father.equals(family.father)
                && this.membersTotal == family.membersTotal;
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(this.mother, this.father, this.membersTotal);
        return result;
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family obj before Garbage Collector will delete it : " + this);
    }


//  @Override HumanCreator implement, method generateGender() и bornChild()
    @Override
    public int generateGender(){ return (int) (Math.random() * 101); }
    @Override
    public Human bornChild(int randGender){
        String[] womanNames = {"Adel", "Bella", "Rose", "Helen", "Rowan"};
        String[] manNames = {"Cain", "Albert", "Bill", "Vlad", "Mark"};
        int yearBirth = (int) (1980 + (Math.random() * 21));
        int indName = (int) (Math.random() * 5);
        return randGender <= 50 ?
                new Woman (womanNames[indName], this.getFather().getSurname(), yearBirth,
                        (this.getFather().getIqLevel()+this.getMother().getIqLevel())/2)
                : new Man (manNames[indName], this.getFather().getSurname(), yearBirth,
                          (this.getFather().getIqLevel()+this.getMother().getIqLevel())/2);
    }



//  Family methods addChildToList()
    public boolean addChildToList(Human child){
        if(this.getChildrenList() == null){
            this.childrenList = new HumanChildrenList();
            this.mother.setChildrenList(this.childrenList);
            this.father.setChildrenList(this.childrenList);
        }
        boolean result = this.childrenList.addChildToHumanChildrenList(child);
        child.setFamily(this);
        this.membersTotal = this.countFamilyMembers();
        return result;
    }


//  Family methods deleteChildFromList(int index)
//  Если удалиться, то объект вернется, п э обнуляем ему ссылку на семью
    public Human deleteChildFromList(int ind){
        if(this.getChildrenList() == null || this.getChildrenList().getChildrenList().size() == 0){
            System.out.println("Family has no children!");
            return null;
        }
        if(ind > this.childrenList.getChildrenList().size() || ind <= 0){
            System.out.println("There is no such index of child! " +
                    "Family has only " + this.childrenList.getChildrenList().size() + " children!");
            return null;
        }

        Woman childWomanLeftFamily = new Woman();
        Man childManLeftFamily = new Man();
        String gender = this.childrenList.getChildrenList().get(ind-1).getGender().gender;

        if(gender.equals("woman")){
            childWomanLeftFamily = (Woman) this.childrenList.deleteChildFromHumanChildrenList(ind);
            childWomanLeftFamily.setFamily(null);

            System.out.printf("Child %s has left the family of %s %s!", childWomanLeftFamily.getName(),
                    this.getFather().getName(), this.getFather().getSurname());
            this.membersTotal = this.countFamilyMembers();
            return childWomanLeftFamily;
        } else {
            childManLeftFamily = (Man) this.childrenList.deleteChildFromHumanChildrenList(ind);
            childManLeftFamily.setFamily(null);

            System.out.printf("Child %s has left the family of %s %s!", childManLeftFamily.getName(),
                    this.getFather().getName(), this.getFather().getSurname());
            this.membersTotal = this.countFamilyMembers();
            return childManLeftFamily;
        }
    }


//  Family methods deleteChildFromList(Human child)
//  Если удалиться, то объект вернется, п э обнуляем ему ссылку на семью
    public Human deleteChildFromList(Human child){
        if(this.getChildrenList() == null || this.getChildrenList().getChildrenList().size() == 0){
            System.out.println("Family has no children!");
            return null;
        }
        if(!this.childrenList.getChildrenList().contains(child)) {
            System.out.println("There is no such object child in the family!");
            return null;
        }

        Woman childWomanLeftFamily = new Woman();
        Man childManLeftFamily = new Man();
        String gender = child.getGender().gender;

        if(gender.equals("woman")){
            childWomanLeftFamily = (Woman) this.childrenList.deleteChildFromHumanChildrenList(child);
            childWomanLeftFamily.setFamily(null);

            System.out.printf("Child %s has left the family of %s %s!", childWomanLeftFamily.getName(),
                    this.getFather().getName(), this.getFather().getSurname());
            this.membersTotal = this.countFamilyMembers();
            return childWomanLeftFamily;
        } else {
            childManLeftFamily = (Man) this.childrenList.deleteChildFromHumanChildrenList(child);
            childManLeftFamily.setFamily(null);

            System.out.printf("Child %s has left the family of %s %s!", childManLeftFamily.getName(),
                    this.getFather().getName(), this.getFather().getSurname());
            this.membersTotal = this.countFamilyMembers();
            return childManLeftFamily;
        }
    }


//  Family methods deleteChildFromList(String name)
//  Если удалиться, то объект не вернется, п э обнулить ему ссылку на семью НУ НИКАК))
    public boolean deleteChildFromList(String name){
        if(this.getChildrenList() == null){
            System.out.println("Family has no children yet!");
            return false;
        }
        boolean result = this.childrenList.deleteChildFromHumanChildrenList(name);
        if(this.childrenList.getChildrenList().size() == 0){
            this.setChildrenList(null);
            this.getMother().setChildrenList(null);
            this.getFather().setChildrenList(null);
        }
        this.membersTotal = this.countFamilyMembers();
        return result;
    }



//  Family method addPet()
    public boolean addPet(Pet pet){
        if(this.getPetsSet() == null){
            this.petsSet = new PetsTreeSet();
            this.father.setPetsSet(this.petsSet);
            this.mother.setPetsSet(this.petsSet);
            if(this.childrenList != null){
                for (Human el: this.childrenList.getChildrenList()) {
                    el.setPetsSet(this.petsSet);
                }
            }
        }
        pet.setFamily(this);
        return this.petsSet.addPetToPetsTreeSet(pet);
    }


//  method deletePet(Pet pet)
    public boolean deletePet(Pet pet){
        if(this.getPetsSet() != null){
            boolean result = this.petsSet.removePetFromPetsTreeSet(pet);
            if(this.getPetsSet().getPetsTreeSet().size() == 0){
                this.petsSet = null;
                this.father.setPetsSet(null);
                this.mother.setPetsSet(null);
            }
            pet.setFamily(null);
            return result;
        }
        return false;
    }


//  Family method countFamilyMembers()
    public int countFamilyMembers(){ return this.getMembersTotal(); }
}