package com.javabasic.hw8.app.domain.human;

import java.util.Objects;

import com.javabasic.hw8.app.domain.collectionHumanSchedule.HumanScheduleTreeMap;
import com.javabasic.hw8.app.domain.family.Family;
import com.javabasic.hw8.app.domain.collectionChildren.HumanChildrenList;
import com.javabasic.hw8.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw8.app.domain.pet.Species;

public abstract class Human implements Comparable{
    private Gender gender;
    private String name;
    private String surname;
    private int yearBirth = -1;
    private int iqLevel = -1;

    private HumanScheduleTreeMap schedule;

    private Family family;
    private HumanChildrenList childrenList;
    private PetsTreeSet petsSet;

//  Constructors
    Human(){
        this.gender = Gender.UNKNOWN;
    }
    Human(Gender gender){
        this.gender = gender;
        this.iqLevel = generateIqLevel();
    }
    Human(Gender gender, String name, String surname) {
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.iqLevel = generateIqLevel();
    }
    Human(Gender gender, String name, String surname, int yearBirth){
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.yearBirth = yearBirth;
        this.iqLevel = generateIqLevel();;
    }
    Human(Gender gender, String name, String surname, int yearBirth, int iqLevel){
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.yearBirth = yearBirth;
        this.iqLevel = iqLevel;
    }

//  Getters & Setters
    public Gender getGender() { return gender;}
    public void setGender(Gender gender) { this.gender = gender;}

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getSurname() { return surname; }
    public void setSurname(String surname) { this.surname = surname; }

    public int getYearBirth() { return yearBirth; }
    public void setYearBirth(int yearBirth) { this.yearBirth = yearBirth; }

    public int getIqLevel() { return iqLevel; }
    public void setIqLevel(int iqLevel) { this.iqLevel = iqLevel; }

    public HumanScheduleTreeMap getSchedule() { return schedule; }
    public void setSchedule() { this.schedule = new HumanScheduleTreeMap(); }
//    public void setSchedule(HumanScheduleTreeMap schedule) { this.schedule = schedule; }

    public Family getFamily() { return family;}
    public void setFamily(Family family) { this.family = family;}

    public HumanChildrenList getChildrenList() { return childrenList; }
    public void setChildrenList(HumanChildrenList childrenList) { this.childrenList = childrenList; }

    public PetsTreeSet getPetsSet() { return petsSet; }
    public void setPetsSet(PetsTreeSet petsSet) { this.petsSet = petsSet;}


//  @Override method compareTo() (implement Comparable)
    @Override
    public int compareTo(Object obj) {
        Human human = (Human) obj;
        return this.getYearBirth() - human.getYearBirth();
    }


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        if(this.getGender().gender.equals("unknown")){
            return "\n\tHuman{gender='" + this.gender.gender + "'}";
        } else {
            return "\n\t\tHuman{gender='" + this.gender.gender +
                    ", name='" + (this.getName() == null ? "no info" : this.getName()) + '\'' +
                    ", surname='" + (this.getSurname() == null ? "no info" : this.getSurname()) + '\'' +
                    ", yearBirth='" + (this.getYearBirth() == -1 ? "no info" : this.getYearBirth()) + '\'' +
                    ", iqLevel='" + this.iqLevel + '\'' +
                    ", family=" + (this.getFamily() == null ?  "'no info'" : "'yes'") +
                    ", pets=" + (this.getPetsSet() == null ?  "'no info'" : "'yes'") +
                    ", schedule=" + (this.schedule == null ? "'no info'" : this.schedule.getHumanScheduleTreeMap()) +
                    "}";
        }
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human) obj;
        return getYearBirth() == human.getYearBirth() && getIqLevel() == human.getIqLevel()
                && getGender() == human.getGender() && Objects.equals(getName(), human.getName())
                && Objects.equals(getSurname(), human.getSurname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(getGender(), getName(), getSurname(), getYearBirth(), getIqLevel());
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human obj before Garbage Collector will delete it : " + this);
    }


//  Human abstract methods           |нужно переопределить по ТЗ, п э abstract methods|
    public abstract String greetPet(Species petSpecies, String nickName);


//  Human methods describePet() |в ТЗ не указано, но я переопределю в наследниках, проедположим, что чел описывает только своего питомца|
    public String describePet(Species species, String nickName){
        return "I am not sure I have pet or not... You want me to describe my pet? )))";
    }


//  Human methods feedPet()          |надо бы преропределить, проверить есть ли такой питомец, но в ТЗ не указано
//                                     и вдруг чел захочет покормить питомца друга или вообще бездомного|
    public boolean feedPet(boolean isTimeToEat, Species species, String nickName, int trickLevel){
        if(isTimeToEat){
            System.out.println("Hmm... Let's feed this " + species.species + " " + nickName + "!");
                return true;
            }
        int randTrickLevel = this.generateIqLevel();
        if (trickLevel > randTrickLevel){
            System.out.println("BUT! pet trick level=" + trickLevel + " > random trick level=" + randTrickLevel);
                System.out.println("It's so canning! Let's feed this " + species.species + " " + nickName + "!");
                return true;
            }
            System.out.println("I think " + nickName + " is not hungry yet!");
            return false;
    }


//  Human methods generateIqLevel()
    public int generateIqLevel(){ return (int) (Math.random() * 101); }

}