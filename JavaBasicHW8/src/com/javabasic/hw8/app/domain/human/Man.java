package com.javabasic.hw8.app.domain.human;

import com.javabasic.hw8.app.domain.pet.Pet;
import com.javabasic.hw8.app.domain.pet.Species;

import java.util.Objects;

public final class Man extends Human{

//  Constructors
    public Man(){
        super(Gender.MAN);
    }
    public Man(String name, String surname){
        super(Gender.MAN, name, surname);
    }
    public Man(String name, String surname, int yearBirth){
        super(Gender.MAN, name, surname, yearBirth);
    }
    public Man(String name, String surname, int yearBirth, int iqLevel){
        super(Gender.MAN, name, surname, yearBirth, iqLevel);
    }

//  Getters & Setters (super)

//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Man)) return false;
        if (!super.equals(obj)) return false;
        Man man = (Man) obj;
        return super.equals(man);
    }
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Man extends Human obj before Garbage Collector will delete it : " + this);
    }


//  @Override super (Human) abstract methods greetPet() |переопределю|
    @Override
    public String greetPet(Species petSpecies, String petNickName){
        if(this.getPetsSet() == null) {
            return "Have no pet yet..";
        }
        String species1 = "";
        String nickName1 = "";

        for (Pet el : this.getPetsSet().getPetsTreeSet()) {
            if(el.getSpecies().species.equals(petSpecies.species)
                    && el.getNickname().equals(petNickName)){
                species1 = petSpecies.species;
                nickName1 = petNickName;
                break;
            }
        }
        if(!Objects.equals(species1, "") && !Objects.equals(nickName1, "")){
            return "Hello, my dear " + species1 + " " + nickName1 +
                    "! I am your owner " + this.getName() + ". Are you hungry?";
        }
        return "I have no such pet " + petNickName + " to greet!";
    }


//  @Override super (Human) methods describePet() |переопределю, в ТЗ не указано, но проедположим, что чел описывает только своего питомца|
    @Override
    public String describePet(Species petSpecies, String petNickName){
        if(this.getPetsSet() == null) {
            return "Have no pet yet..";
        }
        String species2 = "";
        String nickName2 = "";
        String trickLevelStr = "";
        int petAge = -1;

        for (Pet el : this.getPetsSet().getPetsTreeSet()) {
            if(el.getSpecies().species.equals(petSpecies.species)
                    && el.getNickname().equals(petNickName)){
                species2 = el.getSpecies().species;
                nickName2 = el.getNickname();
                trickLevelStr = el.getTrickLevelString(el.getTrickLevel());
                petAge = el.getAge();
                break;
            }
        }
        if(!Objects.equals(species2, "") && !Objects.equals(nickName2, "")) {
            return "I am " + this.getGender().gender + " " + this.getName() +
                    ". I have pet. It's a " + species2 + " " + nickName2 +". " +
                    "It's " + (petAge == -1 ? ".. I forget how many" : petAge) + " years old. And it's "
                    +  trickLevelStr + " :)";
        }
        return "You have no such pet " + petNickName + "!";
    }


//  super (Human) methods feedPet() |оставляю в родителе (Human), в ТЗ не указано переопределить|
//  public boolean feedPet(boolean isTimeToEat, Species species, String nickName, int trickLevel){};


//  only (Man)'s methods
    public String readTheBook(){
        return "It's time for me to read the book.";
    }

}