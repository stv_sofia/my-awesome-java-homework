package com.javabasic.hw8.app.domain.collectionHumanSchedule;

import com.javabasic.hw8.app.domain.human.DayOfWeek;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.TreeMap;

public class HumanScheduleTreeMap {
//  Collection Map->TreeMap
    private Map<DayOfWeek, LinkedHashSet<String>> humanScheduleTreeMap = new TreeMap<>();

//  Constructors
public HumanScheduleTreeMap(){
       for(DayOfWeek day : DayOfWeek.values()){
           this.humanScheduleTreeMap.put(day,new LinkedHashSet<>());
       }
    }

//  Getter
    public Map<DayOfWeek, LinkedHashSet<String>> getHumanScheduleTreeMap() {
        return humanScheduleTreeMap;
    }


//  method addTaskOfTheDay()
    public void addTaskOfTheDay(DayOfWeek day, String toDo){
        this.humanScheduleTreeMap.get(day).add(toDo);
    }


//  method removeTaskOfTheDay()
    public boolean removeTaskOfTheDay(DayOfWeek day, String toDo){
            return this.humanScheduleTreeMap.get(day).remove(toDo);
    }
}