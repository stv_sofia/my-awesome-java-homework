package com.javabasic.hw8.app;

import com.javabasic.hw8.app.dao.CollectionFamilyDao;
import com.javabasic.hw8.app.service.FamilyServiceDefault;
import com.javabasic.hw8.app.controller.FamilyControllerDefault;
import com.javabasic.hw8.app.domain.family.Family;
import com.javabasic.hw8.app.domain.human.Man;
import com.javabasic.hw8.app.domain.human.Woman;
import com.javabasic.hw8.app.domain.pet.*;

public class App {
    public static void main(String[] args) {
        CollectionFamilyDao familyDao = new CollectionFamilyDao();                   //  что бы использовать логику - в main создается экзкмпляр FamilyServiceDefault  и передается туда ссылка на реализацию familyDao (напр CollectionFamilyDao)
        FamilyServiceDefault familyService = new FamilyServiceDefault(familyDao);
        FamilyControllerDefault familyController = new FamilyControllerDefault(familyService);


//        создаем 5 семей
        System.out.println(">>> Checking createNewFamily(). Creating 5 families  >>>");
        Woman firstWoman = new Woman("Eve", "Firstwoman", 1965);
        Man firstMan = new Man("Adam", "Firstman",1960);
        familyController.createNewFamily(firstWoman, firstMan);

        familyController.createNewFamily(new Woman("Rowan", "Secondwoman", 1967),
                new Man("Cain", "Secondman",1962));

        familyController.createNewFamily(new Woman("Adel", "Thirdwoman", 1969),
                new Man("Brad", "Thirdtman",1964));

        familyController.createNewFamily(new Woman("Bella", "Forthwoman", 1963),
                new Man("Fred", "Forthman",1961));

        familyController.createNewFamily(new Woman("Leyla", "Fifthwoman", 1964),
                new Man("John", "Fifthman",1963));

        // выводим список семей
        familyController.displayAllFamilies();


        // получаем семью по индексу
        System.out.println("\n\n>>> Checking getFamilyByIndex(), deleteFamilyByIndex() >>>");
        System.out.println("\nGetting family with index 4: " + familyController.getFamilyByIndex(4));

        // удаляем семью по индексу
        System.out.println("\nIs family with index 4 was delete by it's index: " + familyController.deleteFamilyByIndex(4));
        System.out.println("Is family with index 10 was delete by it's index: " + familyController.deleteFamilyByIndex(10));

        // удаляем семью по экземпляру
        Family familyToDelete = familyController.getFamilyByIndex(0);
        System.out.println("Family with index 0 was delete by family's object: " + familyController.deleteFamily(familyToDelete));

        System.out.println("\nFamiliesList after deleting 2 of them (first & last): ");
        familyController.displayAllFamilies();



// смотрим saveFamily()
        System.out.println("\n\n>>> Checking saveFamily(). Adding for mother of family last in List an empty schedule >>>");
        // обновляем Family (last in List) с методом saveFamily()
        Family lastInListFamily = familyController.getFamilyByIndex(familyController.getAllFamiliesList().size()-1);
        lastInListFamily.getMother().setSchedule();
        familyController.saveFamily(lastInListFamily);
        System.out.println("\nFamily (last in List) for now: " + familyController.getFamilyByIndex(2));

        // создаем новую семью с методом saveFamily()
        System.out.println("\nCreating new family with method saveFamily(). It will be added at the end of family's list >>> ");
        familyController.saveFamily(new Family(firstWoman, firstMan));
        System.out.println("\nFamily (last in List) for now: " + familyController.getFamilyByIndex(3));

       // создаем еще одну новую семью с методом saveFamily()
        System.out.println("\nCreating one more new family with method saveFamily(). It will be added at the end of family's list >>> ");
        familyController.saveFamily(new Family(new Woman("Leyla", "Fifthwoman", 1964),
                new Man("John", "Fifthman",1963)));
        System.out.println("\nFamily (last in List) for now: " + familyController.getFamilyByIndex(4));



//   смотрим метод count()
        // выводим список семей
        System.out.println("\n\nFamilies for now: ");
        familyController.displayAllFamilies();
        // считаем их кол-во
        System.out.println("\nCount families in list: " + familyController.count());



//  добавляем детей в первую семью в списке - 1-го родили, в посленюю семью в списке - 2-х родили + 1-го усыновили
        System.out.println("\n\n>>> Checking bornChild(), adoptChild  >>>");

        Family firstInListFamily = familyController.getFamilyByIndex(0);
        familyController.bornChild(firstInListFamily, "Michaella", "Michel");

        lastInListFamily = familyController.getFamilyByIndex(familyController.getAllFamiliesList().size()-1);
        familyController.bornChild(lastInListFamily, "Ivanna", "Ivan");
        familyController.bornChild(lastInListFamily, "Alexandra", "Alexandr");
        Woman childW = new Woman("Nikol", "SomeSurName", 2008);
        familyController.adoptChild(lastInListFamily, childW);

        System.out.println("\nFamilies after adding for family0 - 1 new born child, and for family4 - 2 new born kids & 1 adopt >>> ");
        familyController.displayAllFamilies();



// смотрим getFamiliesLessThan(), countFamiliesWithMemberNumber(), getFamiliesBiggerThan() >>>
        System.out.println("\n\n>>> Checking getFamiliesLessThan, countFamiliesWithMemberNumber(), getFamiliesBiggerThan() >>>");
        System.out.print("\nFamilies with no children: ");
        familyController.getFamiliesLessThan(3);

        System.out.println("\nCount families with total members 2: "
                + familyController.countFamiliesWithMemberNumber(2) + " families");

        System.out.print("\nFamilies with children: ");
        familyController.getFamiliesBiggerThan(2);


// смотрим deleteAllChildrenOlderThen(),
        System.out.print("\n\n>>> Checking deleteAllChildrenOlderThen(). Deleting children older 20 years >>> ");
        familyController.deleteAllChildrenOlderThen(20);
        System.out.println("\n\nFamilies after deleting children older 20 years >>> ");
        familyController.displayAllFamilies();



// смотрим getPets(), addPets(), deletePet() >>>
        System.out.println("\n\n>>> Checking getPets(), addPets(), deletePet() >>>");
        // получаем питомцев
        familyController.getPets(0);
        familyController.getPets(4);
        // добавляем питомцев
        familyController.addPet(4, new Dog("Rocky", 3));
        //одинаковых не добавит
        familyController.addPet(4, new Cat("Tom", 2));
        familyController.addPet(4, new Cat("Tom", 2));
        familyController.addPet(4, new RoboCat("Robo", 10));
        familyController.addPet(4, new Fish("Nemo", 1));

        System.out.println("\nFamily with index 4. Family's pets after adding 4 different pets: "
                + familyController.getPets(4).getPetsTreeSet());
        // удаляем питомцев
        System.out.print("\nDeleting cat Tom and dog Rocky from family with index 4 >>>");
        familyController.deletePet(4, new Cat("Tom"));
        Dog dogRocky = (Dog) familyController.getPets(4).findPet(Species.DOG, "Rocky");
        familyController.deletePet(4, dogRocky);
        System.out.println("\nFamily's pets after deleting cat Tom and dog Rocky: "
                + familyController.getPets(4).getPetsTreeSet());
        // удаляем питомца не из списка семьи
        System.out.println("\nCan we delete pet not in the family's petList: "
                + familyController.deletePet(4, new Dog("Best")));

    }
}
