package com.javabasic.hw9.app.domain.pet;

public abstract interface PetFoulable {
    public abstract String petDidFoul();
}
