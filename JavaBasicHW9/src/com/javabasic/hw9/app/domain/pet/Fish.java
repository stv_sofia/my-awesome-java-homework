package com.javabasic.hw9.app.domain.pet;

import java.util.Set;

public class Fish extends Pet {

//  Constructors
    public Fish(){
        super(Species.FISH);
    }
    public Fish(String nickname) {
        super(Species.FISH, nickname);
    }
    public Fish(String nickname, int age) {
        super(Species.FISH, nickname, age);
    }
    public Fish(String nickname, int age, Set<String> habitsSet) {
        super(Species.FISH, nickname, age, habitsSet);
    }

//  Getters & Setters (super)

//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Fish)) return false;
        if (!super.equals(obj)) return false;
        Fish fish = (Fish) obj;
        return super.equals(fish);
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }


//  @Override super (Pet) abstract methods petResponding() |переопределяю|
    @Override
    public String petResponding(){
        return "Hello, master! I am your " + this.getSpecies().species + " " + this.getNickname() +
                ". I need some oxygen!";
    }
}