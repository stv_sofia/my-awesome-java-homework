package com.javabasic.hw9.app.domain.pet;

import java.util.Set;

public class Dog extends Pet implements PetFoulable{

//  Constructors
    public Dog(){
        super(Species.DOG);
    }
    public Dog(String nickName) {
        super(Species.DOG, nickName);
    }
    public Dog(String nickname, int age) {
        super(Species.DOG, nickname, age);
    }
    public Dog(String nickname, int age, Set<String> habitsSet) {
        super(Species.DOG, nickname, age, habitsSet);
    }

//  Getters & Setters (super)

//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Dog)) return false;
        if (!super.equals(obj)) return false;
        Dog dog = (Dog) obj;
        return super.equals(dog);
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Dog extends Pet obj before Garbage Collector will delete it : " + this);
    }


//  @Override super (Pet) abstract methods petResponding() |переопределяю|
    @Override
    public String petResponding(){
        return "Hello, master! I am your " + this.getSpecies().species + " " + this.getNickname() +
                ". Let's play!";
    }


//  @Override implement (PetFoulable) abstract methods petDidFoul()
    @Override
    public String petDidFoul(){
        return "I am " + this.getSpecies().species + " " + this.getNickname() +
                ". I did foul - I ate the sofa)) Needs to cover tracks well...";
    }
}