package com.javabasic.hw9.app.domain.human;

import com.javabasic.hw9.app.domain.pet.Species;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {
    private Human module;
    @Before
    public void setUp() throws Exception {
        module = new Human() {
            @Override public String greetPet(Species species, String nickName){return "";}
        };
    }

//  test Check Human() no exemplar
    @Test
    public void testCheckHumanEmptyGenderUnknownSuccess(){
        assertEquals("unknown", module.getGender().gender);
    }


//  test Check Human ToString()
    @Test
    public void testCheckHumanToStringSuccess() {
        assertEquals("\n\tHuman{gender='unknown'}", module.toString());
    }


//  test Check Human Equals()
    @Test
    public void testCheckHumanEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckHumanEqualsNotEqualsSuccess() {
        Human module2 = new Human() {@Override public String greetPet(Species species, String nickName){return "";}};
        module2.setIqLevel(100);
        assertNotEquals(module, module2);
    }


//   test Check Human HashCode()
    @Test
    public void testCheckHumanHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(),module.hashCode());
    }
    @Test
    public void testCheckHumanHashCodeNotEqualsSuccess() {
        Human module2 = new Human() {@Override public String greetPet(Species species, String nickName){return "";}};
        module2.setIqLevel(100);
        assertNotEquals(module.hashCode(),module2.hashCode());
    }


//   test Check Human describePet()
    @Test
    public void testCheckHumanDescribePetSuccess(){
        assertEquals("I am not sure I have pet or not... You want me to describe my pet? )))",
                module.describePet(Species.DOG, "Bob"));
    }


//   test Check Human feedPet()
    @Test
    public void testCheckHumanFeedPetSuccess() {
        assertTrue(module.feedPet(true, Species.DOG, "Bob", 77));
    }
    @Test
    public void testCheckHumanFeedPetNoTimeSuccess() {
        assertFalse(module.feedPet(false, Species.DOG, "Bob", -1));
    }
    @Test
    public void testCheckHumanFeedPetNoTimeButTrickLevelBigSuccess() {
        assertTrue(module.feedPet(false, Species.DOG, "Bob", 100));
    }


//   test Check Human generateIqLevel()
    @Test
    public void generateIqLevel() {
        int randNum = (int) (Math.random() * 101);
        assertTrue(101 > randNum);
        assertTrue( 0 <= randNum);
    }
}