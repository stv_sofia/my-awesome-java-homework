package com.javabasic.hw9.app.domain.family;

import com.javabasic.hw9.app.domain.human.Gender;
import com.javabasic.hw9.app.domain.human.Man;
import com.javabasic.hw9.app.domain.human.Woman;
import com.javabasic.hw9.app.domain.pet.Dog;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyTest {
    private Family module;
    @Before
    public void setUp() throws Exception {
        module = new Family( new Woman("Eve", "SomeSurname"),
                new Man("Adam", "SomeSurname"));
        module.getMother().setIqLevel(100);
        module.getFather().setIqLevel(100);
    }


//   test Check Family ToString()
    @Test
    public void testCheckFamilyToStringSuccess() {
        assertEquals("\nFamily{\n" +
                "\tmother = \n" +
                "\t\tHuman{gender='woman, name='Eve', surname='SomeSurname', yearBirth='no info', iqLevel='100', family='yes', pets='no info', schedule='no info'},\n" +
                "\tfather = \n" +
                "\t\tHuman{gender='man, name='Adam', surname='SomeSurname', yearBirth='no info', iqLevel='100', family='yes', pets='no info', schedule='no info'},\n" +
                "\tchildren = 'no children',\n" +
                "\tpets = 'no pet',\n" +
                "\ttotal human members of family = '2'\n" +
                "}", module.toString());
    }


//   test Check Family Equals()
    @Test
    public void testCheckFamilyEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckFamilyEqualsNotEqualsSuccess() {
        module.getMother().setIqLevel(100);
        module.getFather().setIqLevel(100);
        Family someFamily = new Family(new Woman("Eve", "OtherSurname"),
                new Man("Adam", "OtherSurname"));
        assertNotEquals(someFamily, module);
        assertNotEquals(module, someFamily);
    }


//   test Check Family HashCode()
    @Test
    public void testCheckFamilyHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckFamilyHashCodeNotEqualsSuccess() {
        Family someFamily = new Family(new Woman("Eve", "OtherSurname"),
                new Man("Adam", "OtherSurname"));
        assertNotEquals(someFamily.hashCode(), module.hashCode());
    }



//  test @Override HumanCreator implement, method generateGender()
    @Test
    public void testCheckGenerateGender(){
        int randNum = (int) (Math.random() * 101);
        assertTrue(101 > randNum);
        assertTrue( 0 <= randNum);
    }


//  test @Override HumanCreator implement, method bornChild()
    @Test
    public void testCheckBornChildWomanSuccess(){
        int randGender = 50;
        String genderNewHuman = "";
        if(randGender <= 50){
            genderNewHuman = new Woman("Helen", "Some").getGender().gender;
        }
        assertEquals(Gender.WOMAN.gender, genderNewHuman);
    }
    @Test
    public void testCheckBornChildManSuccess(){
        int randGender = 51;
        String genderNewHuman = "";
        if(randGender > 50){
            genderNewHuman = new Man("John", "Some").getGender().gender;
        }
        assertEquals(Gender.MAN.gender, genderNewHuman);
    }



//   test Check Family method addChild(object)
    @Test
    public void testCheckFamilyAddChildManSuccess() {
        assertTrue(module.addChildToList(new Man("John", "SomeSurname")));
        assertNotNull( module.getChildrenList());
        assertEquals(1, module.getChildrenList().getChildrenList().size());
    }
    @Test
    public void testCheckFamilyAddChildWomanSuccess() {
        assertTrue(module.addChildToList(new Woman("Helen", "SomeSurname")));
        assertNotNull( module.getChildrenList());
        assertEquals(1, module.getChildrenList().getChildrenList().size());
    }


//   test Check Family method deleteChild(object)
    @Test
    public void testCheckFamilyDeleteChildSuccess() {
        Man child = new Man("John", "SomeSurname");
        module.addChildToList(child);
        assertEquals(module.deleteChildFromList(child), child);
    }
    @Test
    public void testCheckFamilyDeleteChildNotFoundSuccess() {
        Man child = new Man("John", "SomeSurname");
        module.addChildToList(child);
        assertNull(module.deleteChildFromList(new Woman("Sara", "SomeSurname")));
    }


//   test Check Family method deleteChild(int)
    @Test
    public void testCheckFamilyDeleteChildByIndexSuccess(){
        Man child = new Man("John", "SomeSurname");
        module.addChildToList(child);
        module.addChildToList(new Woman("Bella", "SomeSurname"));
        assertEquals(module.deleteChildFromList(1), child);
    }
    @Test
    public void testCheckFamilyDeleteChildByIndexNotFoundSuccess(){
        Man child = new Man("John", "SomeSurname");
        module.addChildToList(child);
        module.addChildToList(new Woman("Bella", "SomeSurname"));
        assertNull(module.deleteChildFromList(3));
        assertNull(module.deleteChildFromList(0));
    }


//   test Check Family method deleteChild(String name)
    @Test
    public void testCheckFamilyDeleteChildByNameSuccess(){
        Man child = new Man("John", "SomeSurname");
        module.addChildToList(child);
        assertTrue(module.deleteChildFromList("John"));
    }
    @Test
    public void testCheckFamilyDeleteChildByNameNoSuchChildSuccess(){
        Man child = new Man("John", "SomeSurname");
        module.addChildToList(child);
        assertFalse(module.deleteChildFromList("Bella"));
    }



//   test Check Family method addPet()
    @Test
    public void testCheckFamilyAddPetSuccess() {
        module.addPet(new Dog("Rocky"));
        assertNotNull(module.getPetsSet());
        assertTrue(module.addPet(new Dog("Bob")));
        assertTrue(module.getPetsSet().getPetsTreeSet().size() !=0);
    }


//   test Check Family method removePetFromPetsSet()
    @Test
    public void testCheckFamilyRemovePetFromPetsSetSuccess(){
        Dog dog = new Dog("Rocky");
        module.addPet(dog);
        assertTrue(module.deletePet(dog));
    }
    @Test
    public void testCheckFamilyRemovePetFromPetsSetButNoPetsInFamilySuccess(){
        Dog dog = new Dog("Rocky");
        assertFalse(module.deletePet(dog));
    }



//   test Check Family method countFamilyMembers()
    @Test
    public void testCheckFamilyCountFamilyMembersSuccess() {
        assertEquals(2, module.countFamilyMembers());
        module.addChildToList(new Man("John", "SomeSurname"));
        assertEquals(module.getMembersTotal(), module.countFamilyMembers());
        assertEquals(3, module.countFamilyMembers());
    }

}