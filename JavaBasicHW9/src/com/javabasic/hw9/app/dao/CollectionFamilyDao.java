package com.javabasic.hw9.app.dao;

import com.javabasic.hw9.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw9.app.domain.family.Family;
import com.javabasic.hw9.app.domain.human.Human;
import com.javabasic.hw9.app.domain.human.Man;
import com.javabasic.hw9.app.domain.human.Woman;
import com.javabasic.hw9.app.domain.pet.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

// Реализация FamilyDao в зависимости от того, где будут храниться данные - конкретно описываем, как мы будем работать с источником данных (List<Family>)
public class CollectionFamilyDao implements FamilyDao{
    private List<Family> families;

    public List<Family> getFamilyCollection() { return families; }
    public void setNewFamilyCollection() { this.families = new ArrayList<Family>(); }
    public void setFamilyCollection(List<Family> families) { this.families = families; }


    //1. Возвращает список всех семей List<Family>.
    @Override
    public List<Family> getAllFamiliesList(){
        if(this.families != null){
            return this.getFamilyCollection();
        }
        return null;
    }


    //2. Выводит на экран все семьи (в индексированном списке) со всеми членами семьи.
    @Override
    public void displayAllFamilies(){
        if(this.families != null){
            System.out.println("FamiliesList: [");
            for (Family el : this.getFamilyCollection()) {
                System.out.println("{index='" + this.getFamilyCollection().indexOf(el) + "', " + el + "}");
            }
            System.out.println("]");
        }
    }


    //3. Находит семью по указанному индексу, - возвращает семью по указанному индексу,
    // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.
    @Override
    public Family getFamilyByIndex(int index) {
        if(this.families != null) {
            if (index >= 0 && index < this.getFamilyCollection().size()) {
                return this.getFamilyCollection().get(index);
            }
        }
        return null;
    }


    //4. Удаляет семью с заданным индексом,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamilyByIndex(int index) {
        if(this.families != null) {
            if (index >= 0 && index < this.getFamilyCollection().size()) {
                return this.families.remove(this.getFamilyCollection().get(index));
            }
        }
        return false;
    }


    //5. Удаляет семью если такая существует в списке,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamily(Family family) {
         if(this.families != null){
             if(this.getFamilyCollection().contains(family)) {
                 return this.families.remove(family);
             }
         }
        return false;
    }


    //6. Обновляет существующую семью в БД если такая уже существует,
    // - сохраняет семью в конец списка - если семьи в БД нет.
    @Override
    public void saveFamily(Family family) {
        if(this.families == null) {
            this.setNewFamilyCollection();
        }
        if(this.getFamilyCollection().contains(family)){
            this.families.set(this.getFamilyCollection().indexOf(family), family);
        } else{
            this.families.add(family);
        }
    }



    //7. Возвращает количество семей в БД.
    public int count(){
        return this.getFamilyCollection().size();
    }


    //8.  Находит семьи с количеством людей больше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей больше чем указанное
    public List<Family> getFamiliesBiggerThan(int members){
       if (this.families != null) {
           List<Family> selectedFamilies = new ArrayList<>();
           for (Family el : this.getFamilyCollection()){
               if(el.getMembersTotal() > members){
                   selectedFamilies.add(el);
               }
           }
           if(selectedFamilies.size() > 0){
               System.out.println("\n>>> Families with members more then " + members + ": " + selectedFamilies);
               return selectedFamilies;
           }
       }
       return null;
    }


    //9.  Находит семьи с количеством людей меньше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей меньше чем указанное
    public List<Family> getFamiliesLessThan(int members) {
        if (this.families != null) {
            List<Family> selectedFamilies = new ArrayList<>();
            for (Family el : this.getFamilyCollection()){
                if(el.getMembersTotal() < members){
                    selectedFamilies.add(el);
                }
            }
            if(selectedFamilies.size() > 0){
                System.out.println("\n>>> Families with members less then " + members + ": " + selectedFamilies);
                return selectedFamilies;
            }
        }
        return null;
    }


    //10.  Подсчитывает число семей с количеством людей равное переданному числу.
    public int countFamiliesWithMemberNumber(int members){
        int count = 0;
        if (this.families != null) {
            for (Family el : this.getFamilyCollection()){
                if(el.getMembersTotal() == members){
                    count++;
                }
            }
        }
        return count;
    }


    //11.  Создает новую семью (принимает 2 параметра типа Human):
    //  - создает новую семью, - сохраняет в БД.
    public void createNewFamily(Human woman, Human man){
        Family newFamily = new Family((Woman) woman, (Man) man);
        if(this.families == null){
            this.setNewFamilyCollection();
        }
        this.families.add(newFamily);
    }


    //12. Создает ребенка семьей (принимает Family и 2 типа String: мужское и женское имя)
    //  - в данной семье появляется новый ребенок с учетом данных родителей,
    //  - если рожденный ребенок мальчик - ему присваивается мужское имя, если девочка - женское,
    //  - информация о семье обновляется в БД, - метод возвращает обновленную семью.
    public Family bornChild(Family family, String womanName, String manName){
         if(this.families != null){
             int familyIndex = this.getFamilyCollection().indexOf(family);
             Family fam1 = this.getFamilyCollection().get(familyIndex);

             int randGender = fam1.generateGender();
             if (randGender <= 50) {
                 Woman childW = (Woman) fam1.bornChild(randGender);
                 childW.setName(womanName);
                 fam1.addChildToList(childW);
             } else {
                 Man childM = (Man) fam1.bornChild(randGender);
                 childM.setName(manName);
                 fam1.addChildToList(childM);
             }

             this.families.set(familyIndex, fam1);
             return this.getFamilyCollection().get(familyIndex);
         }
        return null;
    }


    //13. Усыновляет ребенка (принимает 2 параметра: Family, Human) - метод возвращает обновленную семью,
    //  - в данной семье сохраняется данный ребенок, - информация о семье обновляется в БД.
    public Family adoptChild(Family family, Human child){
        if(this.families != null){
            if(this.getFamilyCollection().contains(family)){
                int index = this.getFamilyCollection().indexOf(family);
                Family fam = this.getFamilyCollection().get(index);
                child.setSurname(fam.getFather().getSurname());
                fam.addChildToList(child);

                this.families.set(index, fam);
                return this.getFamilyCollection().get(index);
            }
        }
        return null;
    }


    //14. Удаляет детей старше чем (принимает int)
    // - во всех семьях удаляются дети, которые старше указанно возраста, - информация обновляется в БД.
    public void deleteAllChildrenOlderThen(int yearsOld){
        if(this.families != null){
            for (Family family: this.getFamilyCollection()) {
                if(family.getChildrenList() == null || family.getChildrenList().getChildrenList().size() == 0){
                    continue;
                }

                for(int i = 0; i < family.getChildrenList().getChildrenList().size(); i++){

                    long birthDateMilli = family.getChildrenList().getChildrenList().get(i).getBirthDateMilli();
                    LocalDate birthdayDate = Instant.ofEpochMilli(birthDateMilli).atZone(ZoneId.systemDefault()).toLocalDate();
                    LocalDate toCompareWith = LocalDate.now().minusYears(yearsOld);

                    if(birthdayDate.isBefore(toCompareWith)){
                        family.deleteChildFromList(family.getChildrenList().getChildrenList().get(i));
                        i--;
                    }
                }
                int index = this.getFamilyCollection().indexOf(family);
                this.families.set(index, family);
            }
        }
    }


    //15.  Возвращает список домашних животных, которые живут в семье (принимает индекс семьи),
    public PetsTreeSet getPets(int index){
        if(this.families != null){
            if(this.getFamilyCollection().contains(this.getFamilyCollection().get(index))){
                if(this.getFamilyByIndex(index).getPetsSet() != null){
                    return this.getFamilyByIndex(index).getPetsSet();
                }
                System.out.println("Family with index " + index + " has no pets!");
            }
        }

        return null;
    }


    //16. Добавляет питомца (принимает индекс семьи и параметр Pet) - добавляет питомца в семью, - обновляет данные в БД.
    public boolean addPet(int index, Pet pet){
        if(this.families != null) {
            if(this.getFamilyCollection().contains(this.getFamilyCollection().get(index))){
                boolean result = this.getFamilyByIndex(index).addPet(pet);
                this.families.set(index, this.getFamilyByIndex(index));
                return result;
            }
        }
        return false;
    }


    //17. Удаляет питомца (принимает индекс семьи и параметр Pet) - удаляет питомца из семьи,  - обновляет данные в БД.
    public boolean deletePet(int index, Pet pet){
        if(this.families != null) {
            if(this.getFamilyCollection().contains(this.getFamilyCollection().get(index))){
                boolean result = this.getFamilyByIndex(index).deletePet(pet);
                this.families.set(index, this.getFamilyByIndex(index));
                return result;
            }
        }
        return false;
    }


}
