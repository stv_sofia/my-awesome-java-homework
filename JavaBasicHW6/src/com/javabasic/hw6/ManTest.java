package com.javabasic.hw6;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class ManTest {
    private Man module;

    @Before
    public void setUp() throws Exception{
        module = new Man("Adam", "Firstman");
        module.setIqLevel(99);
    }


//  test Check Man() no initialize
    @Test
    public void testCheckManEmptyGenderWomanSuccess(){
        Man module = new Man();
        assertEquals("man", module.getGender().gender);
    }

//  test Check Man ToString()
    @Test
    public void testCheckManToStringSuccess() {
        assertEquals("\n\t\tHuman{gender=MAN, iqLevel=99}\n" +
                "\t\tMan{name='Adam', surname='Firstman', yearBirth='no info', family='no info', " +
                "pets='no info', schedule='no info'}", module.toString());
    }


//  test Check Man Equals()
    @Test
    public void testCheckManEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckManNotEqualsSuccess() {
        assertNotEquals(new Man(), module);
    }


//  test Check Man HashCode()
    @Test
    public void testCheckManHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckManHashCodeNotEqualsSuccess() {
        assertNotEquals(new Man().hashCode(), module.hashCode());
    }


//  test Check Man GreetPet()
    @Test
    public void testCheckManGreetPetButNoHavePetSuccess() {
        String result = "";
        if(module.getPets() == null){
            result = "Have no pet yet..";
        }
        assertNull(module.getPets());
        assertEquals("Have no pet yet..", result);
    }
    @Test
    public void testCheckManGreetSuccess() {
        Pets pets = new Pets();
        module.setPets(pets);
        String result = "";
        if(module.getPets() != null){
            result = "I greet my pet";
        }
        assertNotNull(module.getPets());
        assertEquals("I greet my pet", result);
    }


//  test Check Man DescribePet()
    @Test
    public void testCheckManDescribePetButHaveNoPetSuccess() {
        String result = "";
        if(module.getPets() == null){
            result = "Have no pet to describe";
        }
        assertNull(module.getPets());
        assertEquals("Have no pet to describe", result);
    }
    @Test
    public void testCheckManDescribePetButNoHaveNoSuchPetSuccess() {
        Pets pets = new Pets();
        module.setPets(pets);
        String mensPetNickName = "Tom";
        String somePetNickName = "Bob";
        String result = "";
        if(!mensPetNickName.equals(somePetNickName)){
            result = "I can describe my pet, but I have no such pet!";
        }
        assertNotNull(module.getPets());
        assertEquals("I can describe my pet, but I have no such pet!", result);
    }
    @Test
    public void testCheckManDescribePetSuccess() {
        Pets pets = new Pets();
        module.setPets(pets);
        String result = "";
        if(module.getPets() != null){
            result = "I describe my pet";
        }
        assertNotNull(module.getPets());
        assertEquals("I describe my pet", result);
    }


//  test Check Man TreadTheBook()
    @Test
    public void testCheckManReadTheBookSuccess() {
        assertEquals("It's time for me to read the book.",
                module.readTheBook());
    }
}