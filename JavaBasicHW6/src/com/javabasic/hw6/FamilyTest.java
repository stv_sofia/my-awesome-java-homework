package com.javabasic.hw6;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class FamilyTest {
    private Family module;
    @Before
    public void setUp() throws Exception {
        module = new Family( new Woman("Eve", "SomeSurname"),
                new Man("Adam", "SomeSurname"));
        module.getMother().setIqLevel(100);
        module.getFather().setIqLevel(100);
    }

//   test Check Family ToString()
    @Test
    public void testCheckFamilyToStringSuccess() {
        assertEquals("\nFamily{\n" +
                "\tmother = \n" +
                "\t\tHuman{gender=WOMAN, iqLevel=100}\n" +
                "\t\tWoman{name='Eve', surname='SomeSurname', yearBirth='no info', family='yes', pets='no info', schedule='no info'},\n" +
                "\tfather = \n" +
                "\t\tHuman{gender=MAN, iqLevel=100}\n" +
                "\t\tMan{name='Adam', surname='SomeSurname', yearBirth='no info', family='yes', pets='no info', schedule='no info'},\n" +
                "\tchildren = [],\n" +
                "\tpets = 'no pet',\n" +
                "\ttotal human members of family = '2'\n" +
                "}", module.toString());
    }


//   test Check Family Equals()
    @Test
    public void testCheckFamilyEqualsSuccess() {
        assertEquals(module, module);
        assertTrue(module.equals(module));
    }
    @Test
    public void testCheckFamilyNotEqualsSuccess() {
        module.getMother().setIqLevel(100);
        module.getFather().setIqLevel(100);
        Family someFamily = new Family(new Woman("Eve", "OtherSurname"),
                new Man("Adam", "OtherSurname"));
        assertNotEquals(someFamily, module);
        assertFalse(module.equals(someFamily));
    }


//   test Check Family HashCode()
    @Test
    public void testCheckFamilyHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckFamilyHashCodeNotEqualsSuccess() {
        Family someFamily = new Family(new Woman("Eve", "OtherSurname"),
                new Man("Adam", "OtherSurname"));
        assertNotEquals(someFamily.hashCode(), module.hashCode());
    }


//  test @Override HumanCreator implement, method generateGender()
    @Test
    public void testCheckGenerateGender(){
        int randNum = (int) (Math.random() * 101);
        assertTrue(101 > randNum);
        assertTrue( 0 <= randNum);
    }


//  test @Override HumanCreator implement, method bornChild()
    @Test
    public void testCheckBornChildWomanSuccess(){
        int randGender = 50;
        String genderNewHuman = "";
        if(randGender <= 50){
            genderNewHuman = new Woman("Helen", "Some").getGender().gender;
        }
        assertTrue(Gender.WOMAN.gender.equals(genderNewHuman));
    }
    @Test
    public void testCheckBornChildManSuccess(){
        int randGender = 51;
        String genderNewHuman = "";
        if(randGender > 50){
            genderNewHuman = new Man("John", "Some").getGender().gender;
        }
        assertTrue(Gender.MAN.gender.equals(genderNewHuman));
    }


//   test Check Family addChild()
    @Test
    public void testCheckFamilyAddChildManSuccess() {
        assertTrue(module.addChild(new Woman("John", "SomeSurname")));
        assertEquals(1, module.getChildren().length);
    }
    @Test
    public void testCheckFamilyAddChildWomanSuccess() {
        assertTrue(module.addChild(new Woman("Helen", "SomeSurname")));
        assertNotNull( ((Woman)module.getChildren()[0]).getFamily());
        assertEquals(1, module.getChildren().length);
    }


//   test Check Family deleteChild(Gender, String name)
    @Test
    public void testCheckFamilyDeleteChildSuccess() {
        module.addChild(new Man("John", "SomeSurname"));
        assertTrue(module.deleteChild(Gender.MAN, "John"));
    }
    @Test
    public void testCheckFamilyDeleteChildNotFoundSuccess() {
        module.addChild(new Man("John", "SomeSurname"));
        assertFalse(module.deleteChild(Gender.WOMAN, "Sara"));
    }


//   test Check Family deleteChild(int)
    @Test
    public void testCheckFamilyDeleteChildByIndexSuccess(){
        module.addChild(new Man("John", "SomeSurname"));
        module.addChild(new Woman("Bella", "SomeSurname"));
        assertTrue(module.deleteChild(2));
    }
    @Test
    public void testCheckFamilyDeleteChildByIndexNotFoundSuccess(){
        module.addChild(new Man("John", "SomeSurname"));
        module.addChild(new Woman("Bella", "SomeSurname"));
        assertFalse(module.deleteChild(3));
        assertFalse(module.deleteChild(0));
    }


//   test Check Family addPet()
    @Test
    public void testCheckFamilyAddPetSuccess() {
        module.addPet(Species.DOG, "Rocky");
        assertNotNull(module.getPets());
        assertNotNull(module.getPets().getDogs()[0].getFamily());
    }
    @Test
    public void testCheckFamilyAddPetNoParamsSuccess() {
        assertTrue(module.addPet(Species.UNKNOWN, "Bob"));
    }


//   test Check Family countFamilyMembers()
    @Test
    public void testCheckFamilyCountFamilyMembersSuccess() {
        assertEquals(2, module.countFamilyMembers());
        module.addChild(new Man("John", "SomeSurname"));
        assertEquals(module.getMembersTotal(), module.countFamilyMembers());
        assertEquals(3, module.countFamilyMembers());
    }
    @Test
    public void testCheckFamilyAddChildSuccess() {
        module.addChild(new Man("John", "SomeSurname"));
        assertNotNull( ((Man)module.getChildren()[0]).getFamily() );
        assertEquals(1, module.getChildren().length);
    }
}