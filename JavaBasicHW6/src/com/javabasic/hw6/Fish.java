package com.javabasic.hw6;

import java.util.*;

public class Fish extends Pet{
    private String[] habits;
    private Family family;

    //  Constructors
    Fish(){
        super(Species.FISH);
    }
    Fish(Species species) {
        super(Species.FISH);
    }
    Fish(Species species, String nickname) {
        super(Species.FISH, nickname);
    }
    Fish(String nickname, int age) {
        super(Species.FISH, nickname, age);
    }
    Fish(String nickname, int age, String[] habits) {
        super(Species.FISH, nickname, age);
        this.habits = habits;
    }

//  Getters & Setters
    public String[] getHabits() { return this.habits; }
    public void setHabits(String[] habits) { this.habits = habits; }

    public Family getFamily() { return family; }
    public void setFamily(Family family) { this.family = family;}


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        assert super.getNickname() != null && super.getSpecies() != null;
        return super.toString() +
                "\n\t\t\t\tFish{" +
                "nickname='" + (this.getNickname() == null ? "'no info'" :  this.getNickname()) +  '\'' +
                ", can fly='" + this.getSpecies().canFly +  '\'' +
                ", number of legs='" + this.getSpecies().numberOfLegs +  '\'' +
                ", has fur='" + this.getSpecies().hasFur +  '\'' +
                ", age='" + (this.getAge() == -1 ? "no info" : this.getAge()) +  '\'' +
                ", trickLevel='" + this.getTrickLevel() +  '\'' +
                ", habits=" + (this.habits == null ? "'no info'" : Arrays.toString(this.habits)) +
                '}';
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Fish)) return false;
        if (!super.equals(obj)) return false;
        Fish fish = (Fish) obj;
        return super.equals(fish) && this.getAge() == fish.getAge() && this.getTrickLevel() == fish.getTrickLevel() &&
                this.getNickname().equals(fish.getNickname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.getNickname(), this.getAge(), this.getTrickLevel());
    }


//  @Override super (Pet) abstract methods petResponding() |переопределяю|
    @Override
    public void petResponding(){
        System.out.printf("Hello, master! I am yor %s %s. And I miss you so!%n",
                this.getSpecies().species, this.getNickname());
    }
}