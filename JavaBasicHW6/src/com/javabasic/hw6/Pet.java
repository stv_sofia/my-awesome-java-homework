package com.javabasic.hw6;

import java.util.*;

public abstract class Pet {
    private Species species;
    private int trickLevel = -1;
    private String nickname;
    private int age = -1;


//  Constructors
    Pet(){
        this.species = Species.UNKNOWN;
    }
    Pet(Species species) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
    }
    Pet(Species species, String nickName) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
        this.nickname = nickName;
    }
    Pet(Species species, String nickName, int age) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
        this.nickname = nickName;
        this.age = age;
    }

//  Getters & Setters
    public Species getSpecies() { return this.species; }
    public void setSpecies(Species species) { this.species = species; }

    public int getTrickLevel() { return trickLevel; }
    public void setTrickLevel(int trickLevel) { this.trickLevel = trickLevel; }

    public String getNickname() { return nickname; }
    public void setNickname(String nickname) { this.nickname = nickname;}

    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return "\n\t\t\tPet{" +
                "species=" + species +
                '}';
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Pet)) return false;
        Pet pet = (Pet) obj;
        return getTrickLevel() == pet.getTrickLevel() && getAge() == pet.getAge()
                && getSpecies() == pet.getSpecies() && getNickname().equals(pet.getNickname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getTrickLevel(), getNickname(), getAge());
    }


//  (Pet) abstract methods petResponding() |нужно переопределить по ТЗ, п э abstract methods|
    public abstract void petResponding();


//  (Pet) methods petEating()
    public void petEating(){
        System.out.printf("I'm pet %s. And I'm eating!%n", this.getSpecies().species);
    }


//  (Pet) methods generateTrickLevel()
    public int generateTrickLevel(){ return (int) (Math.random() * 101); }


//  (Pet) methods getTrickLevelString()
    public String getTrickLevelString(int trickLevel){
        if(trickLevel == -1) return "maybe canning";
        if(trickLevel < 0 || trickLevel > 100) return "not in range of trickLevel [0:100]";
        return trickLevel > 50 ? "very canning one" : "almost not canning";
    }
}