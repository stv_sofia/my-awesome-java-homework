package com.javabasic.hw6;

import java.util.*;

public class Cat extends Pet implements Foulable{
    private String[] habits;
    private Family family;

//  Constructors
    Cat(){
        super(Species.CAT);
    }
    Cat(Species species) {
        super(Species.CAT);
    }
    Cat(Species species, String nickname) {
        super(Species.CAT, nickname);
    }
    Cat(String nickname, int age) {
        super(Species.CAT, nickname, age);
    }
    Cat(String nickname, int age, String[] habits) {
        super(Species.CAT, nickname, age);
        this.habits = habits;
    }

//  Getters & Setters
    public String[] getHabits() { return this.habits; }
    public void setHabits(String[] habits) { this.habits = habits; }

    public Family getFamily() { return family; }
    public void setFamily(Family family) { this.family = family;}


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        assert super.getNickname() != null && super.getSpecies() != null;
        return super.toString() +
                "\n\t\t\t\tCat{" +
                "nickname='" + (this.getNickname() == null ? "'no info'" :  this.getNickname()) +  '\'' +
                ", can fly='" + this.getSpecies().canFly +  '\'' +
                ", number of legs='" + this.getSpecies().numberOfLegs +  '\'' +
                ", has fur='" + this.getSpecies().hasFur +  '\'' +
                ", age='" + (this.getAge() == -1 ? "no info" : this.getAge()) +  '\'' +
                ", trickLevel='" + this.getTrickLevel() +  '\'' +
                ", habits=" + (this.habits == null ? "'no info'" : Arrays.toString(this.habits)) +
                '}';
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Cat)) return false;
        if (!super.equals(obj)) return false;
        Cat cat = (Cat) obj;
        return super.equals(cat) && this.getAge() == cat.getAge()
                && this.getTrickLevel() == cat.getTrickLevel() && this.getNickname().equals(cat.getNickname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.getNickname(), this.getAge(), this.getTrickLevel());
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Woman extends Human obj before Garbage Collector will delete it : " + this);
    }


//  @Override super (Pet) abstract methods petResponding() |переопределяю|
    @Override
    public void petResponding(){
        System.out.printf("Hello, master! I am yor %s %s. And I miss you so!%n",
                this.getSpecies().species, this.getNickname());
    }


//  @Override implement (Foulable) abstract methods petDidFoul()
    @Override
    public void petDidFoul(){
        System.out.printf("I'm %s %s. I did foul) Needs to cover tracks well...%n",
                this.getSpecies().species, this.getNickname());
    }
}