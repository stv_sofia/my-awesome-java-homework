package com.javabasic.hw6;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PetsTest {
    private Pets module;
    @Before
    public void setUp() throws Exception {
        module = new Pets();
    }

    @Test
    public void testCheckPetsToStringSuccess() {
        assertEquals("Pets{\n" +
                "\t\tdogs=[],\n" +
                "\t\tcats=[],\n" +
                "\t\tfishes=[],\n" +
                "\t\trobocats=[]}", module.toString());
    }

    @Test
    public void testCheckPetsAddPetToPetsDodSuccess() {
        Dog dog = new Dog(Species.DOG, "Rocky");
        module.addPetToPets(dog);
        assertEquals(1, module.getDogs().length);
    }

    @Test
    public void testCheckPetsAddPetToPetsCatSuccess() {
        Cat cat = new Cat(Species.CAT, "Rocky");
        module.addPetToPets(cat);
        assertEquals(1, module.getCats().length);
    }

    @Test
    public void testCheckPetsAddPetToPetsFishSuccess() {
        Fish fish = new Fish(Species.FISH, "Rocky");
        module.addPetToPets(fish);
        assertEquals(1, module.getFishes().length);
    }

    @Test
    public void testCheckPetsAddPetToPetsRoboCatSuccess() {
        RoboCat robic = new RoboCat(Species.ROBOCAT, "Robic");
        module.addPetToPets(robic);
        assertEquals(1, module.getRobocats().length);
    }
}