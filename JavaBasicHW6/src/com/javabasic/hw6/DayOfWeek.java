package com.javabasic.hw6;

public enum DayOfWeek {
    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday");

    String nameCapt;

    DayOfWeek(){};

    DayOfWeek(String nameCapt){
        this.nameCapt = nameCapt;
    };
}
