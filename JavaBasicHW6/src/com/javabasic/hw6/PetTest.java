package com.javabasic.hw6;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class PetTest {
    private Pet module;

    @Before
    public void setUp() throws Exception{
        module = new Pet() { @Override public void petResponding() {}};
        module.setTrickLevel(99);
    }


//  test Check Pet() no exemplar
    @Test
    public void testCheckPetEmptySpeciesUnknownSuccess(){
    assertEquals("unknown", module.getSpecies().species);
    }


//  test Check Pet ToString()
    @Test
    public void testCheckPetToStringSuccess() {
        assertEquals("\n" +
                "\t\t\tPet{species=UNKNOWN}", module.toString());
    }


//  test Check Pet Equals()
    @Test
    public void testCheckPetEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckPetNotEqualsSuccess() {
        RoboCat petRobo = new RoboCat(Species.ROBOCAT, "RoboCat");
        assertNotEquals(petRobo, module);
    }


//  test Check Pet HashCode()
    @Test
    public void testCheckPetHashCodEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckPetHashCodNotEqualsSuccess() {
        assertNotEquals(new RoboCat(Species.ROBOCAT, "RoboCat").hashCode(), module.hashCode());
    }


//   test Check Pet generateTrickLevel()
    @Test
    public void testCheckPetGenerateTrickLevelSuccess() {
        int randNum = (int) (Math.random() * 101);
        assertTrue(101 > randNum);
        assertTrue( 0 <= randNum);
    }


//   test Check Pet getTrickLevelString()
    @Test
    public void testCheckPetGetTrickLevelStringSuccess() {
        assertEquals("maybe canning", module.getTrickLevelString(-1));
        assertEquals("very canning one", module.getTrickLevelString(51));
        assertEquals("almost not canning", module.getTrickLevelString(50));
        assertEquals("not in range of trickLevel [0:100]", module.getTrickLevelString(101));
        assertEquals("not in range of trickLevel [0:100]", module.getTrickLevelString(-2));
    }
}