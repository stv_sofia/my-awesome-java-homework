package com.javabasic.hw6;

public abstract interface HumanCreator {
    public abstract int generateGender();
    public abstract Human bornChild(int randGender);
}