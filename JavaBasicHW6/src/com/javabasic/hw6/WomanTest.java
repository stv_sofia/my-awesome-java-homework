package com.javabasic.hw6;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class WomanTest {
    private Woman module;

    @Before
    public void setUp() throws Exception{
        module = new Woman("Eve", "Firstwoman");
        module.setIqLevel(99);
    }

//  test Check Woman() no initialize
    @Test
    public void testCheckWomanEmptyGenderWomanSuccess(){
        Woman module = new Woman();
        assertEquals("woman", module.getGender().gender);
    }


//  test Check Woman ToString()
    @Test
    public void testCheckWomanToStringSuccess() {
        assertEquals("\n\t\tHuman{gender=WOMAN, iqLevel=99}\n" +
                "\t\tWoman{name='Eve', surname='Firstwoman', yearBirth='no info', family='no info', " +
                "pets='no info', schedule='no info'}", module.toString());
    }


//  test Check Woman testEquals()
    @Test
    public void testCheckWomanEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckWomanNotEqualsSuccess() {
        assertNotEquals(new Woman(), module);
    }

    
//  test Check Woman HashCode()
    @Test
    public void testCheckWomanHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckWomanHashCodeNotEqualsSuccess() {
        assertNotEquals(new Woman().hashCode(), module.hashCode());
    }


//  test Check Woman GreetPet()
    @Test
    public void testCheckWomanGreetPetButNoHavePetSuccess() {
        String result = "";
        if(module.getPets() == null){
            result = "Have no pet yet..";
        }
        assertNull(module.getPets());
        assertEquals("Have no pet yet..", result);
    }
    @Test
    public void testCheckWomanGreetSuccess() {
        Pets pets = new Pets();
        module.setPets(pets);
        String result = "";
        if(module.getPets() != null){
            result = "I greet my pet";
        }
        assertNotNull(module.getPets());
        assertEquals("I greet my pet", result);
    }


//  test Check Woman DescribePet()
    @Test
    public void testCheckWomanDescribePetButHaveNoPetSuccess() {
        String result = "";
        if(module.getPets() == null){
            result = "Have no pet to describe";
        }
        assertNull(module.getPets());
        assertEquals("Have no pet to describe", result);
    }
    @Test
    public void testCheckWomanDescribePetButNoHaveNoSuchPetSuccess() {
        Pets pets = new Pets();
        module.setPets(pets);
        String womensPetNickName = "Tom";
        String somePetNickName = "Bob";
        String result = "";
        if(!womensPetNickName.equals(somePetNickName)){
            result = "I can describe my pet, but I have no such pet!";
        }
        assertNotNull(module.getPets());
        assertEquals("I can describe my pet, but I have no such pet!", result);
    }
    @Test
    public void testCheckWomanDescribePetSuccess() {
        Pets pets = new Pets();
        module.setPets(pets);
        String result = "";
        if(module.getPets() != null){
            result = "I describe my pet";
        }
        assertNotNull(module.getPets());
        assertEquals("I describe my pet", result);
    }


//  test Check Woman TravelToSeaSide()
    @Test
    public void testCheckWomanTravelToSeaSideSuccess() {
        assertEquals("Goodbye every one! In's time to travel to the Sea side!",
                module.travelToSeaSide());
    }
}