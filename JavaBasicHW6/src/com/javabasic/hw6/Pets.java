package com.javabasic.hw6;

import java.util.*;


public class Pets {
    private Dog[] dogs = new Dog[0];
    private Cat[] cats = new Cat[0];
    private Fish[] fishes = new Fish[0];
    private RoboCat[] robocats = new RoboCat[0];

    Pets(){};

    public Dog[] getDogs() { return dogs; }
    public Cat[] getCats() { return cats; }
    public Fish[] getFishes() { return fishes; }
    public RoboCat[] getRobocats() { return robocats; }

    @Override
    public String toString() {
        return "Pets{\n" +
                (this.dogs == null ? "" : "\t\tdogs=" + Arrays.toString(dogs)) +
                (this.cats == null ? "" : ",\n\t\tcats=" + Arrays.toString(cats)) +
                (this.fishes == null ? "" : ",\n\t\tfishes=" + Arrays.toString(fishes)) +
                (this.robocats == null ? "" : ",\n\t\trobocats=" + Arrays.toString(robocats)) +
                '}';
    }

    public void addPetToPets(Dog dog){
        Dog[] newDogs = new Dog[this.dogs.length +1];
        if(newDogs.length == 1) {
            newDogs[0] = dog;
        }
        int i = 0;
        for(Dog el : dogs){
            newDogs[i] = el;
            i++;
        }
        newDogs[this.dogs.length] = dog;
        this.dogs = Arrays.copyOf(newDogs,this.dogs.length +1);
    }

    public void addPetToPets(Cat cat){
        Cat[] newCats = new Cat[this.cats.length +1];
        if(newCats.length == 1) {
            newCats[0] = cat;
        }
        int i = 0;
        for(Cat el : cats){
            newCats[i] = el;
            i++;
        }
        newCats[this.cats.length] = cat;
        this.cats = Arrays.copyOf(newCats,this.cats.length +1);
    }

    public void addPetToPets(Fish fish){
        Fish[] newFish = new Fish[this.fishes.length +1];
        if(newFish.length == 1) {
            newFish[0] = fish;
        }
        int i = 0;
        for(Fish el : fishes){
            newFish[i] = el;
            i++;
        }
        newFish[this.fishes.length] = fish;
        this.fishes = Arrays.copyOf(newFish,this.fishes.length +1);
    }

    public void addPetToPets(RoboCat roboCat){
        RoboCat[] newRoboCat = new RoboCat[this.robocats.length +1];
        if(newRoboCat.length == 1) {
            newRoboCat[0] = roboCat;
        }
        int i = 0;
        for(RoboCat el : robocats){
            newRoboCat[i] = el;
            i++;
        }
        newRoboCat[this.robocats.length] = roboCat;
        this.robocats = Arrays.copyOf(newRoboCat,this.robocats.length +1);
    }
}
