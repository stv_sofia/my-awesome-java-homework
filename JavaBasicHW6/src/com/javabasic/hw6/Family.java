package com.javabasic.hw6;

import java.util.*;

public class Family implements HumanCreator{
    private static final int MEMBERS_STATIC = 2;
    private Woman mother;
    private Man father;
    private Human[] children;
    private Pets pets;
    private int membersTotal;

//  Constructors
    Family(Woman mother, Man father){
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.membersTotal = Family.MEMBERS_STATIC;
        // добавляем ссылку для mother(firstWoman) и для father(firstMan) на их семью(на эту семью)
        mother.setFamily(this);
        father.setFamily(this);
        // изменяем фамилию женщины на фамилию мужчины
        mother.setSurname(father.getSurname());
    }

//  Getters & Setters
    public Woman getMother() { return mother; }
    public void setMother(Woman mother) { this.mother = mother; }

    public Man getFather() { return father; }
    public void setFather(Man father) { this.father = father; }

    public Human[] getChildren() { return this.children; }
    public void setChildren(Human[] children) { this.children = children; }

    public Pets getPets() { return pets; }
    public void setPets(Pets pets) { this.pets = pets; }

    public int getMembersTotal() { return Family.MEMBERS_STATIC  + this.children.length; }
    public void setMembersTotal() { this.membersTotal = Family.MEMBERS_STATIC + this.children.length; }


//  @Override method toString, equals & hashCode
    @Override
    public String toString() {
        return "\nFamily{" +
                "\n\tmother = " + this.mother.toString() + "," +
                "\n\tfather = " + this.father.toString() + "," +
                "\n\tchildren = " + Arrays.toString(this.children) + "," +
                "\n\tpets = " + (this.pets == null ? "'no pet'" : pets.toString()) + "," +
                "\n\ttotal human members of family = '" + membersTotal + '\'' +
                "\n}";
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Family family = (Family) obj;
        return this.mother.equals(family.mother) && this.father.equals(family.father)
                && Arrays.equals(this.children, family.children);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(this.mother, this.father) + Arrays.hashCode(this.children);
        return result;
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family obj before Garbage Collector will delete it : " + this);
    }


//  @Override HumanCreator implement, method generateGender() и bornChild()
    @Override
    public int generateGender(){ return (int) (Math.random() * 101); }
    @Override
    public Human bornChild(int randGender){
        String[] womanNames = {"Adel", "Bella", "Rose", "Helen", "Rowan"};
        String[] manNames = {"Cain", "Albert", "Bill", "Vlad", "Mark"};
        int indName = (int) (Math.random() * 5);
        return randGender <= 50 ?
                new Woman (womanNames[indName], this.getFather().getSurname(),
                        (this.getFather().getIqLevel()+this.getMother().getIqLevel())/2,
                        this)
                : new Man (manNames[indName], this.getFather().getSurname(),
                          (this.getFather().getIqLevel()+this.getMother().getIqLevel())/2,
                          this);
    }



//  Family methods addChild()
    public boolean addChild(Woman child) {
        child.setFamily(this);
        child.setPets(this.pets);
        int newLength = this.children.length;
        Human[] newChildren = new Human[newLength+1];
        if(newChildren.length == 1) {
            newChildren[0] = child;
        }
        for (int i = 0; i <= newLength; i++){
        if(i != newLength){
            newChildren[i] = this.children[i];
            continue;
        }
        newChildren[i] = child;
    }
        this.children = Arrays.copyOf(newChildren, newLength+1);
        this.setMembersTotal();
        return true;
    }

    public void addChild(Man child) {
        child.setFamily(this);
        child.setPets(this.pets);
        int newLength = this.children.length;
        Human[] newChildren = new Human[newLength+1];
        if(newChildren.length == 1) {
            newChildren[0] = child;
        }
        for (int i = 0; i <= newLength; i++){
            if(i != newLength){
                newChildren[i] = this.children[i];
                continue;
            }
            newChildren[i] = child;
        }
        this.children = Arrays.copyOf(newChildren, newLength+1);
        this.setMembersTotal();
    }



//  Family methods deleteChild()
    public boolean deleteChild(int ind) {
        Human[] newChildren = new Human[this.children.length-1];

        if(ind <= 0 || ind > this.children.length) {
            System.out.println("There is no child " + ind + " in this family!!!");
            return false;
        } else {
            for(int i = 0, newI = 0; i < this.children.length; i++){
                if(i == ind-1) {
                    continue;
                }
                newChildren[newI] = this.children[i];
                newI++;
            }

            Human childGoing = this.children[ind-1];
            String childGoingName = "";
            if(childGoing.getGender().gender.equals("woman")){
                childGoingName = ((Woman) childGoing).getName();
                ((Woman) childGoing).setFamily(null);
            } else {
                childGoingName = ((Man) childGoing).getName();
                ((Man) childGoing).setFamily(null);
            }
            System.out.println("Child " + childGoingName + " " + this.getFather().getSurname() +
                    " has left the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".");

            this.children = Arrays.copyOf(newChildren, this.children.length-1);
            this.setMembersTotal();
        }
        return true;
    }

    public boolean deleteChild(Gender gender, String name){
        Human[] newChildren = new Human[this.children.length-1];
        int indexEl = 0;
        int indexOfObjToDelete = -1;
        for(Human elem : this.children){
            if(elem.getGender().gender.equals(gender.gender) && elem.getGender().gender.equals("woman")){
                if(((Woman) elem).getName().equals(name)){
                    ((Woman) elem).setFamily(null);
                    indexOfObjToDelete = indexEl;
                    break;
                }
            } else if(elem.getGender().gender.equals(gender.gender) && elem.getGender().gender.equals("man")){
                if(((Man) elem).getName().equals(name)){
                    ((Man) elem).setFamily(null);
                    indexOfObjToDelete = indexEl;
                    break;
                }
            }
            indexEl++;
        }
        if(indexOfObjToDelete == -1) {
            System.out.println("There is no child " + name + " in the family of " +
                    this.getFather().getName() + " " + this.getFather().getSurname() + ".");
            return false;
        } else {
            for(int i = 0, newI = 0; i < this.children.length; i++){
                if(i == indexOfObjToDelete) {
                    continue;
                }
                newChildren[newI] = this.children[i];
                newI++;
            }
            this.children = Arrays.copyOf(newChildren, this.children.length-1);
            System.out.println("Child " + name + " " + this.getFather().getSurname() +
                    " has left the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".");
            this.setMembersTotal();
            return true;
        }
    }



//  Family method addPet()
    public boolean addPet(Species species, String nickName){
        if(this.getPets() == null){
            setPets(new Pets());
            // добавляем ссылку для mother(woman) и для father(man) на их питовцев (на питовцев этой семьи)
            mother.setPets(this.getPets());
            father.setPets(this.getPets());
            for(Human child : this.children){
                if (child.getGender().gender == "woman") {
                    ((Woman)child).setPets(this.getPets());
                } else {
                    ((Man)child).setPets(this.getPets());
                }
            }
        }
        switch (species.species){
            case "dog":
                Dog dog = new Dog();
                dog.setNickname(nickName);
                dog.setFamily(this);
                this.pets.addPetToPets(dog);
                break;

            case "domestic cat":
                Cat cat = new Cat();
                cat.setNickname(nickName);
                cat.setFamily(this);
                this.pets.addPetToPets(cat);
                break;

            case "fish":
                Fish fish = new Fish();
                fish.setNickname(nickName);
                fish.setFamily(this);
                this.pets.addPetToPets(fish);
                break;

            case "robocat":
                RoboCat robocat = new RoboCat();
                robocat.setNickname(nickName);
                robocat.setFamily(this);
                this.pets.addPetToPets(robocat);
                break;

            default:
                System.out.println("Not correct parameters!!!");
        }

        return true;
    }


//  Family method countFamilyMembers()
    public int countFamilyMembers(){ return this.getMembersTotal(); }
}