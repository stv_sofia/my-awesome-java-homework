package com.javabasic.hw6;

import java.util.*;

public abstract class Human {
    private Gender gender;
    private String name;
    private String surname;
    private int yearBirth = -1;
    private int iqLevel = -1;

//  Constructors
    Human(){
        this.gender = Gender.UNKNOWN;
    }
    Human(Gender gender){
        this.gender = gender;
        this.iqLevel = generateIqLevel();
    }
    Human(Gender gender, String name, String surname) {
        this.gender = gender;
        this.iqLevel = generateIqLevel();
        this.name = name;
        this.surname = surname;
    }
    Human(Gender gender, String name, String surname, int iqLevel){
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.iqLevel = iqLevel;
    }

//  Getters & Setters
    public Gender getGender() { return gender;}
    public void setGender(Gender gender) { this.gender = gender;}

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getSurname() { return surname; }
    public void setSurname(String surname) { this.surname = surname; }

    public int getYearBirth() { return yearBirth; }
    public void setYearBirth(int yearBirth) { this.yearBirth = yearBirth; }

    public int getIqLevel() { return iqLevel; }
    public void setIqLevel(int iqLevel) { this.iqLevel = iqLevel; }


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return "\n\t\tHuman{gender=" + gender + ", iqLevel=" + iqLevel + "}";
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human) obj;
        return getYearBirth() == human.getYearBirth() && getIqLevel() == human.getIqLevel()
                && getGender() == human.getGender() && Objects.equals(getName(), human.getName())
                && Objects.equals(getSurname(), human.getSurname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(getGender(), getName(), getSurname(), getYearBirth(), getIqLevel());
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human obj before Garbage Collector will delete it : " + this);
    }


//  Human abstract methods |нужно переопределить по ТЗ, п э abstract methods|
    public abstract void greetPet(Species species, String nickName);


//  Human methods describePet() |в ТЗ не указано, но я переопределю в наследниках, проедположим, что чел описывает только своего питомца|
    public void describePet(Species species, String nickName){
        System.out.println("I am not sure I have pet or not... You want me to describe my pet? )))");
    }


//  Human methods feedPet() |надо бы преропределить, проверить есть ли такой питомец, но в ТЗ не указано
//  и вдруг чел захочет покормить питомца друга или вообще бездомного|
    public boolean feedPet(boolean isTimeToEat, Species species, String nickName, int trickLevel){
        if(isTimeToEat){
            System.out.println("Hmm... Let's feed this " + species.species + " " + nickName + "!");
                return true;
            }
        int randTrickLevel = this.generateIqLevel();
        if (trickLevel > randTrickLevel){
            System.out.println("BUT! pet trick level=" + trickLevel + " > random trick level=" + randTrickLevel);
                System.out.println("It's so canning! Let's feed this " + species.species + " " + nickName + "!");
                return true;
            }
            System.out.println("I think " + nickName + " is not hungry yet!");
            return false;
    }


//  Human methods generateIqLevel()
    public int generateIqLevel(){ return (int) (Math.random() * 101); }

}