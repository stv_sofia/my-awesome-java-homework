package com.javabasic.hw6;

import java.util.*;

public final class Woman extends Human{
    private Family family;
    private Pets pets;
    private String[][] schedule;

//  Constructors
    Woman(){ super(Gender.WOMAN); }

    Woman(Gender gender){
        super(Gender.WOMAN);
    }
    Woman(String name, String surname){
        super(Gender.WOMAN, name, surname);
    }
    Woman(String name, String surname, int iqLevel, Family family){
        super(Gender.WOMAN, name, surname, iqLevel);
        this.family = family;
    }

//  Getters & Setters
    public Family getFamily() { return family; }
    public void setFamily(Family family) { this.family = family; }

    public Pets getPets() { return pets; }
    public void setPets(Pets pets) { this.pets = pets; }

    public String[][] getSchedule() { return schedule; }
    public void setSchedule(String[][] schedule) { this.schedule = schedule; }


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        assert super.getName() != null && super.getSurname() != null;
        return super.toString() +
                "\n\t\tWoman{name='" + super.getName() + '\'' +
                ", surname='" + super.getSurname() + '\'' +
                ", yearBirth=" + (super.getYearBirth() == -1 ? "'no info'" : super.getYearBirth()) +
                ", family=" + (this.family == null ?  "'no info'" : "'yes'") +
                ", pets=" + (this.pets == null ?  "'no info'" : "'yes'") +
                ", schedule=" + (this.schedule == null ? "'no info'" : Arrays.deepToString(this.schedule)) +
                '}';
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Woman)) return false;
        if (!super.equals(obj)) return false;
        Woman woman = (Woman) obj;
        return  super.equals(woman) &&
                getIqLevel() == woman.getIqLevel()
                && Objects.equals(this.getFamily(), woman.getFamily())
                && Objects.equals(this.getPets(), woman.getPets())
                && Arrays.deepEquals(this.getSchedule(), woman.getSchedule());
    }
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Woman extends Human obj before Garbage Collector will delete it : " + this);
    }


//  @Override super (Human) abstract methods greetPet() |переопределяю|
    @Override
    public void greetPet(Species species, String nickName){
        if(this.getPets() == null) {
            System.out.println("Have no pet yet..");
        }
            String petNickName = "";
            switch (species.species) {
                case "dog":
                    for (int i = 0; i < this.getPets().getDogs().length; i++) {
                        if (this.getPets().getDogs()[i].getNickname().equals(nickName)) {
                            petNickName = this.getPets().getDogs()[i].getNickname();
                        }
                    }
                    if (petNickName.equals("")) {
                        System.out.println("You have no such dog!!!");
                    }
                    System.out.printf("Hello, my dear %s %s! I am your owner %s and I love you! %n",
                            species.species, petNickName, this.getName());
                    break;
                case "fish":
                    for (int i = 0; i < this.getPets().getFishes().length; i++) {
                        if (this.getPets().getFishes()[i].getNickname().equals(nickName)) {
                            petNickName = this.getPets().getFishes()[i].getNickname();
                        }
                    }
                    if (petNickName.equals("")) {
                        System.out.println("You have no such fish!!!");
                    }
                    System.out.printf("Hello, my dear %s %s! I am your owner %s and I love you! %n",
                            species.species, petNickName, this.getName());
                    break;
                case "domestic cat":
                    for (int i = 0; i < this.getPets().getCats().length; i++) {
                        if (this.getPets().getCats()[i].getNickname().equals(nickName)) {
                            petNickName = this.getPets().getCats()[i].getNickname();
                        }
                    }
                    if (petNickName.equals("")) {
                        System.out.println("You have no such cat!!!");
                    }
                    System.out.printf("Hello, my dear %s %s! I am your owner %s and I love you! %n",
                            species.species, petNickName, this.getName());
                    break;
                case "robocat":
                    for (int i = 0; i < this.getPets().getRobocats().length; i++) {
                        if (this.getPets().getRobocats()[i].getNickname().equals(nickName)) {
                            petNickName = this.getPets().getRobocats()[i].getNickname();
                        }
                    }
                    if (petNickName.equals("")) {
                        System.out.println("You have no such robocat!!!");
                    }
                    System.out.printf("Hello, my dear %s %s! I am your owner %s and I love you! %n",
                            species.species, petNickName, this.getName());
                    break;
                default:
                    System.out.println("You have no such pet to greet!");
            }
    }


//  @Override super (Human) methods describePet() |переопределю, в ТЗ не указано, но проедположим, что чел описывает только своего питомца|
    @Override
    public void describePet(Species species, String nickName){
    if(this.getPets() == null) {
        System.out.println("Have no pet yet..");
    }
        switch (species){
            case DOG:
                for(Dog elem : this.getPets().getDogs()){
                if(elem.getNickname().equals(nickName)){
                    printDescribingPet(elem.getSpecies().species, elem.getNickname(),
                            elem.getTrickLevelString(elem.getTrickLevel()), elem.getAge());
                    break;
                }}
                break;
            case CAT:
                for(Cat elem : this.getPets().getCats()){
                if(elem.getNickname().equals(nickName)){
                    printDescribingPet(elem.getSpecies().species, elem.getNickname(),
                            elem.getTrickLevelString(elem.getTrickLevel()), elem.getAge());
                    break;
                }}
                break;
            case FISH:
                for(Fish elem : this.getPets().getFishes()){
                if(elem.getNickname().equals(nickName)){
                    printDescribingPet(elem.getSpecies().species, elem.getNickname(),
                            elem.getTrickLevelString(elem.getTrickLevel()), elem.getAge());
                    break;
                }}
                break;
            case ROBOCAT:
                for(RoboCat elem : this.getPets().getRobocats()){
                if(elem.getNickname().equals(nickName)){
                    printDescribingPet(elem.getSpecies().species, elem.getNickname(),
                            elem.getTrickLevelString(elem.getTrickLevel()), elem.getAge());
                    break;
                }}
                break;
            default:
                System.out.println("You have no such pet!!!");
        }
    }
    public static void printDescribingPet(String species, String nickName, String trickLevelStr, int age){
        if(age == -1) {
            System.out.printf("I have pet. It's a %s called %s. I don't remember how many years old it is. And it's %s :)",
                    species, nickName, trickLevelStr);
        } else {
            System.out.printf("I have pet. It's a %s called %s. It's %s years old. And it's %s :)",
                    species, nickName, age, trickLevelStr);
        }
    }


//  super (Human) methods feedPet() |оставляю как в родителе (Human), в ТЗ не указано переопределить|
//  public boolean feedPet(boolean isTimeToEat, Species species, String nickName, int trickLevel){};


//  only (Woman)'s methods
    public String travelToSeaSide() {
        return "Goodbye every one! In's time to travel to the Sea side!";
    }

}