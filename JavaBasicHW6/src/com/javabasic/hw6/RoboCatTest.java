package com.javabasic.hw6;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class RoboCatTest {
    private RoboCat module;

    @Before
    public void setUp() throws Exception {
        module = new RoboCat(Species.ROBOCAT, "Robic");
        module.setTrickLevel(100);
    }


//  test Check RoboCat() no initialize
    @Test
    public void testCheckRoboCatEmptyGenderWomanSuccess(){
        RoboCat module = new RoboCat();
        assertEquals("robocat", module.getSpecies().species);
    }


//  test Check RoboCat ToString()
    @Test
    public void testCheckRoboCatToStringSuccess() {
        assertEquals("\n\t\t\tPet{species=ROBOCAT}\n" +
                "\t\t\t\tRoboCat{nickname='Robic', can fly='false', number of legs='4', " +
                "has fur='false', age='no info', trickLevel='100', habits='no info'}", module.toString());
    }


//  test Check RoboCat Equals()
    @Test
    public void testCheckRoboCatEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckRoboCatNotEqualsSuccess() {
        assertNotEquals(new RoboCat(), module);
    }


//  test Check RoboCat HashCode()
    @Test
    public void testCheckRoboCatHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckRoboCatHashCodeNotEqualsSuccess() {
        assertNotEquals(new RoboCat().hashCode(), module.hashCode());
    }
}