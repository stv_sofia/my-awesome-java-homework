package com.javabasic.hw6;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//  пустой new Human()
        System.out.print(">>> Creating (empty) new Human() >>> ");
        System.out.println(new Human() {
            @Override public void greetPet(Species species, String nickName){}
        });

//  родители >>>
        System.out.println("\n>>> Creating (Woman)Eve & (Man)Adam >>> ");
        Woman firstWoman = new Woman("Eve", "Firstwoman");
        System.out.println("Woman: " + firstWoman);
        System.out.println("Check equals of not equals objects of women: "
                + firstWoman.equals(new Woman("Eve", "Secondwoman")));
        System.out.println("Check equals of the same object of women: "
                + firstWoman.equals(firstWoman));

        Man firstMan = new Man("Adam", "Firstman");
        System.out.println("\nMan: " + firstMan);
        System.out.println("Check equals of not equals objects of man: "
                + firstMan.equals(new Man("Adam", "Secondman")));
        System.out.println("Check equals of the same object of man: "
                + firstMan.equals(firstMan));

//  семья >>>
        System.out.println("\n>>> Creating Family of Adam & Eve >>> ");
        Family fam1 = new Family(firstWoman, firstMan);
        System.out.println("New family >>> " + fam1);

        System.out.println("\n>>> Checking family's links for Adam & Eve >>> ");
        System.out.println("Family of Eve: " + firstWoman.getFamily());
        System.out.println("Family of Adan: " + firstMan.getFamily());


//  будущие дети >>>  генерируем пол ребенка, добавляем в семью fam1, выводим ребенка, семью
        int randGender = fam1.generateGender();
        if (randGender <= 50) {
            fam1.addChild((Woman) fam1.bornChild(randGender));
        } else {
            fam1.addChild((Man) fam1.bornChild(randGender));
        }
        System.out.println("\n>>> New born child has been added to the Family: " + fam1.getChildren()[0]);
        System.out.println("\nFamily after added new child: " + fam1);


//  генерируем пол еще 2-х детей и добавляем в семью fam1, выводим детей потом семью
//  iqLevel у всех детей будет одиаковым по ТЗ |(motherIQ + fatherIQ) / 2|, можно потом засетить, что я и сделала для второго ребенка
        randGender = fam1.generateGender();
        if (randGender <= 50) {
            fam1.addChild((Woman) fam1.bornChild(randGender));
        } else {
            fam1.addChild((Man) fam1.bornChild(randGender));
        }
        randGender = fam1.generateGender();
        if (randGender <= 50) {
            fam1.addChild((Woman) fam1.bornChild(randGender));
        } else {
            fam1.addChild((Man) fam1.bornChild(randGender));
        }
        fam1.getChildren()[1].setIqLevel(98);
        fam1.getChildren()[1].setYearBirth(1998);
        System.out.println("\n>>> More 2 new born children were added to the Family: "
                + fam1.getChildren()[1] + "" + fam1.getChildren()[2]);
        System.out.println("\nFamily after adding 2 more children: " + fam1);


//  проверяем только Man & только Woman method
        System.out.println("\n>>> Checking only Man & only Woman method: ");
        System.out.println("I'm woman " + fam1.getMother().getName() + " >>> ");
        System.out.println(fam1.getMother().travelToSeaSide());
        System.out.println("I'm man " + fam1.getFather().getName() + " >>> ");
        System.out.println(fam1.getFather().readTheBook());


//      проверяем (Family) метод deleteChild(index)
        System.out.println("\n>>> Checking (Family) method deleteChild(1), deleting child number 1 >>> ");
        System.out.println("Children of " + fam1.getFather().getName() + " " +
                fam1.getFather().getSurname() + " for now: " + Arrays.toString(fam1.getChildren()) + "\n");
        fam1.deleteChild(1);
        System.out.println("Children of the Family after child number 1 left >>> " + Arrays.toString(fam1.getChildren()));


//      проверяем (Family) метод deleteChild(Gender, name),
//      добавим пару детей без генератора детей)), что бы знать кого удалять
        System.out.println("\n>>> Checking (Family) method deleteChild(Gender, name) >>>  ");
        System.out.println("Let's add to the Family children Lara, Arnold (not using HumanGenerator, not random way) >>>  ");
        fam1.addChild(new Woman("Lara", "Firstman"));
        fam1.addChild(new Man("Arnold", "Firstman"));
        System.out.println("Family children after adding Lara & Arnold, and before deleting Lara: " +
                Arrays.toString(fam1.getChildren()));
        System.out.println();
        fam1.deleteChild(Gender.WOMAN, "Lara");
        System.out.println("Family1 children after Lara left: " +
                Arrays.toString(fam1.getChildren()));
//      пробуем удалить детей которых нет у данной семьи
        System.out.println("\nTrying to delete un exist child >>>  ");
        fam1.deleteChild(Gender.WOMAN, "Marina");
        fam1.deleteChild(Gender.MAN, "Boris");


//      проверяем (Human) methods feedPet() |надо бы преропределить, проверить есть ли такой питомец, но в ТЗ не указано
//      и вдруг чел захочет покормить питомца друга или вообще бездомного|
//      >> время кормить
        System.out.println("\n>>> Checking (Human) method feedPet(boolean) >>>  ");
        System.out.println("Feeding the pet, it's time to eat >>> ");
        if(fam1.getFather().feedPet(true, Species.DOG, "Rocky", new Dog().generateTrickLevel())){
            System.out.println("Result = I fed my pet.");
        } else {
            System.out.println("Result = I didn't feed my pet.");
        }
//      >> не время кормить
        System.out.println("\nFeeding the pet, it's NOT time to eat >>> ");
        if(fam1.getFather().feedPet(false, Species.DOG, "Rocky", new Dog().generateTrickLevel())){
            System.out.println("Result = I fed the pet.");
        } else {
            System.out.println("Result = I didn't feed the pet.");
        }


//  пустой new Pet()
        System.out.print("\n>>> Creating (empty) new Pet() >>> ");
        System.out.println(new Pet() { @Override public void petResponding() {}});

//  добавляем питомцев в семью
        System.out.println("\n>>> Adding pets to the Family  >>> ");
        fam1.addPet(Species.CAT, "Tom");
        System.out.println("Pets of Family after new pet cat Tom was added >>> \n" + fam1.getPets());
        System.out.println("\nFamily after adding the pet: " + fam1);

        System.out.print("\nAdding 3 more pets to the Family  >>> ");
        fam1.addPet(Species.CAT, "Mars");
        fam1.addPet(Species.DOG, "Bob");
        fam1.addPet(Species.DOG, "Rocky");
        System.out.println("\nFamily after added 3 more pets:" + fam1);


//  проверяем ссылки mother pet has family, mother has pets, child has pet
        System.out.println("\n>>> Checking links (pet has family, mother has pets, child has pet) >>> ");

        //  проверяем ссылки у pet1
        System.out.print("Pet Tom has family >>> ");
        System.out.println(fam1.getPets().getCats()[0].getFamily());

        //  проверяем ссылки у mother
        System.out.println("\nMother has pets >>> ");
        System.out.println(fam1.getMother().getPets());

        //  т к по ТЗ массив детей (как я поняла) Human, и мы приводим их к Man или Woman в зависимости от рендера пола ребенка
        //  и кидаем их туда, то достаем все свойства и методы ребенка (Man или Woman) так же через приведениe...   //  Я бы сделала как с классом Pets, но пусть будет и так (для тренировки))
        //  проверяем ссылки у child1
        System.out.println("\nChild1 has pets >>> ");
        System.out.println(fam1.getChildren()[0].getGender().gender.equals("woman")
                ? ((Woman) fam1.getChildren()[0]).getPets()
                : ((Man) fam1.getChildren()[0]).getPets());


//  вот так можно изменять свойства объектов в Pets,
//  если нужны объекты других классов из Pets, например Cat - getCats() - достаем, находим, сетим
        for(int i = 0; i < fam1.getPets().getDogs().length; i++){
            if(fam1.getPets().getDogs()[i].getNickname().equals("Rocky")){
                fam1.getPets().getDogs()[i].setHabits(new String[]{"eat", "drink", "sleep"});
                fam1.getPets().getDogs()[i].setAge(7);
            }
        }
        System.out.println("\n>>> Dog Rocky set age & new habits >>>\nPets of Family1 after it: \n" + fam1.getPets());


//  добавляем еще питомцев
        System.out.print("\n>>> Adding to the Family 2 more pets: fish Nemo, robocat Terminator >>> ");
        fam1.addPet(Species.FISH, "Nemo");
        fam1.addPet(Species.ROBOCAT, "Terminator");
        System.out.println("\nPets of the Family after it: \n" + fam1.getPets());


//  проверяем (Human) abstract (@Override in Man & in Woman) method greetPet()
        System.out.println("\n>>> Checking (Human) abstract (@Override in Man & in Woman) method greetPet() >>> ");
        System.out.print("\t>> ");
        fam1.getFather().greetPet(Species.DOG, "Rocky");
        System.out.print("\t>> ");
        fam1.getMother().greetPet(Species.FISH, "Nemo");
        System.out.print("\t>> ");
        fam1.getFather().greetPet(Species.CAT, "Tom");
        System.out.print("\t>> ");
        fam1.getMother().greetPet(Species.ROBOCAT, "Terminator");
        System.out.print("\t>> ");
        fam1.getFather().greetPet(Species.UNKNOWN, "Tom");


//  проверяем (Woman) (Man) method describePet(Species, String nickName)
        System.out.println("\n>>> Checking (Woman) (Man) method describePet(Species, String nickName) >>> ");
        System.out.println("Woman describing her pet >>> ");
        System.out.print("I am " + fam1.getMother().getName() + ". ");
        fam1.getMother().describePet(Species.DOG, "Rocky");

        System.out.println("\n\nMan describing his pet >>> ");
        System.out.print("I am " + fam1.getFather().getName() + ". ");
        fam1.getFather().describePet(Species.ROBOCAT, "Terminator");


        System.out.println("\n");
    }
}