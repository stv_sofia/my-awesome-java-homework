package com.javabasic.hw6;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class CatTest {
    private Cat module;

    @Before
    public void setUp() throws Exception {
        module = new Cat(Species.CAT, "Tomas");
        module.setTrickLevel(95);
    }


//  test Check Cat() no initialize
    @Test
    public void testCheckCatEmptyGenderWomanSuccess(){
        Cat module = new Cat();
        assertEquals("domestic cat", module.getSpecies().species);
    }


//  test Check Cat ToString()
    @Test
    public void testCheckCatToStringSuccess() {
        assertEquals("\n\t\t\tPet{species=CAT}\n" +
                "\t\t\t\tCat{nickname='Tomas', can fly='false', number of legs='4', " +
                "has fur='true', age='no info', trickLevel='95', habits='no info'}", module.toString());
    }


//  test Check Cat Equals()
    @Test
    public void testCheckCatEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckCatNotEqualsSuccess() {
        assertNotEquals(new Cat(), module);
    }


//  test Check Cat HashCode()
    @Test
    public void testCheckCatHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckCatHashCodeNotEqualsSuccess() {
        assertNotEquals(new Cat().hashCode(), module.hashCode());
    }
}