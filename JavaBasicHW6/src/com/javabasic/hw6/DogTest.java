package com.javabasic.hw6;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class DogTest {
    private Dog module;

    @Before
    public void setUp() throws Exception {
        module = new Dog(Species.DOG, "Rocky");
        module.setTrickLevel(45);
    }


//  test Check Dog() no initialize
    @Test
    public void testCheckDogEmptyGenderWomanSuccess(){
        Dog module = new Dog();
        assertEquals("dog", module.getSpecies().species);
    }


//  test Check Dog ToString()
    @Test
    public void testCheckDogToStringSuccess() {
        assertEquals("\n\t\t\tPet{species=DOG}\n" +
                "\t\t\t\tDod{nickname='Rocky', can fly='false', number of legs='4', " +
                "has fur='true', age='no info', trickLevel='45', habits='no info'}", module.toString());
    }


//  test Check Dog Equals()
    @Test
    public void testCheckDogEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckDogNotEqualsSuccess() {
        assertNotEquals(new Dog(), module);
    }


//  test Check Dog HashCode()
    @Test
    public void testCheckDogHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckDogHashCodeNotEqualsSuccess() {
        assertNotEquals(new Dog().hashCode(), module.hashCode());
    }
}