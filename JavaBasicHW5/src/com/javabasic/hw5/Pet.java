package com.javabasic.hw5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private Species species;
    private String nickname;
    private int age = -1;
    private int trickLevel = -1;
    private String[] habits;
    private Family family;

//  Constructors
    Pet(){}
    Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

//  Getters & Setters
    public Species getSpecies() {
        return species;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }
    public void setTrickLevel() { this.trickLevel = generateTrickLevel(); }
    public void setTrickLevelNum(int num) { this.trickLevel = num; }

    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public Family getFamily() { return family; }
    public void setFamily(Family family) { this.family = family;}


//  @Override methods equals & hashCode
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;

        Pet pet = (Pet) obj;
        return this.getSpecies().equals(pet.getSpecies())
                && this.getNickname().equals(pet.getNickname())
                && this.getAge() == pet.getAge();
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.species, this.nickname, this.age);
    }

//  @Override method toString
    @Override
    public String toString(){
        //  assert this.species != null && this.nickname !=null;
        if(this.species == null && this.nickname == null){
            return "Pet{no info of pet}";
        }
        return "Pet{" +
                "species='" + (this.species == null ? "no info" : this.species.species)  + '\'' +
                ", can fly=" + (this.species == null ? "no info" : this.species.canFly) +
                ", number of legs=" + (this.species == null ? "no info" : this.species.numberOfLegs) +
                ", has fur=" + (this.species == null ? "no info" : this.species.hasFur) +
                ", nickname='" + (this.nickname == null ? "no info" : this.nickname)  +  '\'' +
                ", age=" + (this.age == -1 ? "no info" : this.age) +
                ", trickLevel=" + (this.trickLevel == -1 ? "no info" : this.trickLevel) +
                ", habits=" + (this.habits == null ? "no info" : Arrays.toString(this.habits)) +
                '}';
    }

//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Pet obj before Garbage Collector will delete it : " + this);
    }


//  Pet methods
    public int generateTrickLevel(){ return (int) (Math.random() * 101); }
    public String getTrickLevelString(int trickLevel){
        if(trickLevel == -1) return "maybe canning";
        if(trickLevel < 0 || trickLevel > 100) return "not in range of trickLevel [0:100]";
        return trickLevel > 50 ? "very canning one" : "almost not canning";

    }

    public void petResponding(){
        System.out.printf("Hello, master! I am yor %s %s. And I miss you so!%n",
                this.getSpecies().species, this.getNickname());
    }

    public void petEating(){
        System.out.printf("I'm %s %s. And I'm eating!%n",
                this.getSpecies().species, this.getNickname());
    }

    public void petDoingFoul(){
        System.out.printf("I'm %s %s. I did foul) Needs to cover tracks well...%n",
                this.getSpecies().species, this.getNickname());
    }

    public boolean isPetHasOwners(){
        return (this.family != null);
    }

    public String tellOwnersName(){
        if(this.isPetHasOwners()){
            String str1 = "I am " + this.getSpecies().species + " " + this.getNickname() +
                    ". I have my owner-mother: " + this.family.getMother().getName() +
                    ", my owner-father: " + this.family.getFather().getName();
            String str2 = " and i have no owner-child yet!";
            String str3 = "";
            if(this.family.getChildren().length == 0){
                return str1 + str2;
            }
            int i = 1;
            for(Human child : family.getChildren()){
                str3 = str3 + ", my owner-child " + i++ + ": "+ child.getName();
            }
            return str1 + str3;
        }
        return "I still looking for my owners!";
    }
}