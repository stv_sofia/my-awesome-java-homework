package com.javabasic.hw5;

import org.junit.*;
import static org.junit.Assert.*;

public class PetTest {
    private Pet module;
    @Before
    public void setUp() throws Exception { module = new Pet(); }

//   test Check Pet ToString()
    @Test
    public void testCheckPetToStringNoInfoSuccess() {
        String actualResult = module.toString();
        assertEquals("Pet{no info of pet}", actualResult);
    }
    @Test
    public void testCheckPetToStringHalfInfoSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        String actualResult = module.toString();
        assertEquals("Pet{species='dog', can fly=false, number of legs=4, has fur=true, nickname='Bob', " +
                "age=no info, trickLevel=no info, habits=no info}", actualResult);
    }
    @Test
    public void testCheckPetToStringFullInfoSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        module.setAge(5);
        module.setTrickLevelNum(77);
        module.setHabits(new String[]{"eat", "drink", "sleep"});
        String actualResult = module.toString();
        assertEquals("Pet{species='dog', can fly=false, number of legs=4, has fur=true, nickname='Bob', " +
                "age=5, trickLevel=77, habits=[eat, drink, sleep]}", actualResult);
    }


//   test Check Pet Equals()
    @Test
    public void testCheckPetEqualsSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        assertEquals( module, new Pet(Species.DOG, "Bob"));
        assertTrue(module.equals(new Pet(Species.DOG, "Bob")));
    }
    @Test
    public void testCheckPetNotEqualsSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        assertNotEquals( module, new Pet(Species.DOG, "Rocky"));
        assertFalse(module.equals(new Pet(Species.DOG, "Rocky")));
    }


//   test Check Pet HashCode()
    @Test
    public void testCheckPetHashCodesEqualsSuccess() {
        assertEquals(module.hashCode(), new Pet().hashCode());
    }
    @Test
    public void testCheckPetHashCodesNotEqualsSuccess() {
        assertNotEquals(module.hashCode(), new Pet(Species.DOG, "Rocky").hashCode());
    }


//  test Equals And HashCod Agreement >>
//    - Если x.equals(y)  возвращает true, то hashCode() у обоих экземпляров объектов должны возвращать одинаковые значения.
    @Test
    public void testCheckPetEqualsAndHashCodAgreementBothEqualsSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        assertTrue(module.equals(new Pet(Species.DOG, "Bob")));
        assertEquals(new Pet(Species.DOG, "Bob").hashCode(), module.hashCode());
    }

//    - Но если x.hashCode() == y.hashCode(), то вовсе не обязательно, чтобы x.equals(y) возвращало true,
//      x.equals(y) может возвращать как true
    @Test
    public void testCheckPetEqualsAndHashCodAgreementNotNecessaryButEqualsSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        assertEquals(new Pet(Species.DOG, "Bob").hashCode(), module.hashCode());
        assertTrue(module.equals(new Pet(Species.DOG, "Bob")));
    }

//     x.equals(y) может возвращать так и false
    @Test
    public void testCheckPetEqualsAndHashCodAgreementNotNecessaryAndNotEqualsSuccess() {
        module.setSpecies(Species.DOG);
        module.setNickname("Bob");
        assertEquals(module.hashCode(), new Pet(Species.DOG, "Bob").hashCode());
        assertFalse(module.equals(new Pet(Species.DOG, "Bob", 5,
                77, new String[]{"eat", "drink", "sleep"})));
    }


//   test Check Pet GetTrickLevelString()
    @Test
    public void testCheckPetGetTrickLevelStringNotCanningSuccess() {
        String actualResult = module.getTrickLevelString(50);
        assertEquals("almost not canning", actualResult);
    }
    @Test
    public void testCheckPetGetTrickLevelStringCanningSuccess() {
        String actualResult = module.getTrickLevelString(51);
        assertEquals("very canning one", actualResult);
    }
    @Test
    public void testCheckPetGetTrickLevelStringNotInRangeSuccess() {
        String actualResult = module.getTrickLevelString(101);
        assertEquals("not in range of trickLevel [0:100]", actualResult);
    }


//   test Check Pet GetTrickLevel()
    @Test
    public void testCheckPetIsPetHasOwnersSuccess() {
        module.setFamily(new Family(new Human(), new Human()));
        boolean actualResult = module.isPetHasOwners();
        assertTrue(actualResult);
    }
    @Test
    public void testCheckPetIsPetHasOwnersSuccessFalse() {
        assertFalse(module.isPetHasOwners());
    }

}