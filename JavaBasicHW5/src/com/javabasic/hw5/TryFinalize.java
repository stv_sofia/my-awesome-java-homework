package com.javabasic.hw5;

public class TryFinalize {
    public static void main(String[] args) {
        int p = 1, h = 1, f = 1;

        while (p < 300_000){
            new Pet();
            p++;
        }

        while (h < 100_000){
            new Human();
            h++;
        }

        while (f < 100_000){
            new Family(new Human(), new Human());
            f++;
        }

    }
}
