package com.javabasic.hw5;

import org.junit.*;
import static org.junit.Assert.*;

public class FamilyTest {
    private Family module;
    @Before
    public void setUp() throws Exception {
        module = new Family( new Human("Eve", "SomeSurname"),
                new Human("Adam", "SomeSurname"));
    }

//   test Check Family ToString()
    @Test
    public void testCheckFamilyToStringSuccess() {
        assertEquals("\nFamily{\n" +
                "\tmother = \n" +
                "\tHuman{name = 'Eve', surname = 'SomeSurname', yearBirth = no info, iqLevel = no info, schedule = no info},\n" +
                "\tfather = \n" +
                "\tHuman{name = 'Adam', surname = 'SomeSurname', yearBirth = no info, iqLevel = no info, schedule = no info},\n" +
                "\tchildren = [],\n" +
                "\tpet = no pet,\n" +
                "\ttotal human members of family = 2\n" +
                "}", module.toString());
    }


//   test Check Family Equals()
    @Test
    public void testCheckFamilyEqualsSuccess() {
        Family someFamily = new Family(new Human("Eve", "SomeSurname"),
                new Human("Adam", "SomeSurname"));
        assertEquals(someFamily, module);
        assertTrue(module.equals(someFamily));
    }
    @Test
    public void testCheckFamilyNotEqualsSuccess() {
        Family someFamily = new Family(new Human("Eve", "OtherSurname"),
                new Human("Adam", "OtherSurname"));
        assertNotEquals(someFamily, module);
        assertFalse(module.equals(someFamily));
    }


//   test Check Family HashCode()
    @Test
    public void testCheckFamilyHashCodeEqualsSuccess() {
        Family someFamily = new Family(new Human("Eve", "SomeSurname"),
            new Human("Adam", "SomeSurname"));
        assertEquals(someFamily.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckFamilyHashCodeNotEqualsSuccess() {
        Family someFamily = new Family(new Human("Eve", "OtherSurname"),
                new Human("Adam", "OtherSurname"));
        assertNotEquals(someFamily.hashCode(), module.hashCode());
    }


//   test Check Family addPet()
    @Test
    public void testCheckFamilyAddPetSuccess() {
        Pet somePet = new Pet(Species.DOG, "Rocky");
        module.addPet(somePet);
        assertNotNull(module.getPet());
        assertNotNull(somePet.getFamily());
    }
    @Test
    public void testCheckFamilyAddPetNoParamsSuccess() {
        assertFalse(module.addPet(new Pet()));
    }


//   test Check Family countFamilyMembers()
    @Test
    public void testCheckFamilyCountFamilyMembersSuccess() {
        assertEquals(2, module.countFamilyMembers());
        module.addChild(new Human("John", "SomeSurname", 1999));
        assertEquals(module.getMembersTotal(), module.countFamilyMembers());
        assertEquals(3, module.countFamilyMembers());
    }


//   test Check Family addChild(Human)
    @Test
    public void testCheckFamilyAddChildNoParamsSuccess() {
        assertFalse(module.addChild(new Human()));
    }
    @Test
    public void testCheckFamilyAddChildSuccess() {
        Human child = new Human ("John", "SomeSurname", 1999);
        assertTrue(module.addChild(child));
        assertNotNull(child.getFamily());
        assertEquals(1, module.getChildren().length);
        assertEquals(module.getChildren()[0], child);
    }


//   test Check Family deleteChild(Human)
    @Test
    public void testCheckFamilyDeleteChildSuccess() {
        Human child = new Human ("John", "SomeSurname", 1999);
        module.addChild(child);
        assertTrue(module.deleteChild(child));
    }
    @Test
    public void testCheckFamilyDeleteChildNotFoundSuccess() {
        Human someHuman = new Human ("John", "OtherSurname", 2000);
        Human child = new Human ("John", "SomeSurname", 1999);
        module.addChild(child);
        assertFalse(module.deleteChild(someHuman));
    }


//   test Check Family deleteChild(int)
    @Test
    public void testCheckFamilyDeleteChildByIndexSuccess(){
        Human child1 = new Human ("John", "SomeSurname", 1999);
        Human child2 = new Human ("Bella", "SomeSurname", 1987);
        module.addChild(child1);
        module.addChild(child2);
        assertTrue(module.deleteChild(2));
        assertNull(child2.getFamily());
    }
    @Test
    public void testCheckFamilyDeleteChildByIndexNotFoundSuccess(){
        Human child1 = new Human ("Bill", "SomeSurname", 1978);
        Human child2 = new Human ("Bella", "SomeSurname", 1987);
        module.addChild(child1);
        module.addChild(child2);
        assertFalse(module.deleteChild(3));
        assertFalse(module.deleteChild(0));
    }

}