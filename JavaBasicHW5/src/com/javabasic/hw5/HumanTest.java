package com.javabasic.hw5;

import org.junit.*;
import static org.junit.Assert.*;

public class HumanTest {
    private Human module;
    @Before
    public void setUp() throws Exception { module = new Human(); }

//   test Check Human ToString()
    @Test
    public void testCheckHumanToStringNoInfoSuccess() {
        String actualResult = module.toString();
        assertEquals("Human{no info of human}", actualResult);
    }
    @Test
    public void testCheckHumanToStringHalfInfoSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        String actualResult = module.toString();
        assertEquals("\n\tHuman{name = 'Adam', surname = 'SomeSurname', yearBirth = no info, " +
                "iqLevel = no info, schedule = no info}", actualResult);
    }
    @Test
    public void testCheckHumanToStringFullInfoSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setYearBirth(1956);
        module.setIqLevel(100);
        module.setSchedule(new String[][] {
                {DayOfWeek.SUNDAY.name(), "sleep", "eat", "riding a horse"},
                {DayOfWeek.MONDAY.nameCapt, "hunt", "eat", "riding a horse"}});
        String actualResult = module.toString();
        assertEquals("\n\tHuman{name = 'Adam', surname = 'SomeSurname', yearBirth = 1956, iqLevel = 100, " +
                "schedule = [[SUNDAY, sleep, eat, riding a horse], [Monday, hunt, eat, riding a horse]]}", actualResult);
    }


//   test Check Human Equals()
    @Test
    public void CheckTestHumanEqualsSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setYearBirth(1956);
        assertEquals(new Human("Adam", "SomeSurname", 1956), module);
        assertTrue(module.equals(new Human("Adam", "SomeSurname", 1956)));
    }
    @Test
    public void CheckTestHumanNotEqualsSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setYearBirth(1956);
        assertNotEquals(new Human("Adam", "OtherSurname", 1956), module);
        assertFalse(module.equals(new Human("Adam", "OtherSurname", 1956)));
    }


//   test Check Human HashCode()
    @Test
    public void testCheckHumanHashCodesEqualsSuccess() {
        assertEquals(new Human().hashCode(), module.hashCode());
    }
    @Test
    public void testCheckHumanHashCodesNotEqualsSuccess() {
        assertNotEquals(new Human("Adam", "SomeSurname").hashCode(), module.hashCode());
    }


//   test Check Human greetingPet()
    @Test
    public void testCheckHumanGreetingPetSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setFamily(new Family(new Human("Eve", "SomeSurname"), module));
        module.getFamily().addPet(new Pet(Species.DOG, "Rocky"));
        String nickName = module.getFamily().getPet().getNickname();
        assertTrue(module.greetingPet(nickName));
    }
    @Test
    public void testCheckHumanGreetingPetNOPetSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        String nickName = "noPet";
        assertFalse(module.greetingPet(nickName));
    }


//   test Check Human describingPet()
    @Test
    public void testCheckHumanDescribingPetSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setFamily(new Family(new Human("Eve", "SomeSurname"), module));
        module.getFamily().addPet(new Pet(Species.DOG, "Rocky"));
        assertTrue(module.describingPet());
    }
    @Test
    public void testCheckHumanDescribingPetNoPetSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        assertFalse(module.describingPet());
    }


//   test Check Human feedPet()
    @Test
    public void testCheckHumanFeedPetTimeToEatSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setFamily(new Family(new Human("Eve", "SomeSurname"), module));
        Pet pet = new Pet(Species.DOG, "Rocky");
        pet.setTrickLevel();
        module.getFamily().addPet(pet);
        assertTrue(module.feedPet(true));
    }
    @Test
    public void testCheckHumanFeedPetNotTimeToEatSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setFamily(new Family(new Human("Eve", "SomeSurname"), module));
        module.getFamily().addPet(new Pet(Species.DOG, "Rocky"));
        assertFalse(module.feedPet(false));
    }
    @Test
    public void testCheckHumanFeedPetTimeToEatButNoTrickLevelSetSuccess() {
        module.setName("Adam");
        module.setSurname("SomeSurname");
        module.setFamily(new Family(new Human("Eve", "SomeSurname"), module));
        module.getFamily().addPet(new Pet(Species.DOG, "Rocky"));
        assertFalse(module.feedPet(true));
    }

}