package com.javabasic.hw5;

public enum Species {
    CAT ("cat", false, 4, true),
    DOG ("dog",false, 4, true),
    PARROT ("parrot",true, 2, false),
    FISH ("fish",false, 0, true);

    String species;
    boolean canFly;
    int numberOfLegs;
    boolean hasFur;

    Species(){}
    Species(String species, boolean canFly, int numberOfLegs, boolean hasFur){
        this.species = species;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
