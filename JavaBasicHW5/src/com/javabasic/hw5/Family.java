package com.javabasic.hw5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private static final int MEMBERS_STATIC = 2;
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    private int membersTotal;

//  Constructors
    Family(Human mother, Human father){
       this.mother = mother;
       this.father = father;
       this.children = new Human[0];
       this.membersTotal = Family.MEMBERS_STATIC;
       // добавляем ссылку для mother(woman1) и для father(man1) на их семью (на эту семью)
       mother.setFamily(this);
       father.setFamily(this);
       // изменяем фамилию женщины на фамилию мужчины
       mother.setSurname(father.getSurname());
    }

//  Getters & Setters
    public Human getMother() { return mother; }
    public void setMother(Human mother) { this.mother = mother; }

    public Human getFather() { return father; }
    public void setFather(Human father) { this.father = father; }

    public Human[] getChildren() { return this.children; }
    public void setChildren(Human[] children) { this.children = children; }

    public Pet getPet() { return pet; }
    public void setPet(Pet pet) { this.pet = pet; }

    public int getMembersTotal() { return Family.MEMBERS_STATIC  + this.children.length; }
    public void setMembersTotal() { this.membersTotal = Family.MEMBERS_STATIC + this.children.length; }


//  @Override methods equals & hashCode
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Family family = (Family) obj;
        return this.mother.equals(family.mother) && this.father.equals(family.father)
                && Arrays.equals(this.children, family.children);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(this.mother, this.father);
        result = 31 * result + Arrays.hashCode(this.children);
        return result;
    }

//  @Override method toString
    @Override
    public String toString() {
        return "\nFamily{" +
                "\n\tmother = " + this.mother.toString() + "," +
                "\n\tfather = " + this.father.toString() + "," +
                "\n\tchildren = " + Arrays.toString(this.children) + "," +
                "\n\tpet = " + (this.pet == null ? "no pet" : pet.toString()) + "," +
                "\n\ttotal human members of family = " + membersTotal +
                "\n}";
    }

//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family obj before Garbage Collector will delete it : " + this);
    }


//  Family methods
    public boolean addPet(Pet pet){
        if(pet.getSpecies() != null && pet.getNickname() != null){
            pet.setFamily(this);
            this.setPet(pet);
            System.out.println("Pet has been added to the family!!!");
            return true;
        }
        System.out.println("To add the Pet to the family you have to set Species and Nickname of pet!!!");
        return false;
    }

    public int countFamilyMembers(){
        return this.getMembersTotal();
    }

    public boolean addChild(Human child) {
        if(child.getName() == null && child.getFamily() == null && child.getYearBirth() == -1) {
            System.out.println("To add child to the family you have to set Name, Surname and YearBirth for child!!!");
            return false;
        }
            child.setFamily(this);
            int newLength = this.children.length;
            Human[] newChildren = new Human[newLength+1];
            if(newChildren.length == 1) {
                newChildren[0] = child;
            }
            for (int i = 0; i <= newLength; i++){
                if(i != newLength){
                    newChildren[i] = this.children[i];
                    continue;
                }
                newChildren[i] = child;
            }
            this.children = Arrays.copyOf(newChildren, newLength+1);
            this.setMembersTotal();
            return true;
        }

    public boolean deleteChild(Human child){
        Human[] newChildren = new Human[this.children.length-1];
        int indexEl = 0;
        int indexOfObjToDelete = -1;
        for(Human elem : this.children){
            if(elem.equals(child) && elem.hashCode() == child.hashCode()){
                indexOfObjToDelete = indexEl;
                break;
            }
            indexEl++;
        }
        if(indexOfObjToDelete == -1) {
            System.out.println("There is no child " + child.getName() + " " + child.getSurname() +
                    " in the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".\n");
            return false;
        } else {
            for(int i = 0, newI = 0; i < this.children.length; i++){
                if(i == indexOfObjToDelete) {
                    continue;
                }
                newChildren[newI] = this.children[i];
                newI++;
            }
            this.children = Arrays.copyOf(newChildren, this.children.length-1);
            System.out.println("Child " + child.getName() + " " + child.getSurname() +
                    " has left the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".\n");
            child.setFamily(null);
            this.setMembersTotal();
            return true;
        }
    }

    public boolean deleteChild(int ind) {
        Human[] newChildren = new Human[this.children.length-1];

        if(ind <= 0 || ind > this.children.length) {
            System.out.println("There is no child " + ind + " in this family!!!");
            return false;
        } else {
            for(int i = 0, newI = 0; i < this.children.length; i++){
                if(i == ind-1) {
                    continue;
                }
                newChildren[newI] = this.children[i];
                newI++;
            }

            Human childGoing = this.children[ind-1];
            System.out.println("Child " + childGoing.getName() + " " + childGoing.getSurname() +
                    " has left the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".\n");
            childGoing.setFamily(null);

            this.children = Arrays.copyOf(newChildren, this.children.length-1);
            this.setMembersTotal();
            return true;
        }
    }

}