package com.javabasic.hw12.app.dao;

import com.javabasic.hw12.app.domain.family.Family;

import java.io.*;
import java.util.List;

public class Serializator {
    public boolean serialization (List<Family> families){
        boolean isSerializationDone = false;

        File file = new File("./FamilyCollection.data");

        ObjectOutputStream oos = null;
        FileOutputStream fos = null;

        try {
           fos = new FileOutputStream(file);
            if(fos != null){
                oos = new ObjectOutputStream(fos);
                oos.writeObject(families);
                isSerializationDone = true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(oos != null){
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return isSerializationDone;
    }

//    List<Family>
    public List<Family> deSerialization() throws InvalidObjectException {
        List<Family> families = null;
        File file = new File("./FamilyCollection.data");

        ObjectInputStream ois = null;
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(file);
            if(fis != null){
                ois = new ObjectInputStream(fis);
                families = (List<Family>) ois.readObject();
                return families;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(ois != null){
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        throw new InvalidObjectException("Object fail");
    }

}
