package com.javabasic.hw12.app.dao;

import java.io.Serializable;

public class SomeObject implements Serializable {
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
