package com.javabasic.hw12.app.domain.pet;

public enum Species {
    UNKNOWN ("unknown", false, -1, false),
    FISH ("fish",false, 0, false),
    CAT ("domestic cat", false, 4, true),
    DOG ("dog",false, 4, true),
    ROBOCAT ("robocat", false, 4, false);

    public String species;
    boolean canFly;
    int numberOfLegs;
    boolean hasFur;

    Species(){
        this.species = "UNKNOWN";
    }
    Species(String species, boolean canFly, int numberOfLegs, boolean hasFur){
        this.species = species;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }


    public String getSpeciesValName() {
        return species;
    }

    public boolean getIsCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean getIsHasFur() {
        return hasFur;
    }
}
