package com.javabasic.hw12.app.domain.pet;

public interface PetFoulable {
    String petDidFoul();
}
