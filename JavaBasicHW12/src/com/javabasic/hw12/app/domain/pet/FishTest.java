package com.javabasic.hw12.app.domain.pet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FishTest {
    private Fish module;

    @Before
    public void setUp() throws Exception {
        module = new Fish("Nemo");
        module.setTrickLevel(35);
    }


//  test Check Fish() no initialize
    @Test
    public void testCheckCFishEmptyGenderWomanSuccess(){
        Fish module = new Fish();
        assertEquals("fish", module.getSpecies().species);
    }


//  test Check Fish ToString()
    @Test
    public void testCheckFishToStringSuccess() {
        assertEquals("Pet{species='fish', nickname='Nemo', can fly='false', " +
                "number of legs='0', has fur='false', trickLevel='35', age='no info', " +
                "habits='no info', family='no info'}", module.toString());
    }


//  test Check Fish Equals()
    @Test
    public void testCheckFishEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckFishNotEqualsSuccess() {
        assertNotEquals(new Fish("Fish"), module);
    }


//  test Check Fish HashCode()
    @Test
    public void testCheckFishHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckFishHashCodeNotEqualsSuccess() {
        assertNotEquals(new Fish().hashCode(), module.hashCode());
    }


//  test Check Fish (Pet) abstract methods petResponding()
    @Test
    public void testCheckFishPetRespondingSuccess(){
        assertEquals("Hello, master! I am your fish Nemo. I need some oxygen!", module.petResponding());
    }
}