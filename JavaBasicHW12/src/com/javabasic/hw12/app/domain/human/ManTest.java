package com.javabasic.hw12.app.domain.human;

import com.javabasic.hw12.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw12.app.domain.pet.Dog;
import com.javabasic.hw12.app.domain.pet.Species;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ManTest {
    private Man module;

    @Before
    public void setUp() throws Exception{
        module = new Man("Adam", "Firstman");
        module.setIqLevel(99);
    }


//  test Check Man() no initialize
    @Test
    public void testCheckManEmptyGenderWomanSuccess(){
        Man module = new Man();
        assertEquals("man", module.getGender().gender);
    }

//  test Check Man ToString()
    @Test
    public void testCheckManToStringSuccess() {
        assertEquals("Human{gender='man', name='Adam', surname='Firstman', birthDate=no info', " +
                "iqLevel=99, schedule='no info', family='no info', " +
                "childrenList='no info', petsSet='no info'}", module.toString());
    }


//  test Check Man Equals()
    @Test
    public void testCheckManEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckManNotEqualsSuccess() {
        assertNotEquals(new Man(), module);
    }


//  test Check Man HashCode()
    @Test
    public void testCheckManHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckManHashCodeNotEqualsSuccess() {
        assertNotEquals(new Man().hashCode(), module.hashCode());
    }


//  test Check Man GreetPet()
    @Test
    public void testCheckManGreetPetButNoHavePetSuccess() {
        String result = "";
        if(module.getPetsSet() == null){
            result = "Have no pet yet..";
        }
        assertNull(module.getPetsSet());
        assertEquals("Have no pet yet..", result);
    }
    @Test
    public void testCheckManGreetSuccess() {
        Dog dog = new Dog("Rocky");
        PetsTreeSet pets = new PetsTreeSet();
        module.setPetsSet(pets);
        module.getPetsSet().addPetToPetsTreeSet(dog);
        String result = "";
        if(module.getPetsSet() != null){
            result = "I greet my pet";
        }
        assertNotNull(module.getPetsSet());
        assertEquals(1, module.getPetsSet().getPetsTreeSet().size());
        assertEquals("I greet my pet", result);
    }


//  test Check Man DescribePet()
    @Test
    public void testCheckManDescribePetButHaveNoPetSuccess() {
        String result = "";
        if(module.getPetsSet() == null){
            result = "Have no pet to describe";
        }
        assertNull(module.getPetsSet());
        assertEquals("Have no pet to describe", result);
    }
    @Test
    public void testCheckManDescribePetButNoHaveNoSuchPetSuccess() {
        Dog dog = new Dog("Rocky");
        PetsTreeSet pets = new PetsTreeSet();
        module.setPetsSet(pets);
        module.getPetsSet().addPetToPetsTreeSet(dog);
        String result = "";
        String somePetNickName = "SuperPet";
        if(!module.getPetsSet().findPet(Species.DOG, "Rocky").getNickname().equals(somePetNickName)){
            result = "I can describe my pet, but I have no such pet!";
        }
        assertNotNull(module.getPetsSet());
        assertEquals("I can describe my pet, but I have no such pet!", result);
    }
    @Test
    public void testCheckManDescribePetSuccess() {
        Dog dog = new Dog("Rocky");
        PetsTreeSet pets = new PetsTreeSet();
        module.setPetsSet(pets);
        module.getPetsSet().addPetToPetsTreeSet(dog);
        String result = "";
        if(module.getPetsSet() != null){
            result = "I describe my pet";
        }
        assertNotNull(module.getPetsSet());
        assertEquals("I describe my pet", result);
    }


//  test Check Man readTheBook()
    @Test
    public void testCheckManReadTheBookSuccess() {
        assertEquals("It's time for me to read the book.",
                module.readTheBook());
    }
}