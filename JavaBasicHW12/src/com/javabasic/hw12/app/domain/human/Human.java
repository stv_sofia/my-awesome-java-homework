package com.javabasic.hw12.app.domain.human;

import com.javabasic.hw12.app.domain.collectionChildren.HumanChildrenList;
import com.javabasic.hw12.app.domain.collectionHumanSchedule.HumanScheduleTreeMap;
import com.javabasic.hw12.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw12.app.domain.family.Family;
import com.javabasic.hw12.app.domain.pet.Species;

import java.io.Serializable;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

// implements Comparable
public abstract class Human implements Serializable {
    private Gender gender;
    private String name;
    private String surname;
    private long birthDate = -1;
    private int iqLevel = -1;

    private HumanScheduleTreeMap schedule;

    private Family family;
    private HumanChildrenList childrenList;
    private PetsTreeSet petsSet;

//  Constructors
    Human(){
        this.gender = Gender.UNKNOWN;
    }
    Human(Gender gender){
        this.gender = gender;
        this.iqLevel = generateIqLevel();
    }
    Human(Gender gender, String name, String surname) {
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.iqLevel = generateIqLevel();
    }
    Human(Gender gender, String name, String surname, String birthDate){
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.birthDate = convertBirthDateStringToMilli(birthDate);
        this.iqLevel = generateIqLevel();;
    }
    Human(Gender gender, String name, String surname, String birthDate, int iqLevel){
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.birthDate = convertBirthDateStringToMilli(birthDate);
        this.iqLevel = iqLevel;
    }

//  Getters & Setters
    public Gender getGender() { return gender;}
    public void setGender(Gender gender) { this.gender = gender;}

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getSurname() { return surname; }
    public void setSurname(String surname) { this.surname = surname; }

    public void setBirthDate(String birthDayStr) { this.birthDate = convertBirthDateStringToMilli(birthDayStr); }
    public String getBirthDate() { return convertBirthDateMilliToString(this.birthDate) ; }
    public long getBirthDateMilli() { return birthDate; }


    public int getIqLevel() { return iqLevel; }
    public void setIqLevel(int iqLevel) { this.iqLevel = iqLevel; }

    public HumanScheduleTreeMap getSchedule() { return schedule; }
    public void setSchedule() { this.schedule = new HumanScheduleTreeMap(); }

    public Family getFamily() { return family;}
    public void setFamily(Family family) { this.family = family;}

    public HumanChildrenList getChildrenList() { return childrenList; }
    public void setChildrenList(HumanChildrenList childrenList) { this.childrenList = childrenList; }

    public PetsTreeSet getPetsSet() { return petsSet; }
    public void setPetsSet(PetsTreeSet petsSet) { this.petsSet = petsSet;}


//  @Override method compareTo() (implement Comparable)
//    @Override
//    public int compareTo(Object obj) {
//        Human human = (Human) obj;
//        return (int) convertBirthDateStringToMilli(this.getBirthDate()) - (int) convertBirthDateStringToMilli(human.getBirthDate());
//    }


//  @Override methods toString(), + prettyFormatHuman()
    @Override
    public String toString() {
        if (this.getGender().gender.equals("unknown")) {
            return "Human{gender=" + gender + "}";
        } else {
            return "Human{" +
                    "gender='" + this.gender.gender + '\'' +
                    ", name='" + (this.getName() == null ? "no info" : this.getName()) + '\'' +
                    ", surname='" + (this.getSurname() == null ? "no info" : this.getSurname()) + '\'' +
                    ", birthDate=" + (this.birthDate == -1 ? "no info" : this.getBirthDate()) + '\'' +
                    ", iqLevel=" + this.iqLevel +
                    ", schedule=" + (this.schedule == null ? "'no info'" : this.schedule.getHumanScheduleTreeMap()) +
                    ", family=" + (this.getFamily() == null ?  "'no info'" : "'yes'") +
                    ", childrenList=" + (this.getChildrenList() == null ?  "'no info'" : this.getChildrenList()) +
                    ", petsSet=" + (this.getPetsSet() == null ?  "'no info'" : "'yes'") +
                    '}';
        }
    }

    public String prettyFormatHuman() {
        if(this.getGender().gender.equals("unknown")){
            return "\n\tHuman{gender='" + this.gender.gender + "'}";
        } else {
            return "\n\t\tHuman{gender='" + this.gender.gender + '\'' +
                    ", name='" + (this.getName() == null ? "no info" : this.getName()) + '\'' +
                    ", surname='" + (this.getSurname() == null ? "no info" : this.getSurname()) + '\'' +
                    ", birthDate='" + (this.birthDate == -1 ? "no info" : this.getBirthDate()) + '\'' +
                    ", iqLevel='" + this.iqLevel + '\'' +
                    ", family=" + (this.getFamily() == null ?  "'no info'" : "'yes'") +
                    ", children=" + (this.getChildrenList() == null ?  "'no info'" : "'yes'") +
                    ", pets=" + (this.getPetsSet() == null ?  "'no info'" : "'yes'") +
                    ", \n\t\t\t  schedule=" + (this.schedule == null ? "'no info'" : this.schedule.getHumanScheduleTreeMap()) +
                    "}";
        }
    }


//  @Override methods equals(), hashCode()
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human) obj;
        return getBirthDate() == human.getBirthDate() && getGender() == human.getGender()
                && Objects.equals(getName(), human.getName())
                && Objects.equals(getSurname(), human.getSurname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(getBirthDate(), getGender(), getName(), getSurname());
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human obj before Garbage Collector will delete it : " + this);
    }



//  Human abstract methods
    public abstract String greetPet(Species petSpecies, String nickName);



//  Human methods describePet()    |в ТЗ не указано, но я переопределю в наследниках, проедположим, что чел описывает только своего питомца|
    public String describePet(Species species, String nickName){
        return "I am not sure I have pet or not... You want me to describe my pet? )))";
    }



//  Human methods feedPet()
    public boolean feedPet(boolean isTimeToEat, Species species, String nickName, int trickLevel){
        if(isTimeToEat){
            System.out.println("Hmm... Let's feed this " + species.species + " " + nickName + "!");
                return true;
            }
        int randTrickLevel = this.generateIqLevel();
        if (trickLevel > randTrickLevel){
            System.out.println("BUT! pet trick level=" + trickLevel + " > random trick level=" + randTrickLevel);
                System.out.println("It's so canning! Let's feed this " + species.species + " " + nickName + "!");
                return true;
            }
            System.out.println("I think " + nickName + " is not hungry yet!");
            return false;
    }



//  Human methods generateIqLevel()
    public int generateIqLevel(){ return (int) (Math.random() * 101); }



//  Human methods convertBirthDateStringToMilli()
    public long convertBirthDateStringToMilli(String birthday){
        return LocalDate.parse(birthday, DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
    }



//  Human methods convertBirthDateMilliToString()
    public String convertBirthDateMilliToString(long birthDateMilli){
        LocalDate birthdayDate = Instant.ofEpochMilli(birthDateMilli).atZone(ZoneId.systemDefault()).toLocalDate();
        return birthdayDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }



//  Human methods describeAge()
    public String describeAge() {
        LocalDate birthdayDate = LocalDate.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault());
        Period period = Period.between(birthdayDate, LocalDate.now());
        return "A person has lived " + period.getYears() + " years, "
                + period.getMonths() + " months , " + period.getDays() + " days";
    }

}