package com.javabasic.hw12.app.domain.family;

import com.javabasic.hw12.app.domain.human.Human;

public abstract interface HumanCreator {
    public abstract int generateGender();
    public abstract Human bornChild(int randGender);
}