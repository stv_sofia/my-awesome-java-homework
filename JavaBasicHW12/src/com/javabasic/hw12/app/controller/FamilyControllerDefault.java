package com.javabasic.hw12.app.controller;

import com.javabasic.hw12.app.domain.collectionChildren.HumanChildrenList;
import com.javabasic.hw12.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw12.app.domain.family.Family;
import com.javabasic.hw12.app.domain.human.Human;
import com.javabasic.hw12.app.domain.pet.Pet;
import com.javabasic.hw12.app.service.FamilyServiceDefault;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class FamilyControllerDefault implements FamilyController{
    // - содержит поле(ссылку) FamilyServiceDefault для доступа к методам бизнес-логики приложения.
    private FamilyServiceDefault familyServiceDefault;

    public FamilyControllerDefault(FamilyServiceDefault familyServiceDefault){
        this.familyServiceDefault = familyServiceDefault;
    }

    // - Класс FamilyControllerDefault - должен иметь все те же методы, которые есть в FamilyControllerDefault.

    //1. Возвращает список всех семей List<Family>.
    @Override
    public List<Family> getAllFamiliesList(){
        return familyServiceDefault.getAllFamiliesList();
    }


    //2. Выводит на экран все семьи (в индексированном списке) со всеми членами семьи.
    @Override
    public void displayAllFamilies(){
        familyServiceDefault.displayAllFamilies();
    }


    //3. Находит семью по указанному индексу, - возвращает семью по указанному индексу,
    // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.
    @Override
    public Family getFamilyByIndex(int index) {
        return familyServiceDefault.getFamilyByIndex(index);
    }


    //4. Удаляет семью с заданным индексом,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamilyByIndex(int index) {
        return familyServiceDefault.deleteFamilyByIndex(index);
    }


    //5. Удаляет семью если такая существует в списке,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamily(Family family) {
        return  familyServiceDefault.deleteFamily(family);
    }


    //6. Обновляет существующую семью в БД если такая уже существует,
    // - сохраняет семью в конец списка - если семьи в БД нет.
    @Override
    public void saveFamily(Family family) {
        familyServiceDefault.saveFamily(family);
    }


    //7. Возвращает количество семей в БД.
    @Override
    public int count(){
        return familyServiceDefault.count();
    }


    //7.1. Загружает List<Family> (список всех семей) из файла.
    @Override
    public List<Family> loadAllFamiliesDataFromFile(){
        return familyServiceDefault.loadAllFamiliesDataFromFile();
    }


    //7.2. сохраняет List<Family> (список всех семей) в файл.
    @Override
    public boolean saveAllFamiliesDataToFile(List<Family> families){
        return familyServiceDefault.saveAllFamiliesDataToFile(families);
    }


    //8.  Находит семьи с количеством людей больше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей больше чем указанное
    public List<Family> getFamiliesBiggerThan(int members) {
        return familyServiceDefault.getFamiliesBiggerThan(members);
    }


    //9.  Находит семьи с количеством людей меньше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей меньше чем указанное
    public List<Family> getFamiliesLessThan(int members) {
        return familyServiceDefault.getFamiliesLessThan(members);
    }


    //10.  Подсчитывает число семей с количеством людей равное переданному числу.
    public int countFamiliesWithMembersNumber(int members){
        return familyServiceDefault.countFamiliesWithMembersNumber(members);
    }


    //11.  Создает новую семью (принимает 2 параметра типа Human) - создает новую семью, - сохраняет в БД.
    public void createNewFamily(Human woman, Human man){
        familyServiceDefault.createNewFamily(woman, man);
    }


    //12. Создает ребенка семьей (принимает Family и 2 типа String: мужское и женское имя)
    //  - в данной семье появляется новый ребенок с учетом данных родителей,
    //  - если рожденный ребенок мальчик - ему присваивается мужское имя, если девочка - женское,
    //  - информация о семье обновляется в БД, - метод возвращает обновленную семью.
    public Family bornChild(Family family, String womanName, String manName, int limitChildren) throws FamilyOverflowException {
        int index = familyServiceDefault.getAllFamiliesList().indexOf(family)+1;

        this.setNewChildrenListForFamilyIfEmpty(index);

        if(familyServiceDefault.getFamilyByIndex(index).getChildrenList().getChildrenList().size() >= limitChildren) {
            throwLimitForChildrenError(limitChildren);
            return null;
        }

        return familyServiceDefault.bornChild(family, womanName, manName);
    }


    //13. Усыновляет ребенка (принимает 2 параметра: Family, Human) - метод возвращает обновленную семью,
    //  - в данной семье сохраняется данный ребенок, - информация о семье обновляется в БД.
    public Family adoptChild(Family family, Human child, int limitChildren) {
        int index = familyServiceDefault.getAllFamiliesList().indexOf(family)+1;

        this.setNewChildrenListForFamilyIfEmpty(index);

        if(familyServiceDefault.getFamilyByIndex(index).getChildrenList().getChildrenList().size() >= limitChildren) {
            throwLimitForChildrenError(limitChildren);
            return null;
        }

        return familyServiceDefault.adoptChild(family, child);
    }


    //14. Удаляет детей старше чем (принимает int)
    // - во всех семьях удаляются дети, которые старше указанно возраста, - информация обновляется в БД.
    public void deleteAllChildrenOlderThen(int yearsOld){
        familyServiceDefault.deleteAllChildrenOlderThen(yearsOld);
    }


    //15.  Возвращает список домашних животных, которые живут в семье (принимает индекс семьи),
    public PetsTreeSet getPets(int index){
        return familyServiceDefault.getPets(index);
    }


    //16. Добавляет питомца в семью (принимает индекс семьи и параметр Pet)
    // - добавляет нового питомца в семью,  - обновляет данные в БД.
    public boolean addPet(int index, Pet pet){
        return familyServiceDefault.addPet(index, pet);
    }


    //17. Удаляет питомца (принимает индекс семьи и параметр Pet) - удаляет питомца из семьи,  - обновляет данные в БД.
    public boolean deletePet(int index, Pet pet){
        return familyServiceDefault.deletePet(index, pet);
    }




//  Дополнительные методы для ConsoleApp >>>
//    IsDbEmpty() проверяет если коллекция FamilyCollection пуста
    public boolean IsDbEmpty() {
        if(familyServiceDefault.getAllFamiliesList() == null || familyServiceDefault.getAllFamiliesList().size() == 0){
            System.out.println("The DB Families is empty! \n0 >> Fill Families DB with new test data, or  " +
                    "\n1 >> Load Families DB from file (if such file or directory");
            return true;
        }
        return false;
    }



//    setNewFamilyCollection()  создает новую пустую FamilyCollection
    public List<Family> setNewFamilyCollection(){
        return familyServiceDefault.setNewFamilyCollection();
    }



//    setNewChildrenListForFamilyIfEmpty()  создает новый пустой ChildrenList для семьи если еще ре был создан
    public void setNewChildrenListForFamilyIfEmpty(int indexOfTheFamily){
        if(familyServiceDefault.getFamilyByIndex(indexOfTheFamily).getChildrenList() == null){
            familyServiceDefault.getFamilyByIndex(indexOfTheFamily).setChildrenList(new HumanChildrenList());

            familyServiceDefault.getFamilyByIndex(indexOfTheFamily).getMother()
                    .setChildrenList(familyServiceDefault.getFamilyByIndex(indexOfTheFamily).getChildrenList());
            familyServiceDefault.getFamilyByIndex(indexOfTheFamily).getFather()
                    .setChildrenList(familyServiceDefault.getFamilyByIndex(indexOfTheFamily).getChildrenList());
        }
    }



//    checkInputDataInteger()  проверяет введенные данные на число
    public int checkInputDataInteger(String inputData, int max){
        int inputDataInteger = -1;
        try {
            inputDataInteger = Integer.parseInt(inputData);
            if(inputDataInteger < 0 || inputDataInteger > max) {
                System.out.println("!WRONG INPUT! >> Enter positive integer only! In range of DB items! or In range of menu!");
                inputDataInteger = -1;
            }
        } catch (NumberFormatException nfe){
            System.out.println("!ERROR! NumberFormatException! >> Enter integer only!");   // System.out.println(nfe.toString());
        }
        return inputDataInteger;
    }



//    checkInputDataInteger()  проверяет введенные данные на число + маркер(год/месяц/день)
    public String checkInputDataIntegerYMD (String inputData, String marker) {
        int inputDataInteger = -1;
        if(marker.equals("Y")){
            try{
                inputDataInteger = Integer.parseInt(inputData);
                if(inputDataInteger < 1923 || inputDataInteger > 2003){
                    System.out.println("!WRONG INPUT! >> The year allowed [1923:2003]!");
                    inputData = "";
                }
            } catch (NumberFormatException nfe) {
                System.out.println("!ERROR! NumberFormatException! >> Enter integer only!");
                inputData = "";
            }
        }

        if(marker.equals("M")){
            try{
                inputDataInteger = Integer.parseInt(inputData);
                if(inputDataInteger < 1 || inputDataInteger > 12){
                    System.out.println("!WRONG INPUT! >> The month allowed [01:12]!");
                    inputData = "";
                }
            } catch (NumberFormatException nfe) {
                System.out.println("!ERROR! NumberFormatException! >> Enter integer only!");
                inputData = "";
            }
        }

        if(marker.equals("D")){
            try{
                inputDataInteger = Integer.parseInt(inputData);
                if(inputDataInteger < 1 || inputDataInteger > 31){
                    System.out.println("!WRONG INPUT! >> The Day allowed [01:31]!");
                    inputData = "";
                }
            } catch (NumberFormatException nfe) {
                System.out.println("!ERROR! NumberFormatException! >> Enter integer only!");
                inputData = "";
            }
        }
        return inputData;
    }



//    checkInputDataChars()  проверяет введенные данные на только символы
    public String checkInputDataChars(String inputData, int maxLength){
        if(!inputData.matches("[\\p{L}| ]+") || inputData.length() > maxLength){
            System.out.println("!WRONG INPUT! >> Should content characters only, maximum Length: " + maxLength + " characters!");
            return "";
        }
        return inputData;
    }



//     checkInputDataBirthDayString()  проверяет введенные данные на соответствие нужного строчного формата даты
    public String checkInputDataBirthDayString(String birthdayString) {
        try {
            long birthDateMilli = LocalDate.parse(birthdayString, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                    .atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();

            if( (Instant.ofEpochMilli(birthDateMilli).atZone(ZoneId.systemDefault()).toLocalDate())
                    .isAfter(LocalDate.now()) ){
                System.out.println("!WRONG INPUT! >> Date is after today!");
                birthdayString = "";
            }
        } catch (DateTimeParseException e) {
//            e.printStackTrace();
            System.out.println("!ERROR! >> " + e.getLocalizedMessage());
            System.out.println("!WRONG INPUT! >> Check day/month/year you enter correct!");
            birthdayString = "";
        }

        return birthdayString;
    }



//     throwLimitForChildrenError()  бросает исключение если превышен лимит детей в семье
    public void throwLimitForChildrenError(int limitForChildren) {
        try {
            throw new FamilyOverflowException().familyOverflowException(limitForChildren);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}