package com.javabasic.hw12.app;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        ConsoleApp consoleApp = new ConsoleApp();
        Scanner scanner = new Scanner(System.in);

        String userString = "";
        int numberOfActionMainMenu = -1;

        do {
            consoleApp.showMainMenu();
            System.out.print("Your choice >>> ");
            userString = scanner.nextLine().toLowerCase();

            if(userString.equals("exit")) break;

            numberOfActionMainMenu = consoleApp.checkInputDataInteger(userString, 10);

            if (numberOfActionMainMenu != -1) {
                consoleApp.implementTheSelectedAction(numberOfActionMainMenu); }

        } while (true);

    }
}