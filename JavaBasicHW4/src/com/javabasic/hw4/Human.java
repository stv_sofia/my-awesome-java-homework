package com.javabasic.hw4;

import java.util.Arrays;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int yearBirth = -1;
    private int iqLevel = -1;
    private String[][] schedule;
    private Family family;

//  Constructors
    Human(){}
    Human(String name, String surname, int yearBirth){
        this.name = name;
        this.surname = surname;
        this.yearBirth = yearBirth;
    }
    Human(String name, String surname, int yearBirth, int iqLevel, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.yearBirth = yearBirth;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
    }

//  Getters & Setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYearBirth() {
        return yearBirth;
    }
    public void setYearBirth(int yearBirth) {
        this.yearBirth = yearBirth;
    }

    public int getIqLevel() {
        return iqLevel;
    }
    public void setIqLevel(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public String[][] getSchedule() { return this.schedule; }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() { return this.family; }
    public void setFamily(Family family) { this.family = family;}


//  @Override methods equals & hashCode
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Human human = (Human) obj;
        return this.name.equals(human.name) && this.surname.equals(human.surname) && this.yearBirth == human.yearBirth;
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.surname, this.yearBirth);
    }

//  @Override method toString
    @Override
    public String toString() {
        return "\n\tHuman{" +
                "name = '" + (this.name == null ? "no info" : this.name) + '\'' +
                ", surname = '" + (this.surname == null ? "no info" : this.surname) + '\'' +
                ", yearBirth = " + (this.yearBirth == -1 ? "no info" : this.yearBirth) +
                ", iqLevel = " + (this.iqLevel == -1 ? "no info" : this.iqLevel) +
                ", schedule = " + (this.schedule == null ? "no info" : Arrays.deepToString(this.schedule)) +
                "}";
    }


//  Human methods
    public void greetingPet(String petNickName){
        if(this.family.getPet() == null){
            System.out.println("Have no pet yet..");
        }
        System.out.printf("Hello, my dear %s! %n", this.family.getPet().getNickname());
    }

    public void describingPet(){
        if(this.family.getPet() == null){
            System.out.println("Have no pet yet..");
        }
        int trickLevel = this.family.getPet().getTrickLevel();
        String trickLevelStr = this.family.getPet().getTrickLevelString(trickLevel);

        System.out.printf("I have pet. It's a %s called %s. It's %d years old. And it's %s :)",
                this.family.getPet().getSpecies(), this.family.getPet().getNickname(),
                this.family.getPet().getAge(), trickLevelStr);
    }

    public boolean feedPet(boolean isTimeToEat){
        if(isTimeToEat){
            System.out.println("Hmm... Let's feed may " +
                    this.family.getPet().getSpecies() + " " + this.family.getPet().getNickname() + "!");
            return true;
        }
        int randTrickLevel = this.family.getPet().generateTrickLevel();
        if (this.family.getPet().getTrickLevel() > randTrickLevel){
            System.out.println("BUT! pet trick level=" +
                    this.family.getPet().getTrickLevel() + " > random trick level=" + randTrickLevel);
            System.out.println("It's so canning! Let's feed my " +
                    this.family.getPet().getSpecies() + " " + this.family.getPet().getNickname() + "!");
            return true;
        }
        System.out.println("I think my " + this.family.getPet().getNickname() + " is not hungry yet!");
        return false;
    }
}