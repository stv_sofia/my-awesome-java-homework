package com.javabasic.hw4;

import java.util.Arrays;

public class HappyFamilyMain {
    public static void main(String[] args) {
//      родители женщины Adel
        Human motherFirst1 = new Human("Eve1", "Firstwoman", 1935);
        Human fatherFirst1 = new Human("Adam1", "Firstwoman", 1936);
        Family fam1 = new Family(motherFirst1, fatherFirst1);

//      создаем женщину Adel, добавляем ее как ребенка в семью fam1, выводим ее и ее семью
        Human woman1 = new Human("Adel", fam1.getFather().getSurname(), 1955);
//      ссылка добавленному ребенку на его семью добавляется в (Family) методе addChild()
        fam1.addChild(woman1);
        System.out.println("Woman >>  " + woman1);
        System.out.println("Woman's family >>  " + woman1.getFamily());


//      родители мужчины Cain
        Human motherFirst2 = new Human("Eve2", "Firstman", 1930);
        Human fatherFirst2 = new Human("Adam2", "Firstman", 1929);
        Family fam2 = new Family(motherFirst2, fatherFirst2);

//      создаем мужчину Cain, добавляем его как ребенка в семью fam1, выводим его и его семью
        Human man1 = new Human("Cain", fam2.getFather().getSurname(), 1950, 88,
                new String[][]{
                        {"Sunday", "sleep", "eat", "riding a horse"},
                        {"Monday", "hunt", "eat", "riding a horse"}} );
        fam2.addChild(man1);
        System.out.println("\nMan >> " + man1);
        System.out.println("Man's family >> " + man1.getFamily());


//      создаем и выводим family1 (Adel & Cain)
        Family family1 = new Family(woman1, man1);
        System.out.println("\nFamily Adel & Cain. Family1 >>  " + family1);

//      проверяем наличие детей у family1 (сразу после создания семьи - детей еще нет))
        System.out.println("\nChecking children. Just created family (Adel & Cain) >>  \n" +
                "children = " + Arrays.toString(family1.getChildren()) + ", " +
                "number of children: " + family1.getChildren().length);

//      создаем ребенка, >> добавляем в массив детей в family1
        Human childToAdd1 = new Human("Arnold", family1.getFather().getSurname(), 1975);
        family1.addChild(childToAdd1);
        System.out.println("\nAfter adding a child. Children of Family1 >>  \nchildren = "
                + Arrays.toString(family1.getChildren()));

//      проверяем добавилась ли ссылка добавленному ребенку на его семью (добавляется в методе addChild())
        System.out.println("\nChild " + childToAdd1.getName() + " has family (Family1) >>  " + childToAdd1.getFamily());

//      создаем еще 2х детей, добавляем в массив детей в family1, выводим семью family1
        Human childToAdd2 = new Human("Helen", family1.getFather().getSurname(), 1976);
        family1.addChild(childToAdd2);

        Human childToAdd3 = new Human("Bill", family1.getFather().getSurname(), 1978);
        family1.addChild(childToAdd3);

        System.out.println("\nAfter adding more 2 children. Family1 >>  " + family1);


//      добавляем в family1 домашнее животное и выводим его кличку через family1
//      ссылка животному на семью хозяев добавдяеться в (Family) методе addPet()
        Pet dogBob = new Pet("dog", "Bob",
                5, 77, new String[]{"eat", "drink", "sleep"});
        family1.addPet(dogBob);
        System.out.println("\nFamily1 got new pet!\n" +
                "After adding the pet >>  \nFamily1 pet = "
                + family1.getPet().getSpecies() + " "
                + family1.getPet().getNickname());

//      поверяем колличество членов семьи через family1
        System.out.println("\nChecking Family method countFamilyMembers() >>");
        System.out.println("Family1 members: " + family1.countFamilyMembers() + " humans & the pet "
                + family1.getPet().getSpecies() + " " + family1.getPet().getNickname() + " :)");


//      проверяем (Family) метод deleteChild()
        System.out.println("\nFamily of " + family1.getFather().getName() + " " +
                family1.getFather().getSurname() + " for now >>  " + family1);

        System.out.println("\n>> Checking (Family) method deleteChild() >>  ");
        family1.deleteChild(man1);
        family1.deleteChild(woman1);

        family1.deleteChild(childToAdd1);
        System.out.println("After child " + childToAdd1.getName() + " left the family of " +
                family1.getFather().getName() + " " + family1.getFather().getSurname() +
                ", checking what's his family for now >>  \nfamily = " + childToAdd1.getFamily());
        System.out.println("So... No Family = No Pet :)");


        System.out.println("\nAfter child left the family, " + "\nFamily of " +
                family1.getFather().getName() + " " + family1.getFather().getSurname() + " for now >>  " + family1);


//      проверяем Pet и его методы
        System.out.println("\n>> Checking Pet and it's methods: ");

//      объект класса Pet без инициализации
        System.out.println("Checking Pet without initial >>  ");
        Pet catTom = new Pet();
        System.out.println(catTom);
        catTom.setSpecies("cat");
        catTom.setNickname("Tom");
        System.out.println("After setting Species & Nickname >>  ");
        System.out.println(catTom);

//      у dogRocky нет хозяев (нет семьи)
        Pet dogRocky = new Pet("dog", "Rocky");
        System.out.println("\nPet with no family >> ");
        System.out.println("I am " + dogRocky);
        System.out.println(dogRocky.isPetHasOwners() ? "I have family!" : "I have no family yet...");
        System.out.println(dogRocky.tellOwnersName());

//      у dogBob есть семья family1
        System.out.println("\nPet has the family >> ");
        System.out.println("I am " + family1.getPet());
        System.out.println(dogBob.isPetHasOwners() ? "I have family!" : "I have no family yet...");
        System.out.println(dogBob.tellOwnersName());

        System.out.print("\nPet responding >>  \n");
        dogBob.petResponding();
        System.out.print("Pet eating >>  \n");
        dogBob.petEating();
        System.out.print("Pet did foul >>  \n");
        dogBob.petDoingFoul();

//      меняем уровень хитрости домашнего животного
        System.out.println("\nChecking pet TrickLevel >>  ");
        System.out.print("TrickLevel for " + dogBob.getNickname() +  " now is: ");
        System.out.println(dogBob.getTrickLevel());
        dogBob.setTrickLevel();
        System.out.print("New TrickLevel for " + dogBob.getNickname() + " after generated & set: ");
        System.out.println(dogBob.getTrickLevel());
        System.out.println("Pet of family1 info: " + dogBob);


//      проверяем Human методы
//      метод описать питомца
        System.out.println("\n>> Checking Human's methods: ");
        System.out.println("Describing my pet >>> ");
        System.out.print("I am " + man1.getName() + ". ");
        man1.describingPet();
//      или можно через семью
//      System.out.print("I am " + family1.getFather().getName() + ". ");
//      family1.getFather().describingPet();

//      метод поприветствовать питомца
        System.out.println("\n\nGreeting my pet >>> ");
        man1.greetingPet(man1.getFamily().getPet().getNickname());

//      метод покормить домашнее животное
//      время кормить
        System.out.println("\nFeeding the pet, it's time to eat >>> ");
        if(man1.feedPet(true)){
            System.out.println("Result = I fed my pet.");
        } else {
            System.out.println("Result = I didn't feed my pet.");
        }
//      не время кормить
        System.out.println("\nFeeding the pet, it's NOT time to eat >>> ");
        if(man1.feedPet(false)){
            System.out.println("Result = I fed my pet.");
        } else {
            System.out.println("Result = I didn't feed my pet.");
        }

    }
}