package com.javabasic.hw4;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private static final int membersStatic = 2;
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    private int membersTotal;

//  Constructors
    Family(Human mother, Human father){
       this.mother = mother;
       this.father = father;
       this.children = new Human[0];
       this.membersTotal = Family.membersStatic;
       // добавляем ссылку для mother(woman1) и для father(man1) на их семью (на эту семью)
       mother.setFamily(this);
       father.setFamily(this);
       // изменяем фамилию женщины на фамилию мужчины
       mother.setSurname(father.getSurname());
    }

//  Getters & Setters
    public Human getMother() { return mother; }
    public void setMother(Human mother) { this.mother = mother; }

    public Human getFather() { return father; }
    public void setFather(Human father) { this.father = father; }

    public Human[] getChildren() { return this.children; }
    public void setChildren(Human[] children) { this.children = children; }

    public Pet getPet() { return pet; }
    public void setPet(Pet pet) { this.pet = pet; }

    public int getMembersTotal() { return membersTotal; }
    public void setMembersTotal() { this.membersTotal = Family.membersStatic + this.children.length; }


//  @Override methods equals & hashCode
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Family family = (Family) obj;
        return this.mother.equals(family.mother) && this.father.equals(family.father)
                && Arrays.equals(this.children, family.children);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(this.mother, this.father);
        result = 31 * result + Arrays.hashCode(this.children);
        return result;
    }

//  @Override method toString
    @Override
    public String toString() {
        return "\nFamily{" +
                "\n\tmother = " + this.mother.toString() + "," +
                "\n\tfather = " + this.father.toString() + "," +
                "\n\tchildren = " + Arrays.toString(this.children) + "," +
                "\n\tpet = " + (this.pet == null ? "no pet" : pet.toString()) + "," +
                "\n\ttotal human members of family = " + membersTotal +
                "\n}";
    }


//  Family methods
    public void addPet(Pet pet){
        pet.setFamily(this);
        this.setPet(pet);
    }

    public int countFamilyMembers(){
        return this.getMembersTotal();
    }

    public void addChild(Human child) {
        child.setFamily(this);
        int newLength = this.children.length;
        Human[] newChildren = new Human[newLength+1];
        if(newChildren.length == 1) {
            newChildren[0] = child;
        }
        for (int i = 0; i <= newLength; i++){
            if(i != newLength){
                newChildren[i] = this.children[i];
                continue;
            }
            newChildren[i] = child;
        }
        this.children = Arrays.copyOf(newChildren, newLength+1);
        this.setMembersTotal();
    }

    public void deleteChild(Human child){
        Human[] newChildren = new Human[this.children.length-1];
        int indexEl = 0;
        int indexOfObjToDelete = -1;
        for(Human elem : this.children){
            if(elem.equals(child)){
                indexOfObjToDelete = indexEl;
                break;
            }
            indexEl++;
        }
        if(indexOfObjToDelete == -1) {
            System.out.println("There is no child " + child.getName() + " " + child.getSurname() +
                    " in the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".\n");
        } else {
            for(int i = 0, newI = 0; i < this.children.length; i++){
                if(i == indexOfObjToDelete) {
                    continue;
                }
                newChildren[newI] = this.children[i];
                newI++;
            }
            this.children = Arrays.copyOf(newChildren, this.children.length-1);
            System.out.println("Child " + child.getName() + " " + child.getSurname() +
                    " has left the family of " + this.getFather().getName() + " " + this.getFather().getSurname() + ".\n");
            child.setFamily(null);
            this.setMembersTotal();
        }

    }
}