package com.javabasic.hw4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species;
    private String nickname;
    private int age = -1;
    private int trickLevel = -1;
    private String[] habits;
    private Family family;

//  Constructors
    Pet(){}
    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

//  Getters & Setters
    public String getSpecies() {
        return species;
    }
    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }
    public void setTrickLevel() {this.trickLevel = generateTrickLevel();}

    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public Family getFamily() { return family; }
    public void setFamily(Family family) { this.family = family;}


//  @Override methods equals & hashCode
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;

        Pet pet = (Pet) obj;
        return this.getSpecies().equals(pet.getSpecies())
                && this.getNickname().equals(pet.getNickname())
                && this.getAge() == pet.getAge();
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.species, this.nickname, this.age);
    }

//  @Override method toString
    @Override
    public String toString() {
        if(this.species == null && this.nickname == null){
            return "no info of pet";
        }
        return "Pet{" +
                "species='" + (this.species == null ? "no info" : this.species)  + '\'' +
                ", nickname='" + (this.nickname == null ? "no info" : this.nickname)  +  '\'' +
                ", age=" + (this.age == -1 ? "no info" : this.age) +
                ", trickLevel=" + (this.trickLevel == -1 ? "no info" : this.trickLevel) +
                ", habits=" + (this.habits == null ? "no info" : Arrays.toString(this.habits)) +
                '}';
    }


//  Pet methods
    public int generateTrickLevel(){ return (int) (Math.random() * 101); }
    public String getTrickLevelString(int trickLevel){
        return trickLevel > 50 ? "very canning one" : "almost not canning";
    }

    public void petResponding(){
        System.out.printf("Hello, master! I am yor %s %s. And I miss you so!%n",
                this.getSpecies(), this.getNickname());
    }

    public void petEating(){
        System.out.printf("I'm %s %s. And I'm eating!%n",
                this.getSpecies(), this.getNickname());
    }

    public void petDoingFoul(){
        System.out.printf("I'm %s %s. I did foul) Needs to cover tracks well...%n",
                this.getSpecies(), this.getNickname());
    }

    public boolean isPetHasOwners(){
        return (this.family != null);
    }

    public String tellOwnersName(){
        if(this.isPetHasOwners()){
            String str1 = "I have my owner-mother: " + this.family.getMother().getName() +
                    ", my owner-father: " + this.family.getFather().getName();
            String str2 = " and i have no owner-child yet!";
            String str3 = "";
            if(this.family.getChildren().length == 0){
                return str1 + str2;
            }
            int i = 1;
            for(Human child : family.getChildren()){
                str3 = str3 + ", my owner-child " + i++ + ": "+ child.getName();
            }
            return str1 + str3;
        }
        return "I still looking for my owners!";
    }
}