package com.javabasic.hw7;

import java.util.List;
import java.util.TreeSet;

public class App {
    public static void main(String[] args) {
//  пустой new Human()
        System.out.print(">>> Creating (empty) new Human() >>> ");
        System.out.println(new Human() {
            @Override public String greetPet(Species species, String nickName){return "";}
        });

//  родители Man & Woman
        System.out.println("\n>>> Creating (Woman)Eve & (Man)Adam >>> ");
        Woman firstWoman = new Woman("Eve", "Firstwoman", 1965);
        System.out.println("Woman: " + firstWoman);
        System.out.println("Check equals of not equals objects of women: "
                + firstWoman.equals(new Woman("Eve", "Secondwoman")));
        System.out.println("Check equals of the same object of women: "
                + firstWoman.equals(firstWoman));

        Man firstMan = new Man("Adam", "Firstman",1960);
        System.out.println("\nMan: " + firstMan);
        System.out.println("Check equals of not equals objects of man: "
                + firstMan.equals(new Man("Adam", "Secondman")));
        System.out.println("Check equals of the same object of man: "
                + firstMan.equals(firstMan));


//  добавим Man (можно и Woman) рассписание
        System.out.println("\n\n>>> Lets add for Man schedule: ");
        // пустое
        firstMan.setSchedule();
        System.out.println("Schedule of Man for now: " + firstMan.getSchedule().getHumanScheduleTreeMap());

        // заполняем по дням
        System.out.println("\nLets add some daily task for Man schedule: ");
        firstMan.getSchedule().addTaskOfTheDay(DayOfWeek.SUNDAY, "travel to America");
        firstMan.getSchedule().addTaskOfTheDay(DayOfWeek.SUNDAY, "go to the sport club");
        firstMan.getSchedule().addTaskOfTheDay(DayOfWeek.MONDAY, "travel to Italy");
        firstMan.getSchedule().addTaskOfTheDay(DayOfWeek.MONDAY, "go to eat pizza");
        firstMan.getSchedule().addTaskOfTheDay(DayOfWeek.TUESDAY, "sleep all day");
        System.out.println("Schedule of Man after it: \n" + firstMan.getSchedule().getHumanScheduleTreeMap());

        // удаляем по дню и делу
        System.out.println("\nLets delete daily task for SUNDAY (go to the sport club)");
        firstMan.getSchedule().removeTaskOfTheDay(DayOfWeek.SUNDAY,"go to the sport club");
        System.out.println("Schedule of Man after it: \n" + firstMan.getSchedule().getHumanScheduleTreeMap());

        System.out.println("\nMan for now: " + firstMan);



//  проверяем только Man & только Woman method
        System.out.println("\n\n>>> Checking only Man & only Woman method: ");
        System.out.println("I'm woman " +firstWoman.getName() + " >>> ");
        System.out.println(firstWoman.travelToSeaSide());
        System.out.println("I'm man " + firstMan.getName() + " >>> ");
        System.out.println(firstMan.readTheBook());



//  семья >>>
        System.out.println("\n\n>>> Creating Family of Adam & Eve >>> ");
        Family fam1 = new Family(firstWoman, firstMan);
        System.out.println("New family: " + fam1);

        System.out.println("\nChecking family's links for Adam & Eve >>> ");
        System.out.println("Family of Eve: " + firstWoman.getFamily());
        System.out.println("Family of Adam: " + firstMan.getFamily());



//  будущие дети >>>  генерируем пол ребенка, добавляем в семью fam1, выводим ребенка, семью
        int randGender = fam1.generateGender();
        if (randGender <= 50) {
            fam1.addChildToList( (Woman) fam1.bornChild(randGender));
        } else {
            fam1.addChildToList( (Man) fam1.bornChild(randGender));
        }
        System.out.println("\n\n>>> New born child has been added to the Family: "
                + fam1.getChildrenList().getChildrenList());
        System.out.println("\nFamily after added new child: " + fam1);


//  генерируем пол еще 2-х детей и добавляем в семью fam1, выводим детей потом семью
//  iqLevel у всех детей будет одиаковым по ТЗ |(motherIQ + fatherIQ) / 2|, можно потом засетить, что я и сделала для первого ребенка
        randGender = fam1.generateGender();
        if (randGender <= 50) {
            fam1.addChildToList( (Woman) fam1.bornChild(randGender));
        } else {
            fam1.addChildToList( (Man) fam1.bornChild(randGender));
        }
        randGender = fam1.generateGender();
        if (randGender <= 50) {
            fam1.addChildToList( (Woman) fam1.bornChild(randGender));
        } else {
            fam1.addChildToList( (Man) fam1.bornChild(randGender));
        }
        fam1.getChildrenList().getChildrenList().get(0).setIqLevel(98);
        fam1.getChildrenList().getChildrenList().get(0).setYearBirth(2021);
        System.out.println("\n>>> Family after adding 2 more children " +
                "and changing for child1 IqLevel(to 98), YearBirth(to 2021): " + fam1);
        System.out.println("Family children for now: "
                + fam1.getChildrenList().getChildrenList());



//      проверяем (Family) метод deleteChild(index)
        System.out.println("\n\n>>> Checking (Family) method deleteChild(index) >>> ");
        System.out.println("Children of " + fam1.getFather().getName() + " " +
                fam1.getFather().getSurname() + " for now: " + fam1.getChildrenList().getChildrenList() + "\n");
//      индекс задаем по человечески) т е если нужно удалить ребенка 1 то передаем 1 (а не 0 - его индекс в списке)
        System.out.println("Deleting first child (index 1) >>> ");
        fam1.deleteChildFromList(1);
        System.out.println("\nChildren of the Family after child1 left >>> "
                + fam1.getChildrenList().getChildrenList());

//      проверяем (Family) метод deleteChild(Gender, name),
//      добавим пару детей без генератора детей)), что бы знать кого удалять
        System.out.println("\n\nChecking (Family) method deleteChild(object) | deleteChild(name) >>>  ");
        System.out.println("Let's add to the Family children Lara, Arnold (not using HumanGenerator, not random way)");

        Man arnoldFirstman = new Man("Arnold", "Firstman");
        fam1.addChildToList(arnoldFirstman);
        fam1.addChildToList(new Woman("Lara", "Firstman"));
        System.out.println("Family children after adding Lara & Arnold: "
                + fam1.getChildrenList().getChildrenList());
//      удаляем arnoldFirstman (по объекту)
        System.out.println("\nDeleting Arnold by (object) >>> ");
        fam1.deleteChildFromList(arnoldFirstman);

//      удаляем Lara (по имени)
        System.out.println("\n\nDeleting Lara by name >>> ");
        fam1.deleteChildFromList("Lara");

        System.out.println("\n\nFamily children after Lara & Arnold left: "
                + fam1.getChildrenList().getChildrenList());
//      пробуем удалить детей которых нет у данной семьи
        System.out.println("\nTrying to delete un exist child >>>  ");
        fam1.deleteChildFromList("Marina");
        fam1.deleteChildFromList("Boris");
        fam1.deleteChildFromList(10);
        fam1.deleteChildFromList(-1);
        fam1.deleteChildFromList(new Man("Fedor", "Firstman"));
        System.out.println("\nFamily for now: " + fam1);



//      проверяем (Human) methods feedPet() |надо бы преропределить, проверить есть ли такой питомец, но в ТЗ не указано
//      и вдруг чел захочет покормить питомца друга или вообще бездомного|
//      >> время кормить
        System.out.println("\n\n>>> Checking (Human) method feedPet(boolean) >>>  ");
        System.out.println("Feeding the pet, it's time to eat >>> ");
        if(fam1.getFather().feedPet(true, Species.DOG, "Rocky", new Dog().generateTrickLevel())){
            System.out.println("Result = I fed the pet.");
        } else {
            System.out.println("Result = I didn't feed my pet.");
        }
//      >> не время кормить
        System.out.println("\nFeeding the pet, it's NOT time to eat >>> ");
        if(fam1.getFather().feedPet(false, Species.DOG, "Rocky", new Dog().generateTrickLevel())){
            System.out.println("Result = I fed the pet.");
        } else {
            System.out.println("Result = I didn't feed the pet.");
        }


//  пустой new Pet()
        System.out.print("\n\n>>> Creating (empty) new Pet() >>> ");
        System.out.println(new Pet() { @Override public String petResponding() {return "";}});

//  добавляем питомцев в семью
        System.out.print("\n>>> Adding pets to the Family >>> ");
        fam1.addPetToPetsSet(new Cat("Tom"));
        System.out.println("\nFamily after adding the pet cat Tom: " + fam1);

        System.out.print("\nAdding 3 more pets to the Family  >>> ");
        fam1.addPetToPetsSet(new Cat("Mars"));
        fam1.addPetToPetsSet(new Dog("Bob"));
        fam1.addPetToPetsSet(new Dog("Rocky"));
        System.out.println("\nFamily after added 3 more pets (pets sorted by trickLevel):" + fam1);


//  проверяем ссылки mother pet has family, mother has pets, child has pet
        System.out.println("\n\n>>> Checking links (pet has family, mother has pets, child has pet) >>> ");

        //  проверяем ссылки у pet1
        System.out.print("Pet Tom has family >>> ");
        System.out.println(fam1.getPetsSet().findPet(Species.CAT, "Tom").getFamily());

        //  проверяем ссылки у mother
        System.out.println("\nMother has pets >>> ");
        System.out.println(fam1.getMother().getPetsSet().getPetsTreeSet());

        //  т к по ТЗ массив детей (как я поняла) Human, и мы приводим их к Man или Woman в зависимости от рендера пола ребенка
        //  и кидаем их туда, то достаем все свойства и методы ребенка (Man или Woman) так же через приведениe...
        //  проверяем ссылки у child1
        System.out.println("\nChild1 has pets >>> ");
        System.out.println(fam1.getChildrenList().getChildrenList().get(0).getGender().gender.equals("woman")
                ? ((Woman) fam1.getChildrenList().getChildrenList().get(0)) .getPetsSet().getPetsTreeSet()
                :  ((Man) fam1.getChildrenList().getChildrenList().get(0)) .getPetsSet().getPetsTreeSet());



//  вот так можно изменять свойства объектов в Pets
        fam1.getPetsSet().findPet(Species.CAT, "Tom").setAge(10);
        fam1.getPetsSet().findPet(Species.CAT, "Tom").setHabitsSet(new TreeSet<>(List.of("sleep", "play")));
        System.out.println("\n\n>>> Cat Tom set age & new habits >>>\nPets of Family1 after it: \n"
                + fam1.getPetsSet().getPetsTreeSet());


//  добавляем еще питомцев
        System.out.print("\nAdding to the Family 2 more pets: fish Nemo, robocat Terminator >>> ");
        fam1.addPetToPetsSet(new Fish("Nemo"));
        fam1.addPetToPetsSet(new RoboCat("Terminator"));
        System.out.println("\nPets of the Family after it: \n" + fam1.getPetsSet().getPetsTreeSet());


//  удаляем питомца dog Bob
        System.out.print("\nDeleting pet dog Bob >>> ");
        Dog dogBob = (Dog) fam1.getPetsSet().findPet(Species.DOG, "Bob");
        fam1.removePetFromPetsSet(dogBob);
        System.out.println("\nPets of the Family after deleting pet dog Bob: \n" + fam1.getPetsSet().getPetsTreeSet());


//  удаляем питомца dog Bob
        System.out.print("\n>>> Checking petDidFoul() (implement PetFoulable) for cat Tom & Dog Rocky >>> \n");
        System.out.println(((Cat) fam1.getPetsSet().findPet(Species.CAT, "Tom")).petDidFoul());
        System.out.println(((Dog) fam1.getPetsSet().findPet(Species.DOG, "Rocky")).petDidFoul());


//  проверяем (Human) abstract (@Override in Man & in Woman) method greetPet()
        System.out.println("\n\n>>> Checking (Human) abstract (@Override in Man & in Woman) method greetPet()");
        System.out.print("\t>> ");
        System.out.println(fam1.getFather().greetPet(Species.DOG, "Rocky"));
        System.out.print("\t>> ");
        System.out.println(fam1.getMother().greetPet(Species.FISH, "Nemo"));
        System.out.print("\t>> ");
        System.out.println(fam1.getFather().greetPet(Species.CAT, "Tom"));
        System.out.print("\t>> ");
        System.out.println(fam1.getMother().greetPet(Species.ROBOCAT, "Terminator"));
        System.out.print("\t>> ");
        System.out.println(fam1.getFather().greetPet(Species.UNKNOWN, "SomeNickName"));


//  проверяем (Woman) (Man) method describePet(Species, String nickName)
        System.out.println("\n>>> Checking (Woman) (Man) method describePet(Species, String nickName) >>> ");
        System.out.println("Woman describing her pet Rocky >>> ");
        System.out.println(fam1.getMother().describePet(Species.CAT, "Tom"));

        System.out.println("\nMan describing his pet Terminator >>> ");
        System.out.println(fam1.getFather().describePet(Species.ROBOCAT, "Terminator"));

    }
}