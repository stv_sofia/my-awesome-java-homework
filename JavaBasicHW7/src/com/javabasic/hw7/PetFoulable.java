package com.javabasic.hw7;

public abstract interface PetFoulable {
    public abstract String petDidFoul();
}
