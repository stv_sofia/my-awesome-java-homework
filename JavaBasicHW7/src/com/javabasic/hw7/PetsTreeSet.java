package com.javabasic.hw7;

import java.util.Set;
import java.util.TreeSet;

public class PetsTreeSet {
//  Collection Set->TreeSet
    private Set<Pet> petsTreeSet = new TreeSet<>();

//  Getter
    public Set<Pet> getPetsTreeSet() {
        return this.petsTreeSet;
    }

//  method findPet()
    public Pet findPet(Species species, String nickName){
        for (Pet el: this.petsTreeSet) {
            if(el.getSpecies().species.equals(species.species) && el.getNickname().equals(nickName)){
                return el;
            }
        }
        return null;
    }


//  method addPetToPetsTreeSet()
    public boolean addPetToPetsTreeSet(Pet pet){
        return this.petsTreeSet.add(pet);
    }


//  method removePetToPetsTreeSet(Pet pet)
    public boolean removePetFromPetsTreeSet(Pet pet){
        return this.petsTreeSet.remove(pet);
    }

}