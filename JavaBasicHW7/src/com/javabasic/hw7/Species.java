package com.javabasic.hw7;

public enum Species {
    UNKNOWN ("unknown", false, -1, false),
    FISH ("fish",false, 0, false),
    CAT ("domestic cat", false, 4, true),
    DOG ("dog",false, 4, true),
    ROBOCAT ("robocat", false, 4, false);

    String species;
    boolean canFly;
    int numberOfLegs;
    boolean hasFur;

    Species(){
        this.species = "UNKNOWN";
    }
    Species(String species, boolean canFly, int numberOfLegs, boolean hasFur){
        this.species = species;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
