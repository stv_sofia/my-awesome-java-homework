package com.javabasic.hw7;

import java.util.ArrayList;
import java.util.List;

public class HumanChildrenList {
//  Collection List->ArrayList
    private List<Human> childrenList = new ArrayList<>();

//  Getter && Setter
    public List<Human> getChildrenList() {
        return this.childrenList;
    }
    public void setChildrenList(List<Human> childrenList) {
        this.childrenList = childrenList;
    }


//  method addChildToHumanChildrenList(Human child)
    public boolean addChildToHumanChildrenList(Human child){
        return this.childrenList.add(child);
    }


//  method deleteChildFromHumanChildrenList(int index)
//  - удаляем по переданному индеку remove(index-1) - (т е возвращается удаленный объект)
    public Human deleteChildFromHumanChildrenList(int index){
        return this.childrenList.remove(index-1);
    }


//  method deleteChildFromHumanChildrenList(Human child)
//  - передаем объект но удаляем по индеку remove(i) - (т е возвращается удаленный объект)
    public Human deleteChildFromHumanChildrenList(Human child){
        for(int i = 0; i < this.childrenList.size(); i++){
            if (childrenList.get(i).equals(child)) {
                return this.childrenList.remove(i);
            }
        }
        return null;
    }


//  method deleteChildFromHumanChildrenList(String name)
//  - удаляем по объекту remove(el) - (т е возвращается результат выполненного true || false)
    public boolean deleteChildFromHumanChildrenList(String name) {
        for (Human el : this.childrenList) {
            if (el.getName().equals(name)) {
                System.out.printf("Child %s has left the family of %s %s!", el.getName(),
                        el.getFamily().getFather().getName(), el.getFamily().getFather().getSurname());
                return this.childrenList.remove(el);
            }
        }
        System.out.println("Family has no such child name " + name + "!");
        return false;
    }
}