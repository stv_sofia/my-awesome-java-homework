package com.javabasic.hw7;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FishTest {
    private Fish module;

    @Before
    public void setUp() throws Exception {
        module = new Fish("Nemo");
        module.setTrickLevel(35);
    }


//  test Check Fish() no initialize
    @Test
    public void testCheckCFishEmptyGenderWomanSuccess(){
        Fish module = new Fish();
        assertEquals("fish", module.getSpecies().species);
    }


//  test Check Fish ToString()
    @Test
    public void testCheckFishToStringSuccess() {
        assertEquals("\n\t\tPet{species='fish', nickname='Nemo', " +
                "can fly='false', number of legs='0', has fur='false', " +
                "age='no info', trickLevel='35', habits='no info'}", module.toString());
    }


//  test Check Fish Equals()
    @Test
    public void testCheckFishEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckFishNotEqualsSuccess() {
        assertNotEquals(new Fish(), module);
    }


//  test Check Fish HashCode()
    @Test
    public void testCheckFishHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckFishHashCodeNotEqualsSuccess() {
        assertNotEquals(new Fish().hashCode(), module.hashCode());
    }


//  test Check Fish (Pet) abstract methods petResponding()
    @Test
    public void testCheckFishPetRespondingSuccess(){
        assertEquals("Hello, master! I am your fish Nemo. I need some oxygen!", module.petResponding());
    }
}