package com.javabasic.hw7;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public abstract class Pet implements Comparable{
    private Species species;
    private int trickLevel = -1;
    private String nickname;
    private int age = -1;
    private Set<String> habitsSet = new TreeSet<>();
    private Family family;


//  Constructors
    Pet(){
        this.species = Species.UNKNOWN;
        this.trickLevel = generateTrickLevel();
    }

    Pet(Species species) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
    }
    Pet(Species species, String nickName) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
        this.nickname = nickName;
    }
    Pet(Species species, String nickName, int age) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
        this.nickname = nickName;
        this.age = age;
    }
    Pet(Species species, String nickName, int age, Set<String> habitsSet) {
        this.species = species;
        this.trickLevel = generateTrickLevel();
        this.nickname = nickName;
        this.age = age;
        this.habitsSet = habitsSet;
    }

//  Getters & Setters
    public Species getSpecies() { return this.species; }
    public void setSpecies(Species species) { this.species = species; }

    public int getTrickLevel() { return trickLevel; }
    public void setTrickLevel(int trickLevel) { this.trickLevel = trickLevel; }

    public String getNickname() { return nickname; }
    public void setNickname(String nickname) { this.nickname = nickname;}

    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }

    public Set<String> getHabitsSet() { return habitsSet; }
    public void setHabitsSet(Set<String> habitsSet) { this.habitsSet = habitsSet; }

    public Family getFamily() { return family; }
    public void setFamily(Family family) { this.family = family;}


//  @Override method compareTo() (implement Comparable)
    @Override
    public int compareTo(Object obj) {
        Pet pet = (Pet) obj;
        return this.getTrickLevel() - pet.getTrickLevel();
    }

//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        if(this.getSpecies().species.equals("unknown")){
            return "\n\tPet{" +
                    "species='" + species.species + "'}";
        } else {
            return "\n\t\tPet{" +
                    "species='" + species.species + '\'' +
                    ", nickname='" + (this.getNickname() == null ? "no info" :  this.getNickname()) +  '\'' +
                    ", can fly='" + this.getSpecies().canFly +  '\'' +
                    ", number of legs='" + this.getSpecies().numberOfLegs +  '\'' +
                    ", has fur='" + this.getSpecies().hasFur +  '\'' +
                    ", age='" + (this.getAge() == -1 ? "no info" : this.getAge()) +  '\'' +
                    ", trickLevel='" + this.getTrickLevel() +  '\'' +
                    ", habits=" + (this.getHabitsSet().size() == 0 ? "'no info'" : this.getHabitsSet()) +
                    '}';
        }
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Pet)) return false;
        Pet pet = (Pet) obj;
        return getTrickLevel() == pet.getTrickLevel() && getAge() == pet.getAge()
                && getSpecies() == pet.getSpecies() && getNickname().equals(pet.getNickname());
    }
    @Override
    public int hashCode() {
        return Objects.hash(getTrickLevel(), getAge(), getSpecies(),  getNickname());
    }


//  (Pet) abstract methods petResponding() |нужно переопределить по ТЗ, п э abstract methods|
    public abstract String petResponding();


//  (Pet) methods petEating()
    public String petEating(){
        return "I'm pet " + this.getSpecies().species + ". And I'm eating!";
    }


//  (Pet) methods generateTrickLevel()
    public int generateTrickLevel(){ return (int) (Math.random() * 101); }


//  (Pet) methods getTrickLevelString()
    public String getTrickLevelString(int trickLevel){
        if(trickLevel == -1) return "maybe canning";
        if(trickLevel < 0 || trickLevel > 100) return "not in range of trickLevel [0:100]";
        return trickLevel > 50 ? "very canning one" : "almost not canning";
    }
}