package com.javabasic.hw7;

public enum Gender {
    MAN("man"),
    WOMAN("woman"),
    UNKNOWN ("unknown");

    String gender;

    Gender(){
        this.gender = "UNKNOWN";
    }

    Gender(String gender){
        this.gender = gender;
    }
}