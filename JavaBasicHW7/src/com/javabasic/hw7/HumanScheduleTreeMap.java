package com.javabasic.hw7;

import java.util.*;

public class HumanScheduleTreeMap {
//  Collection Map->TreeMap
    private Map<DayOfWeek, LinkedHashSet<String>> humanScheduleTreeMap = new TreeMap<>();

//  Constructors
    HumanScheduleTreeMap(){
       for(DayOfWeek day : DayOfWeek.values()){
           this.humanScheduleTreeMap.put(day,new LinkedHashSet<>());
       }
    }

//  Getter
    public Map<DayOfWeek, LinkedHashSet<String>> getHumanScheduleTreeMap() {
        return humanScheduleTreeMap;
    }


//  method addTaskOfTheDay()
    public void addTaskOfTheDay(DayOfWeek day, String toDo){
        this.humanScheduleTreeMap.get(day).add(toDo);
    }


//  method removeTaskOfTheDay()
    public boolean removeTaskOfTheDay(DayOfWeek day, String toDo){
            return this.humanScheduleTreeMap.get(day).remove(toDo);
    }
}