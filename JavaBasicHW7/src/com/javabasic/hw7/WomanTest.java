package com.javabasic.hw7;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WomanTest {
    private Woman module;

    @Before
    public void setUp() throws Exception{
        module = new Woman("Eve", "Firstwoman");
        module.setIqLevel(99);
    }

//  test Check Woman() no initialize
    @Test
    public void testCheckWomanEmptyGenderWomanSuccess(){
        Woman module = new Woman();
        assertEquals("woman", module.getGender().gender);
    }


//  test Check Woman ToString()
    @Test
    public void testCheckWomanToStringSuccess() {
        assertEquals("\n\t\tHuman{gender='woman, name='Eve', surname='Firstwoman', " +
                "yearBirth='no info', iqLevel='99', family='no info', pets='no info', schedule='no info'}", module.toString());
    }


//  test Check Woman testEquals()
    @Test
    public void testCheckWomanEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckWomanNotEqualsSuccess() {
        assertNotEquals(new Woman(), module);
    }

    
//  test Check Woman HashCode()
    @Test
    public void testCheckWomanHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckWomanHashCodeNotEqualsSuccess() {
        assertNotEquals(new Woman().hashCode(), module.hashCode());
    }


//  test Check Woman GreetPet()
    @Test
    public void testCheckWomanGreetPetButNoHavePetSuccess() {
        String result = "";
        if(module.getPetsSet() == null){
            result = "Have no pet yet..";
        }
        assertNull(module.getPetsSet());
        assertEquals("Have no pet yet..", result);
    }
    @Test
    public void testCheckWomanGreetPetSuccess() {
        Dog dog = new Dog("Rocky");
        PetsTreeSet pets = new PetsTreeSet();
        module.setPetsSet(pets);
        module.getPetsSet().addPetToPetsTreeSet(dog);
        String result = "";
        if(module.getPetsSet() != null){
            result = "I greet my pet";
        }
        assertNotNull(module.getPetsSet());
        assertEquals(1, module.getPetsSet().getPetsTreeSet().size());
        assertEquals("I greet my pet", result);
    }


//  test Check Woman DescribePet()
    @Test
    public void testCheckWomanDescribePetButHaveNoPetSuccess() {
        String result = "";
        if(module.getPetsSet() == null){
            result = "Have no pet to describe";
        }
        assertNull(module.getPetsSet());
        assertEquals("Have no pet to describe", result);
    }
    @Test
    public void testCheckWomanDescribePetButNoHaveNoSuchPetSuccess() {
        Dog dog = new Dog("Rocky");
        PetsTreeSet pets = new PetsTreeSet();
        module.setPetsSet(pets);
        module.getPetsSet().addPetToPetsTreeSet(dog);
        String result = "";
        String somePetNickName = "SuperPet";
        if(!module.getPetsSet().findPet(Species.DOG, "Rocky").getNickname().equals(somePetNickName)){
            result = "I can describe my pet, but I have no such pet!";
        }
        assertNotNull(module.getPetsSet());
        assertEquals("I can describe my pet, but I have no such pet!", result);
    }
    @Test
    public void testCheckWomanDescribePetSuccess() {
        Dog dog = new Dog("Rocky");
        PetsTreeSet pets = new PetsTreeSet();
        module.setPetsSet(pets);
        module.getPetsSet().addPetToPetsTreeSet(dog);
        String result = "";
        if(module.getPetsSet() != null){
            result = "I describe my pet";
        }
        assertNotNull(module.getPetsSet());
        assertEquals("I describe my pet", result);
    }


//  test Check Woman TravelToSeaSide()
    @Test
    public void testCheckWomanTravelToSeaSideSuccess() {
        assertEquals("Goodbye every one! It's time to travel to the Sea side!",
                module.travelToSeaSide());
    }
}