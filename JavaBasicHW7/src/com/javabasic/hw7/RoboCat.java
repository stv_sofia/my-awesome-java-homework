package com.javabasic.hw7;

import java.util.Set;

public class RoboCat extends Pet {

//  Constructors
    RoboCat(){
        super(Species.ROBOCAT);
    }
    RoboCat(String nickname) {
        super(Species.ROBOCAT, nickname);
    }
    RoboCat(String nickname, int age) {
        super(Species.ROBOCAT, nickname, age);
    }
    RoboCat(String nickname, int age, Set<String> habitsSet) {
        super(Species.ROBOCAT, nickname, age, habitsSet);
    }

//  Getters & Setters (super)

//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof RoboCat)) return false;
        if (!super.equals(obj)) return false;
        RoboCat roboCat = (RoboCat) obj;
        return super.equals(roboCat);
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }


//  @Override super (Pet) abstract methods petResponding() |переопределяю|
    @Override
    public String petResponding(){
        return "Hello, master! I am your " + this.getSpecies().species + " " + this.getNickname() +
                ". I need to charge!";
    }
}