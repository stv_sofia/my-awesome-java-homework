package com.javabasic.hw10.app.dao;

import com.javabasic.hw10.app.domain.family.Family;

import java.util.List;


// Создаем абстрактный интерфейс FamilyDao для сущности из List<Family> families
// - абстрактно описывает как мы будем работать с источником данных List<Family>

public interface FamilyDao {
    public List<Family> getAllFamiliesList();          // - возвращает список всех семей (List).


    public void displayAllFamilies();                   // - выводит на экран все семьи (в индексированном списке) со всеми членами семьи.


    public Family getFamilyByIndex(int index);         // - возвращает семью по указанному индексу,
                                                       // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.

    public boolean deleteFamilyByIndex(int index);     // - удаляет семью с заданным индексом, если такой индекс существует;
                                                       // - возвращает true если удаление произошло, - false - если нет.


    public boolean deleteFamily(Family family);        // - удаляет семью если такая существует в списке;
                                                       // - возвращает true если удаление произошло, - false - если нет.

    public void saveFamily(Family family);             // - обновляет существующую семью в БД если такая уже существует,
                                                       // - сохраняет в конец списка - если нет.

    public int count();                                // - возвращает количество семей в БД.
}
