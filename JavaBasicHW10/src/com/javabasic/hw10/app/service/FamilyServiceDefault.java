package com.javabasic.hw10.app.service;

import com.javabasic.hw10.app.dao.CollectionFamilyDao;
import com.javabasic.hw10.app.domain.collectionChildren.HumanChildrenList;
import com.javabasic.hw10.app.domain.collectionPets.PetsTreeSet;
import com.javabasic.hw10.app.domain.family.Family;
import com.javabasic.hw10.app.domain.human.Human;
import com.javabasic.hw10.app.domain.human.Man;
import com.javabasic.hw10.app.domain.human.Woman;
import com.javabasic.hw10.app.domain.pet.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;


public class FamilyServiceDefault implements FamilyService{
    private CollectionFamilyDao familyDao;

    public FamilyServiceDefault(CollectionFamilyDao familyDao) {
        this.familyDao = familyDao;            // FamilyServiceDefault familyDao содержит ссылку на FamilyDaoCollection (источник данных)
    }


    //1. Возвращает список всех семей List<Family>.
    @Override
    public List<Family> getAllFamiliesList(){
        return familyDao.getAllFamiliesList();
    }


    //2. Выводит на экран все семьи (в индексированном списке) со всеми членами семьи.
    @Override
    public void displayAllFamilies(){
        familyDao.displayAllFamilies();
    }


    //3. Находит семью по указанному индексу, - возвращает семью по указанному индексу,
    // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.
    @Override
    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }


    //4. Удаляет семью с заданным индексом,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamilyByIndex(int index) {
       return familyDao.deleteFamilyByIndex(index);
    }


    //5. Удаляет семью если такая существует в списке,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamily(Family family) {
        return  familyDao.deleteFamily(family);
    }


    //6. Обновляет существующую семью в БД если такая уже существует,
    // - сохраняет семью в конец списка - если семьи в БД нет.
    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }



    //7. Возвращает количество семей в БД.
    @Override
    public int count(){
        return familyDao.count();
    }


    // >>> Рефакторинг getFamiliesBiggerThan() - используем возможности Java 8
    //8.  Находит семьи с количеством людей больше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей больше чем указанное
    public List<Family> getFamiliesBiggerThan(int members) {
        if (familyDao.getAllFamiliesList() != null) {
            List<Family> selectedFamilies = familyDao.getAllFamiliesList().stream()
                    .filter(family -> family.countFamilyMembers() > members)
                    .collect(Collectors.toList());
            if(selectedFamilies.size() > 0){
                System.out.println("\n>>> Families with members more then " + members + ": " + selectedFamilies);
                return selectedFamilies;
            }
        }
        return null;
    }


    // >>> Рефакторинг getFamiliesLessThan() - используем возможности Java 8
    //9.  Находит семьи с количеством людей меньше чем, (принимает количество человек)
    //  если находит: - выводит информацию на экран, - возвращает все семьи где количество людей меньше чем указанное
    public List<Family> getFamiliesLessThan(int members) {
        if (familyDao.getAllFamiliesList() != null) {
            List<Family> selectedFamilies = familyDao.getAllFamiliesList().stream()
                    .filter(family -> family.countFamilyMembers() < members)
                    .collect(Collectors.toList());
            if(selectedFamilies.size() > 0){
                System.out.println("\n>>> Families with members less then " + members + ": " + selectedFamilies);
                return selectedFamilies;
            }
        }
        return null;
    }


    // >>> Рефакторинг countFamiliesWithMemberNumber() - используем возможности Java 8
    //10.  Подсчитывает число семей с количеством людей равное переданному числу.
    public int countFamiliesWithMemberNumber(int members){
        return (int) familyDao.getAllFamiliesList().stream()
                .filter(family -> family.countFamilyMembers() == members)
                .count();
    }


    //11.  Создает новую семью (принимает 2 параметра типа Human) - создает новую семью, - сохраняет в БД.
    public void createNewFamily(Human woman, Human man){
        Family newFamily = new Family((Woman) woman, (Man) man);
        if(familyDao.getAllFamiliesList() == null){
            familyDao.setNewFamilyCollection();
        }
        familyDao.getAllFamiliesList().add(newFamily);
    }


    //12. Создает ребенка семьей (принимает Family и 2 типа String: мужское и женское имя)
    //  - в данной семье появляется новый ребенок с учетом данных родителей,
    //  - если рожденный ребенок мальчик - ему присваивается мужское имя, если девочка - женское,
    //  - информация о семье обновляется в БД, - метод возвращает обновленную семью.
    public Family bornChild(Family family, String womanName, String manName){
        if(familyDao.getAllFamiliesList() != null){
            int familyIndex = familyDao.getAllFamiliesList().indexOf(family);
            Family fam1 = familyDao.getAllFamiliesList().get(familyIndex);

            int randGender = fam1.generateGender();
            if (randGender <= 50) {
                Woman childW = (Woman) fam1.bornChild(randGender);
                childW.setName(womanName);
                fam1.addChildToList(childW);
            } else {
                Man childM = (Man) fam1.bornChild(randGender);
                childM.setName(manName);
                fam1.addChildToList(childM);
            }

            familyDao.getAllFamiliesList().set(familyIndex, fam1);
            return familyDao.getAllFamiliesList().get(familyIndex);
        }
        return null;
    }


    //13. Усыновляет ребенка (принимает 2 параметра: Family, Human) - метод возвращает обновленную семью,
    //  - в данной семье сохраняется данный ребенок, - информация о семье обновляется в БД.
    public Family adoptChild(Family family, Human child) {
        if(familyDao.getAllFamiliesList() != null){
            if(familyDao.getAllFamiliesList().contains(family)){
                int index = familyDao.getAllFamiliesList().indexOf(family);
                Family fam = familyDao.getAllFamiliesList().get(index);
                child.setSurname(fam.getFather().getSurname());
                fam.addChildToList(child);
                saveFamily(fam);
                return familyDao.getAllFamiliesList().get(index);
            }
        }
        return null;
    }


    // >>> Рефакторинг deleteAllChildrenOlderThen() - используем возможности Java 8
    //14. Удаляет детей старше чем (принимает int)
    // - во всех семьях удаляются дети, которые старше указанно возраста, - информация обновляется в БД.
    public void deleteAllChildrenOlderThen(int yearsOld){
        if(familyDao.getAllFamiliesList() != null){
            familyDao.getAllFamiliesList().forEach( family -> {
                if(family.getChildrenList() != null && family.getChildrenList().getChildrenList().size() > 0){
                    List<Human> filteredChildren = family.getChildrenList().getChildrenList().stream()
                            .filter( child -> (Instant.ofEpochMilli(child.getBirthDateMilli()).atZone(ZoneId.systemDefault()).toLocalDate())
                                    .isAfter(LocalDate.now().minusYears(yearsOld)) )
                            .collect(Collectors.toList());

                    if(filteredChildren.size() != 0){
                        HumanChildrenList newChildrenList = new HumanChildrenList();
                        newChildrenList.setChildrenList(filteredChildren);
                        family.setChildrenList(newChildrenList);
                        this.saveFamily(family);
                        family.setMembersTotal();
                    }
                }
            });
        }
    }


    //15.  Возвращает список домашних животных, которые живут в семье (принимает индекс семьи),
    public PetsTreeSet getPets(int index){
        if(familyDao.getAllFamiliesList() != null){
            if(familyDao.getAllFamiliesList().contains(familyDao.getAllFamiliesList().get(index))){
                if(this.getFamilyByIndex(index).getPetsSet() != null){
                    return this.getFamilyByIndex(index).getPetsSet();
                }
                System.out.println("Family with index " + index + " has no pets!");
            }
        }
        return null;
    }


    //16. Добавляет питомца в семью (принимает индекс семьи и параметр Pet)
    // - добавляет нового питомца в семью,  - обновляет данные в БД.
    public boolean addPet(int index, Pet pet){
        if(familyDao.getAllFamiliesList() != null) {
            if(familyDao.getAllFamiliesList().contains(familyDao.getAllFamiliesList().get(index))){
                boolean result = familyDao.getFamilyByIndex(index).addPet(pet);
                familyDao.getAllFamiliesList().set(index, familyDao.getFamilyByIndex(index));
                return result;
            }
        }
        return false;
    }


    //17. Удаляет питомца (принимает индекс семьи и параметр Pet) - удаляет питомца из семьи,  - обновляет данные в БД.
    public boolean deletePet(int index, Pet pet){
        if(familyDao.getAllFamiliesList() != null) {
            if(familyDao.getAllFamiliesList().contains(familyDao.getAllFamiliesList().get(index))){
                boolean result = familyDao.getFamilyByIndex(index).deletePet(pet);
                familyDao.getAllFamiliesList().set(index, familyDao.getFamilyByIndex(index));
                return result;
            }
        }
        return false;
    }


}