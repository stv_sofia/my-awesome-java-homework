package com.javabasic.hw10.app.domain.pet;

public interface PetFoulable {
    String petDidFoul();
}
