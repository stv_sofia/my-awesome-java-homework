package com.javabasic.hw10.app.domain.pet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DogTest {
    private Dog module;

    @Before
    public void setUp() throws Exception {
        module = new Dog("Rocky");
        module.setTrickLevel(45);
    }


//  test Check Dog() no initialize
    @Test
    public void testCheckDogEmptyGenderWomanSuccess(){
        Dog module = new Dog();
        assertEquals("dog", module.getSpecies().species);
    }


//  test Check Dog ToString()
    @Test
    public void testCheckDogToStringSuccess() {
        assertEquals("\n\t\tPet{species='dog', nickname='Rocky', " +
                "can fly='false', number of legs='4', has fur='true', " +
                "age='no info', trickLevel='45', habits='no info'}", module.toString());
    }


//  test Check Dog Equals()
    @Test
    public void testCheckDogEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckDogNotEqualsSuccess() {
        assertNotEquals(new Dog("Dog"), module);
    }


//  test Check Dog HashCode()
    @Test
    public void testCheckDogHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckDogHashCodeNotEqualsSuccess() {
        assertNotEquals(new Dog().hashCode(), module.hashCode());
    }


//  test Check Dog (Pet) abstract methods petResponding()
    @Test
    public void testCheckDogPetRespondingSuccess(){
        assertEquals("Hello, master! I am your dog Rocky. Let's play!", module.petResponding());
    }


//  test Check Dog implement (PetFoulable) abstract methods petDidFoul()
    @Test
    public void testCheckDogPetDidFoulSuccess(){
        assertEquals("I am dog Rocky. I did foul - I ate the sofa)) Needs to cover tracks well...",
                module.petDidFoul());
    }
}