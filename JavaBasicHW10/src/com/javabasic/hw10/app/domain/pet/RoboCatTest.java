package com.javabasic.hw10.app.domain.pet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RoboCatTest {
    private RoboCat module;

    @Before
    public void setUp() throws Exception {
        module = new RoboCat("Robic");
        module.setTrickLevel(100);
    }


//  test Check RoboCat() no initialize
    @Test
    public void testCheckRoboCatEmptyGenderWomanSuccess(){
        RoboCat module = new RoboCat();
        assertEquals("robocat", module.getSpecies().species);
    }


//  test Check RoboCat ToString()
    @Test
    public void testCheckRoboCatToStringSuccess() {
        assertEquals("\n\t\tPet{species='robocat', nickname='Robic', " +
                "can fly='false', number of legs='4', has fur='false', " +
                "age='no info', trickLevel='100', habits='no info'}", module.toString());
    }


//  test Check RoboCat Equals()
    @Test
    public void testCheckRoboCatEqualsSuccess() {
        assertEquals(module, module);
    }
    @Test
    public void testCheckRoboCatNotEqualsSuccess() {
        assertNotEquals(new RoboCat("RCat"), module);
    }


//  test Check RoboCat HashCode()
    @Test
    public void testCheckRoboCatHashCodeEqualsSuccess() {
        assertEquals(module.hashCode(), module.hashCode());
    }
    @Test
    public void testCheckRoboCatHashCodeNotEqualsSuccess() {
        assertNotEquals(new RoboCat().hashCode(), module.hashCode());
    }

//  test Check RoboCat (Pet) abstract methods petResponding()
    @Test
    public void testCheckRoboCatPetRespondingSuccess(){
        assertEquals("Hello, master! I am your robocat Robic. I need to charge!", module.petResponding());
    }

}