package com.javabasic.hw10.app.domain.pet;

import java.util.Set;

public class Cat extends Pet implements PetFoulable{

//  Constructors
    public Cat(){
        super(Species.CAT);
    }
    public Cat(String nickname) {
        super(Species.CAT, nickname);
    }
    public Cat(String nickname, int age) {
        super(Species.CAT, nickname, age);
    }
    public Cat(String nickname, int age, Set<String> habitsSet) {
        super(Species.CAT, nickname, age, habitsSet);
    }

//  Getters & Setters (super)


//  @Override methods toString(), equals(), hashCode()
    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Cat)) return false;
        if (!super.equals(obj)) return false;
        Cat cat = (Cat) obj;
        return super.equals(cat);
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }


//  @Override method finalize
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Cat extends Pet obj before Garbage Collector will delete it : " + this);
    }


//  @Override super (Pet) abstract methods petResponding() |переопределяю|
    @Override
    public String petResponding(){
        return "Hello, master! I am your " + this.getSpecies().species + " " + this.getNickname() +
                ". And I miss you so!";
    }


//  @Override implement (PetFoulable) abstract methods petDidFoul()
    @Override
    public String petDidFoul(){
        return "I am " + this.getSpecies().species + " " + this.getNickname() +
                ". I did foul - I dug all the plants)) Needs to cover tracks well...";
    }
}