package com.javabasic.hw10.app.domain.collectionPets;

import com.javabasic.hw10.app.domain.pet.Pet;
import com.javabasic.hw10.app.domain.pet.Species;

import java.util.Set;
import java.util.TreeSet;

public class PetsTreeSet {
//  Collection Set->TreeSet
    private Set<Pet> petsTreeSet = new TreeSet<>();

//  Getter
    public Set<Pet> getPetsTreeSet() {
        return this.petsTreeSet;
    }

//  method findPet()
    public Pet findPet(Species species, String nickName){
        for (Pet el: this.petsTreeSet) {
            if(el.getSpecies().species.equals(species.species) && el.getNickname().equals(nickName)){
                return el;
            }
        }
        return null;
    }


//  method addPetToPetsTreeSet()
    public boolean addPetToPetsTreeSet(Pet pet){
        for (Pet el: this.petsTreeSet) {
            if(el.equals(pet)) return false;
        }
        return this.petsTreeSet.add(pet);
    }


//  method removePetToPetsTreeSet(Pet pet)
    public boolean removePetFromPetsTreeSet(Pet pet){
        for (Pet el: this.petsTreeSet) {
            if(el.getSpecies().species.equals(pet.getSpecies().species) && el.getNickname().equals(pet.getNickname())){
                return this.petsTreeSet.remove(el);
            }

        }
        return this.petsTreeSet.remove(pet);
    }

}