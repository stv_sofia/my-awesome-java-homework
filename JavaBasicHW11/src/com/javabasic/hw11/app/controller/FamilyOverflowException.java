package com.javabasic.hw11.app.controller;

public class FamilyOverflowException extends RuntimeException{
    public Throwable familyOverflowException(int limitForChildren) throws RuntimeException {
        throw new RuntimeException("!ERROR! OverflowException! >> Family can't have more then " + limitForChildren + " children for now!");
    }
}
