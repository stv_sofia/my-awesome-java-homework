package com.javabasic.hw11.app.service;

import com.javabasic.hw11.app.domain.family.Family;

import java.util.List;

public interface FamilyService {
    public List<Family> getAllFamiliesList();
    public void displayAllFamilies();
    public Family getFamilyByIndex(int index);
    public boolean deleteFamilyByIndex(int index);
    public boolean deleteFamily(Family family);
    public void saveFamily(Family family);
    public int count();
}
