package com.javabasic.hw11.app;

import com.javabasic.hw11.app.controller.FamilyControllerDefault;
import com.javabasic.hw11.app.dao.CollectionFamilyDao;
import com.javabasic.hw11.app.domain.family.Family;
import com.javabasic.hw11.app.domain.human.Gender;
import com.javabasic.hw11.app.domain.human.Man;
import com.javabasic.hw11.app.domain.human.Woman;
import com.javabasic.hw11.app.domain.pet.Cat;
import com.javabasic.hw11.app.domain.pet.Dog;
import com.javabasic.hw11.app.domain.pet.Fish;
import com.javabasic.hw11.app.service.FamilyServiceDefault;

import java.util.Scanner;

public class ConsoleApp {
    private FamilyControllerDefault familyController;

    public ConsoleApp(){
        familyController = new FamilyControllerDefault(new FamilyServiceDefault(new CollectionFamilyDao()));
        //        CollectionFamilyDao familyDao = new CollectionFamilyDao();   //        FamilyServiceDefault familyService = new FamilyServiceDefault(familyDao);   //        familyController = new FamilyControllerDefault(familyService);
    }



//  showMainMenu() - выводит консольное меню
    public void showMainMenu() {
        System.out.println("\nSelect a menu item: \n" +
                "1  ....>> Fill DB with new test data (reset new)\n" +
                "2  ....>> Show entire list of families\n" +
                "3  ....>> Display a list of families where the number of people is greater than a given\n" +
                "4  ....>> Display a list of families where the number of people is less than a given\n" +
                "5  ....>> Count the number of families where the number of members is equal to a given\n" +
                "6  ....>> Create a new family\n" +
                "7  ....>> Delete a family by family index in the general list\n" +
                "8  ....>> Edit a family by family index in the general list\n" +
                "9  ....>> Remove all children over the given age\n" +
                "exit ..>> Quit the application");
    }



//  checkInputDataInteger() - проверяет введенную строку на число
    public int checkInputDataInteger (String inputData, int max){
        return familyController.checkInputDataInteger(inputData, max);
    }



//  readInputData() - считывает введенные пользователем данные
    private String readInputData(){
        Scanner scanner = new Scanner(System.in);
        String inputData = "";
        try {
            inputData = scanner.nextLine().toLowerCase();
        } catch (Exception e){
            System.out.println("Error input data");
            return inputData;
        }
        return inputData;
    }



//  implementTheSelectedAction() - (реализует) вызывает методы для реализации выбранный пользователем пункт меню
    public void implementTheSelectedAction(int choiceNumber){
        switch (choiceNumber) {
            case 1 -> this.fillFamilyCollectionWithTestData();


            case 2 -> {
                if (familyController.IsDbEmpty()) break;
                familyController.displayAllFamilies();
                System.out.println();
            }


            case 3 -> {
                if (familyController.IsDbEmpty()) break;
                System.out.print("To Get families where the number of people is greater than a given, enter the given number >>> ");
                int numberOfMembers1 = familyController.checkInputDataInteger(readInputData(), familyController.getAllFamiliesList().size());
                familyController.getFamiliesBiggerThan(numberOfMembers1);
            }


            case 4 -> {
                if (familyController.IsDbEmpty()) break;
                System.out.print("To Get families where the number of people is less than a given, enter the given number >>> ");

                familyController.getFamiliesLessThan(familyController.checkInputDataInteger(readInputData(),
                        familyController.getAllFamiliesList().size()));
            }


            case 5 -> {
                if (familyController.IsDbEmpty()) break;
                System.out.print("To Count families where the number of members is equal to a given, enter the given number >>> ");

                int numberOfMembers = familyController.checkInputDataInteger(readInputData(),
                        familyController.getAllFamiliesList().size());

                int result = familyController.countFamiliesWithMembersNumber(numberOfMembers);
                if (result != -1)
                    System.out.println("Families with members equal to " + numberOfMembers + ": " + result);
            }


            case 6 -> {
                System.out.println("\nTo Create a new family, you need to fill some data.");
                this.createNewFamily();
            }


            case 7 -> {
                if (familyController.IsDbEmpty()) break;
                System.out.print("To Delete a family by family index in the general list, enter index ot the family >>> ");

                int indexOfTheFamilyToDelete = familyController.checkInputDataInteger(readInputData(),
                        familyController.getAllFamiliesList().size());

                if (indexOfTheFamilyToDelete != -1) {
                    boolean result = familyController.deleteFamilyByIndex(indexOfTheFamilyToDelete);
                    System.out.println("Family with index " + indexOfTheFamilyToDelete + " was deleted: " + result);
                }
            }


            case 8 -> {
                if (familyController.IsDbEmpty()) break;
                int indexOfTheFamilyToEdit = -1;
                int editFamilyAction = -1;
                int limitChildren = 3;

                while (indexOfTheFamilyToEdit == -1) {
                    System.out.print("To Edit a family by family index in the general list, enter index ot the family >>> ");
                    indexOfTheFamilyToEdit = familyController.checkInputDataInteger(readInputData(),
                            familyController.getAllFamiliesList().size());
                }

                while (editFamilyAction == -1) {
                    System.out.print("Choose:  1 ....>> To Born a child  ||  2 ....>> To Adopt a child  ||  3 ....>> To Exit" +
                            "\nYour choice >>> ");
                    editFamilyAction = familyController.checkInputDataInteger(readInputData(), 3);
                }

                familyController.setNewChildrenListForFamilyIfEmpty(indexOfTheFamilyToEdit);
                if (familyController.getFamilyByIndex(indexOfTheFamilyToEdit).getChildrenList().getChildrenList().size() >= limitChildren) {
                    familyController.throwLimitForChildrenError(limitChildren);
                    System.out.println();
                    break;
                }

                this.editTheFamily(indexOfTheFamilyToEdit, editFamilyAction, limitChildren);
            }


            case 9 -> {
                if (familyController.IsDbEmpty()) break;
                int ageOfChildrenMustBeDeleted = -1;

                while (ageOfChildrenMustBeDeleted == -1) {
                    System.out.print("To Remove all children over the given age from general list, " +
                            "enter the given age [0:100] (for test data recommended 20) >>> ");
                    ageOfChildrenMustBeDeleted = familyController.checkInputDataInteger(readInputData(),100);

                }
                familyController.deleteAllChildrenOlderThen(ageOfChildrenMustBeDeleted);
            }


            default -> System.out.println();
        }
    }



//  fillFamilyCollectionWithTestData() - заполняет (каждый раз при вызове) FamilyCollection DB тестовыми данными
    public void fillFamilyCollectionWithTestData(){
        familyController.setNewFamilyCollection();
        //  создаем 5 семей
        familyController.createNewFamily(new Woman("Eve", "Firstwoman", "21/10/1965", 100),
                new Man("Adam", "Firstman","11/01/1960", 100));

        familyController.createNewFamily(new Woman("Rowan", "Secondwoman", "17/01/1967",100),
                new Man("Cain", "Secondman","18/04/1962", 100));

        familyController.createNewFamily(new Woman("Adel", "Thirdwoman", "19/02/1969"),
                new Man("Brad", "Thirdtman","01/08/1964"));

        familyController.createNewFamily(new Woman("Bella", "Forthwoman", "14/05/1963"),
                new Man("Fred", "Forthman","27/09/1961"));

        familyController.createNewFamily(new Woman("Leyla", "Fifthwoman", "29/03/1964"),
                new Man("John", "Fifthman","19/11/1963"));

        Family firstInListFamily = familyController.getFamilyByIndex(1);
        Family secondInListFamily = familyController.getFamilyByIndex(2);
        Family thirdInListFamily = familyController.getFamilyByIndex(3);
        Family forthInListFamily = familyController.getFamilyByIndex(4);
        Family fifthInListFamily = familyController.getFamilyByIndex(5);


        // добавляем пустое расписание для женщины и мужчины для всех семей

        firstInListFamily.getMother().setSchedule();
        firstInListFamily.getFather().setSchedule();

        secondInListFamily.getMother().setSchedule();
        secondInListFamily.getFather().setSchedule();

        thirdInListFamily.getMother().setSchedule();
        thirdInListFamily.getFather().setSchedule();

        forthInListFamily.getMother().setSchedule();
        forthInListFamily.getFather().setSchedule();

        fifthInListFamily.getMother().setSchedule();
        fifthInListFamily.getFather().setSchedule();


        //  добавляем детей (семья1 >> 1 родили + 1 усыновили,  семья2 >> 2 родили + 1 усыновили, семья5 >> 2 родили + 1 усыновили)
        familyController.bornChild(firstInListFamily, "Michaella", "Michel", 3);
        familyController.adoptChild(firstInListFamily, new Man("Mark", "SomeSurName", "20/10/2001"), 3);


        familyController.bornChild(secondInListFamily, "Ivanna", "Ivan", 3);
        familyController.bornChild(secondInListFamily, "Alexandra", "Alexandr", 3);
        familyController.adoptChild(secondInListFamily, new Woman("Nikol", "SomeSurName", "18/01/2000"), 3);


        familyController.bornChild(fifthInListFamily, "Helen", "Gavin", 3);
        familyController.bornChild(fifthInListFamily, "Olha", "Ben", 3);
        familyController.adoptChild(fifthInListFamily, new Woman("Rowan", "SomeSurName", "01/01/2001"), 3);

        // добавляем питомцев (семья3 >> 1 кот + 1 собака,  семья4 >> 3 рыбки)
        familyController.addPet(3, new Cat("Tom", 2));
        familyController.addPet(3, new Dog("Rocky", 3));

        familyController.addPet(4, new Fish("Sunny", 1));
        familyController.addPet(4, new Fish("Goldy", 1));
        familyController.addPet(4, new Fish("Nemo", 1));
        System.out.println("FILL DB with test DATA OK!");
    }



//  createNewFamily() - собирает, проверяет на корректность введенные данные, создает семью
    public void createNewFamily(){
    String wName = "";
    String wSureName = "";
    String wYearBirth = "";
    String wMonthBirth = "";
    String wdDayBirth = "";
    String wBirthdayStr = "";
    String wIq = "";
    int wIqInt = -1;

    String mName = "";
    String mSureName = "";
    String mYearBirth = "";
    String mMonthBirth = "";
    String mdDayBirth = "";
    String mBirthdayStr = "";
    String mIq = "";
    int mIqInt = -1;

    String inputString = "";
    int confirm = -1;

    while(true){
        if (inputString.equals("dataCollect")) break;
//      Data for the woman >>>
        while (wName.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Name of the a woman >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            wName = familyController.checkInputDataChars(inputString, 20);
            if(!wName.equals(""))
                wName = wName.substring(0, 1).toUpperCase() + wName.substring(1);
        }
        if (inputString.equals("exit")) break;

        while (wSureName.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the SureName of the a woman >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            wSureName = familyController.checkInputDataChars(inputString, 30);
            if(!wSureName.equals(""))
                wSureName = wSureName.substring(0, 1).toUpperCase() + wSureName.substring(1);
        }
        if (inputString.equals("exit")) break;

        while (wYearBirth.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Year of birth of the woman [1923:2003] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            wYearBirth = familyController.checkInputDataIntegerYMD(inputString, "Y");
        }
        if (inputString.equals("exit")) break;

        while (wMonthBirth.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Month of birth of the woman [1:12] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            wMonthBirth = familyController.checkInputDataIntegerYMD(inputString, "M");
            if(wMonthBirth.length() == 1) wMonthBirth = "0" + wMonthBirth;
        }
        if (inputString.equals("exit")) break;

        while (wdDayBirth.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Day of birth of the woman [1:31] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            wdDayBirth = familyController.checkInputDataIntegerYMD(inputString, "D");
            if(wdDayBirth.length() == 1) wdDayBirth = "0" + wdDayBirth;
        }
        if (inputString.equals("exit")) break;
        wBirthdayStr = wdDayBirth + "/" + wMonthBirth + "/" + wYearBirth;       // System.out.println("wBirthdayStr: " + wBirthdayStr);

        while (wIq.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the IQLevel of birth of the woman [0:100] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            wIqInt = familyController.checkInputDataInteger(inputString, 100);
            if (wIqInt != -1) wIq = inputString;
        }
        if (inputString.equals("exit")) break;

//      Data for the man >>>
        System.out.println();
        while (mName.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Name of the a man >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            mName = familyController.checkInputDataChars(inputString, 20);
            if(!mName.equals(""))
                mName = mName.substring(0, 1).toUpperCase() + mName.substring(1);
        }
        if (inputString.equals("exit")) break;

        while (mSureName.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the SureName of the a man >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            mSureName = familyController.checkInputDataChars(inputString, 30);
            if(!mSureName.equals(""))
                mSureName = mSureName.substring(0, 1).toUpperCase() + mSureName.substring(1);
        }
        if (inputString.equals("exit")) break;

        while (mYearBirth.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Year of birth of the man [1923:2003] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            mYearBirth = familyController.checkInputDataIntegerYMD(inputString, "Y");
        }
        if (inputString.equals("exit")) break;

        while (mMonthBirth.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Month of birth of the man [1:12] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            mMonthBirth = familyController.checkInputDataIntegerYMD(inputString, "M");
            if(mMonthBirth.length() == 1) mMonthBirth = "0" + mMonthBirth;
        }
        if (inputString.equals("exit")) break;

        while (mdDayBirth.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the Day of birth of the man [1:31] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            mdDayBirth = familyController.checkInputDataIntegerYMD(inputString, "D");
            if(mdDayBirth.length() == 1) mdDayBirth = "0" + mdDayBirth;
        }
        if (inputString.equals("exit")) break;
        mBirthdayStr = mdDayBirth + "/" + mMonthBirth + "/" + mYearBirth;        //System.out.println("mBirthdayStr: " + mBirthdayStr);

        while (mIq.equals("")) {
            System.out.print("Enter: exit ....>> To Live action  || or Enter: the IQLevel of birth of the man [0:100] >>> ");
            inputString = readInputData();
            if (inputString.equals("exit")) break;
            mIqInt = familyController.checkInputDataInteger(inputString, 100);
            if (mIqInt != -1) mIq = inputString;
        }
        if (inputString.equals("exit")) break;
        inputString = "dataCollect";
    }

//      Confirm & Create The New Family >>>
    if(inputString.equals("dataCollect")){
        while (confirm == -1){
            System.out.print("\nData To Create The New Family correct and collect." +
                    "\nChoose:  1 ....>> To Confirm  ||  0 ....>> To Exit" +
                    "\nYour choice >>> ");
            confirm = familyController.checkInputDataInteger(readInputData(),1);
        }

        if(confirm == 1){
            familyController.createNewFamily(new Woman(wName, wSureName, wBirthdayStr, wIqInt),
                    new Man(mName, mSureName, mBirthdayStr, mIqInt));

            Family newCreatedFamily = familyController.getFamilyByIndex(familyController.getAllFamiliesList().size());

            if (newCreatedFamily != null) {
                System.out.println("\nFamily action 'Create The New Family' success!");
                System.out.println(newCreatedFamily.prettyFormatFamily());
            }
        }
    }
}



//  editTheFamily() - принимает индекс семьи, выбор действия (родить/усановить ребенка/выйти в главное меню), лимит на кол-во детей в семье;
//  собирает, проверяет на корректность введенные данные, выполняет выранное действие
    public void editTheFamily(int indexOfTheFamily, int actionNumber, int limitChildren) {
        Family family = familyController.getFamilyByIndex(indexOfTheFamily);
        int confirm = -1;
        switch (actionNumber) {
            case 1 -> {
                System.out.println("Your choice is To Born a child.");
                String girlName = "";
                String boyName = "";
                while (girlName.equals("")) {
                    System.out.print("Enter the name for a girl >> ");
                    girlName = familyController.checkInputDataChars(readInputData(), 20);
                    girlName = girlName.substring(0, 1).toUpperCase() + girlName.substring(1);
                }
                while (boyName.equals("")) {
                    System.out.print("Enter the name for a boy >> ");
                    boyName = familyController.checkInputDataChars(readInputData(), 30);
                    boyName = boyName.substring(0, 1).toUpperCase() + boyName.substring(1);
                }

                while (confirm == -1){
                    System.out.print("Data To Born a child correct and collect." +
                            "\nChoose:  1 ....>> To Confirm  ||  0 ....>> To Exit" +
                            "\nYour choice >>> ");
                    confirm = familyController.checkInputDataInteger(readInputData(),1);
                }
                if(confirm == 1){
                    family = familyController.bornChild(family, girlName, boyName, limitChildren);
                    if (family != null) {
                        System.out.println("Family action 'to Born child' success!");
                        System.out.println(familyController.getFamilyByIndex(indexOfTheFamily).prettyFormatFamily());
                    }
                }
            }

            case 2 -> {
                System.out.println("Your choice is To Adopt a child.");
                String childName = "";
                String childSurName = "";
                while (childName.equals("")) {
                    System.out.print("Enter the Name for the child >> ");
                    childName = familyController.checkInputDataChars(readInputData(), 20);
                }
                childName = childName.substring(0, 1).toUpperCase() + childName.substring(1);
                while (childSurName.equals("")) {
                    System.out.print("Enter the Surname for the child >> ");
                    childSurName = familyController.checkInputDataChars(readInputData(), 30);
                }
                childSurName = childSurName.substring(0, 1).toUpperCase() + childSurName.substring(1);

                String childGender = "";
                while (childGender.equals("")) {
                    System.out.print("Enter Gender for the child (woman/man) >> ");
                    childGender = familyController.checkInputDataChars(readInputData(), 10);
                    if (!childGender.equals("woman") && !childGender.equals("man")) {
                        childGender = "";
                    }

                }

                String childBirthDateString = "";
                while (childBirthDateString.equals("")) {
                    System.out.print("Enter the Birth date for the child (format: dd/MM/yyyy) >> ");
                    childBirthDateString = familyController.checkInputDataBirthDayString(readInputData());
                }

                int igLevel = -1;
                while (igLevel == -1) {
                    System.out.print("Enter the IO level for the child [0:100] >> ");
                    igLevel = familyController.checkInputDataInteger(readInputData(), 100);
                }

                while (confirm == -1){
                    System.out.print("Data To Adopt a child correct and collect." +
                            "\nChoose:  1 ....>> To Confirm  ||  0 ....>> To Exit" +
                            "\nYour choice >>> ");
                    confirm = familyController.checkInputDataInteger(readInputData(),1);
                }
                if(confirm == 1){
                    if (childGender.equals(Gender.WOMAN.gender)) {
                        family = familyController.adoptChild(family,
                                new Woman(childName, childSurName, childBirthDateString, igLevel), limitChildren);
                    } else {
                        family = familyController.adoptChild(family,
                                new Man(childName, childSurName, childBirthDateString, igLevel), limitChildren);
                    }
                    if (family != null) {
                        System.out.println("Family action 'to Adopt child' success!");
                        System.out.println(familyController.getFamilyByIndex(indexOfTheFamily).prettyFormatFamily());
                    }
                }
            }

            case 3 -> System.out.println("Exit edit family menu!");

            default -> System.out.println();
        }
    }


}