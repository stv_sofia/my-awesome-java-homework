package com.javabasic.hw11.app.dao;

import com.javabasic.hw11.app.domain.family.Family;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


// Реализация FamilyDao в зависимости от того, где будут храниться данные - конкретно описываем, как мы будем работать с источником данных (List<Family>)
public class CollectionFamilyDao implements FamilyDao{
    private List<Family> families;

    public List<Family> getFamilyCollection() { return families; }
    public void setNewFamilyCollection() { this.families = new ArrayList<Family>(); }
    public void setFamilyCollection(List<Family> families) { this.families = families; }


    //1. Возвращает список всех семей List<Family>.
    @Override
    public List<Family> getAllFamiliesList(){
        if(this.families != null){
            return this.families;
        }
        return new ArrayList<Family>();
    }


// Рефакторинг displayAllFamilies() - используем возможности Java 8
    //2.Выводит на экран все семьи (в индексированном списке - индекс с 1!) со всеми членами семьи.
    @Override
    public void displayAllFamilies(){
        if(this.families != null){
            Map<Integer, Family> familyMap = this.getFamilyCollection().stream()
//                    .distinct()
                    .collect(Collectors.toMap(
                            i -> families.indexOf(i)+1,
                            family -> family
                    ));
            familyMap.forEach( (integer, family) -> System.out.printf("\nFamily index: %d, %s", integer, family.prettyFormatFamily()) );
        }
    }


    //3. Находит семью по указанному индексу (индекс передается с 1, в коллекции индекс с 0),
    // - возвращает семью по указанному индексу,
    // - в случае, если запросили элемент с несуществующим индексом - возвращайте null.
    @Override
    public Family getFamilyByIndex(int index) {
        if(this.families != null) {
            if (index >= 1 && index <= this.getFamilyCollection().size()) {
                return this.getFamilyCollection().get(index-1);
            }
        }
        return null;
    }


    //4. Удаляет семью с заданным индексом,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamilyByIndex(int index) {
        if(this.families == null || this.getAllFamiliesList().size() == 0){
            return false;
        }

        if ((index - 1) >= 0 && (index - 1) < this.getFamilyCollection().size()) {
            return this.families.remove(this.getFamilyCollection().get(index-1));
        } else {
            System.out.println("There is no such family with index " + index + " in DB Families!");
        }

        return false;
    }


    //5. Удаляет семью если такая существует в списке,
    // - возвращает true если удаление произошло, - возвращает false, если удаление не произошло.
    @Override
    public boolean deleteFamily(Family family) {
         if(this.families == null || this.getAllFamiliesList().size() == 0){
             return false;
         }

         if(this.getFamilyCollection().contains(family)) {
             return this.families.remove(family);
         }

        return false;
    }


    //6. Обновляет существующую семью в БД если такая уже существует,
    // - сохраняет семью в конец списка - если семьи в БД нет.
    @Override
    public void saveFamily(Family family) {
        if(this.families == null) {
            this.setNewFamilyCollection();
        }
        if(this.getFamilyCollection().contains(family)){
            this.families.set(this.families.indexOf(family), family);
        } else {
            this.families.add(family);
        }
    }


    //7. Возвращает количество семей в БД.
    @Override
    public int count(){
        return this.getFamilyCollection().size();
    }


}