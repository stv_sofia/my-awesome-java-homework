package com.javabasic.hw11.app.domain.human;

public enum Gender {
    MAN("man"),
    WOMAN("woman"),
    UNKNOWN ("unknown");

    public String gender;

    Gender(){
        this.gender = "UNKNOWN";
    }

    Gender(String gender){
        this.gender = gender;
    }

    public String getGenderValName() {
        return gender;
    }
}