package com.javabasic.hw11.app.domain.human;

public enum DayOfWeek {
    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday");

    public String nameCapt;

    DayOfWeek(){};

    DayOfWeek(String nameCapt){
        this.nameCapt = nameCapt;
    };

    public String getDayOfWeekValName() {
        return nameCapt;
    }
}
