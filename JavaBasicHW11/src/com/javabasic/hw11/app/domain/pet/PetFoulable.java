package com.javabasic.hw11.app.domain.pet;

public interface PetFoulable {
    String petDidFoul();
}
