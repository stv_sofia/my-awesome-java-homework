/*
Написать консольную программу "планирощик задач на неделю".
Технические требования:
+ Создать двумерный массив строк размерностью 7х2 String[][] schedule = new String[7][2]
!!! Реализавала иначе чем ТЗ !!! >> переопределила массив schedule[7][2] в двумерный зубчатый,
    чтоб удобней было вносить задачи: одна задача - в одной ячейке

+ Заполните матрицу значениями день недели: главное задание на данный день:
scedule[0][0] = "Sunday";
scedule[0][1] = "do home work";
scedule[1][0] = "Monday";
scedule[1][1] = "go to courses; watch a film";

+ Используя цикл и оператор switch, запросите у пользователя ввести день недели в консоль и, в зависимости от ввода:
    + программа: Please, input the day of the week:
    +>> пользователь вводит корректный день недели (f.e. Monday)
    + программа выводит на экран Your tasks for Monday: go to courses; watch a film.; программа идет на следующую итерацию;
    + программа: Please, input the day of the week:
    +>> пользователь вводит некорректный день недели (f.e. %$=+!11=4)
    + программа выводит на экран Sorry, I don't understand you, please try again.; программа идет на следующую итерацию до успешного ввода;
    + программа: Please, input the day of the week:
    +>> пользователь выводит команду выхода exit
    + программа выходит из цикла и корректно завершает работу.
Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).
+ Учтите: программа должна принимать команды как в нижнем регистре (monday) так и в верхнем (MONDAY) и учитывать, что пользователь мог случайно после дня недели ввести пробел.

Необязательное задание продвинутой сложности:
!!! Реализавала иначе чем ТЗ !!! >> исползовала только команду change, а не change [day of the week]
+ Доработайте программу так, чтобы при вводе пользователем фразы change [day of the week] или reschedule [day of the week],
  программа предложила ввести новые задания для данного дня недели и сохранила их в соответствующей ячейке массива,
  удалив при этом старые задания. После чего программа идет на следующую итерацию до ввода exit.
        + программа: Please, input the day of the week:
        +>> пользователь вводит change Monday
        + программа предлагает ввести новые задания на понедельник Please, input new tasks for Monday.
        +>> пользователь вводит новые задания в консоль
        + программа сохраняет новые задачи в соответствующую ячейку
        +программа пошла на новую итерацию и при вводе "Monday" выдает уже новые задания.
 */
package com.javabasic.hw3;

import java.util.Arrays;
import java.util.Scanner;

public class DailyTaskPlanner {
    public static void main(String[] args) {
        System.out.println("It's your DailyTaskPlanner program. Let's begin!!!");
        String[][] schedule = new String[7][2];
        // !!! реализавала иначе чем ТЗ !!! >> переопределяю массив schedule[7][2] в двумерный зубчатый
        setSchedule(schedule);
//        System.out.println(Arrays.deepToString(schedule));

        Scanner scan = new Scanner(System.in);
        String userString = "";
        int indexDay = -1;

        while (true){
            System.out.print("To exit planner, enter: exit || To change daily plan, enter: change\n" +
            "To check daily plan enter a day of the week (Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday) =>> ");
            userString = scan.nextLine().toLowerCase().trim();

            if (userString.equals("exit")) {
                System.out.println("\nYou left the program DailyTaskPlanner. Goodbye!!!");
                break;
            }

            if(userString.equals("change")) {  // !!! реализавала иначе чем ТЗ !!! >> исползовала только команду change
                // а в методе offeringToChangePlan() - запрос на день недели в котором нужно изменить задачи
                offeringToChangePlan(scan, schedule);
            } else {
                indexDay = getIndexOfTheDay(userString);
                if(indexDay == -1) {
                    System.out.println("Sorry, I don't understand you, please try again...\n");
                } else {
                    printPlansForTheDay(indexDay, schedule);
                }
            }
        }
    }

    static void setSchedule(String[][] schedule){
         String [][] basicSchedule = {
                {"Sunday", "to do sleep till 12PM", "to do home work"},
                {"Monday", "to go to courses", "to watch a film"},
                {"Tuesday", "to go swimming"},
                {"Wednesday", "to go to courses", "to do home work"},
                {"Thursday", "to go to the sport club"},
                {"Friday", "to go to the cinema"},
                {"Saturday", "to go to the night club"},
        };
//       переопределяю массив schedule[7][2] в двумерный зубчатый, чтоб удобней было вносить задачи: одна задача - в одной ячейке
         for (int i = 0; i < basicSchedule.length; i++){
             schedule[i] = Arrays.copyOf(basicSchedule[i], basicSchedule[i].length);
         }
    }

    static int getIndexOfTheDay(String day){
        switch (day){
            case "sunday":
                return 0;
            case "monday":
                return 1;
            case "tuesday":
                return 2;
            case "wednesday":
                return 3;
            case "thursday":
                return 4;
            case "friday":
                return 5;
            case "saturday":
                return 6;
            default:
                return -1;
        }
    }

    static void printPlansForTheDay(int indexDay, String[][] schedule){
        System.out.println("Your tasks for " + schedule[indexDay][0] + ": ");
        for(int i = 1; i < schedule[indexDay].length; i++){
            if(i == schedule[indexDay].length -1) {
                System.out.print(i + ". " + schedule[indexDay][i] + "\n\n");
            } else {
                System.out.print(i + ". " + schedule[indexDay][i] + " || ");
            }
        }
    }

    static void offeringToChangePlan(Scanner scan, String[][] schedule){
        String userDay = "";
        String userPlan = "";
        int indexDay, indexOfNewPlan = 1;
        System.out.print("\nEnter the day of a week you'd like to change your plan for =>> ");
        userDay = scan.nextLine().toLowerCase().trim();
        indexDay = getIndexOfTheDay(userDay);
        if(indexDay == -1){
            System.out.println("Input Error! No such day!");
        } else {
            while(true){
                System.out.print("To exit changing plans, enter: exit || \n" +
                        "To change plan: enter your number" + indexOfNewPlan + " task for " + schedule[indexDay][0] + " =>> ");
                userPlan = scan.nextLine().toLowerCase().trim();
                if(userPlan.equals("exit")) {
                    System.out.println("You left changing of plans.\n");
                    break;
                }
                if(userPlan != null && !userPlan.trim().isEmpty()){
                    setNewPlan(indexDay, indexOfNewPlan, userPlan, schedule);
                    indexOfNewPlan++;
                }
            }
        }
    }

    static void setNewPlan(int indexDay, int indexOfNewPlan, String newPlan, String[][] schedule){
        String[] newTaskForTheDay = new String[indexOfNewPlan+1];
        newTaskForTheDay[0] = schedule[indexDay][0];
        if(indexOfNewPlan == 1) {
            newTaskForTheDay[1] = newPlan;
        } else {
            for(int j = 1; j < schedule[indexDay].length; j++){
                newTaskForTheDay[j] = schedule[indexDay][j];
            }
            newTaskForTheDay[indexOfNewPlan] = newPlan;
        }
        schedule[indexDay] = Arrays.copyOf(newTaskForTheDay, indexOfNewPlan+1);
//      System.out.println(Arrays.deepToString(schedule[indexDay]));
    }
}