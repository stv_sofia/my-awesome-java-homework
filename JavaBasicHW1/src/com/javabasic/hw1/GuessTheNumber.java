/*
Написать программу "числа", которая загадывает случайное число и предлагает игроку его угадать.

Технические требования:
+ С помощью java.util.Random программа загадывает случайное число в диапазоне [0-100]
+ предлагает игроку через консоль ввести свое имя, которое сохраняется в переменной name.
+ Перед началом на экран выводится текст: Let the game begin!.
+ Сам процесс игры обрабатывается в бесконечном цикле.
Игроку предлагается ввести число в консоль, после чего
+ программа сравнивает загаданное число с тем, что ввел пользователь.
  Если введенное число меньше загаданного,
+ то программа выводит на экран текст: Your number is too small. Please, try again..
  Если введенное число больше загаданного,
+ то программа выводит на экран текст: Your number is too big. Please, try again..
  Если введенное число соответствуют загаданному,
+ то программа выводит текст: Congratulations, {name}!.
Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).

Необязательное задание продвинутой сложности:
+ Перед переходом на следующую итерацию, программа сохраняет введенное пользователем число в массив.
+ После того как игрок угадал загаданное число, перед выходом программа выводит на экран текст:
Your numbers:  и показывает все введенные игроком числа, отсортированные от большего к меньшему.
+ После ввода чисел пользователем добавить проверку их корректности. Если пользователь ввел не число - спросить заново.
*/
package com.javabasic.hw1;
import java.util.Scanner;                                                                     //import java.util.Arrays;

public class GuessTheNumber {
    public static void main(String[] args) {
        System.out.println("GUESS THE NUMBER! Let the game begin!");
        Scanner scan = new Scanner(System.in);
        String playerStr = "";
        String playerName = "";
        System.out.print("Enter your name, please =>> ");
        playerName = scan.nextLine();
        int i = 0;
        int [] numbersArray= new int[101];
        int randNumber = (int) (Math.random() * 101);
        System.out.println("Must be deleted! !FOR TEST ONLY! randNumber: " + randNumber + "\n");

        while (true) {
            System.out.print("To exit: enter exit || To see your entered numbers: enter numbers ||\n" +
                    "To play: enter an integer [0 to 100] =>> ");                                              // if(!scan.hasNextInt(playerStr)){ continue; }
            playerStr = scan.nextLine().toLowerCase();

            if (playerStr.equals("exit")) {
                System.out.println("Goodbye, " + playerName + "!!!");
                break;
            }

            if (playerStr.equals("numbers")) {
                if (i > 0) {
                    printArray(i, numbersArray);
                } else {
                    System.out.println("You didn't enter any number yet!");
                }
                continue;
            }

            if (!playerStr.matches("\\d+") || playerStr.length() > 3) {
                printWrongEnter();
                continue;
            }

            int playerNumber = Integer.parseInt(String.valueOf(playerStr));
            if (playerNumber > 100) {
                printWrongEnter();
                continue;
            }

            addNumberToArray(i, playerNumber, numbersArray);
            i++;

            if (playerNumber == randNumber) {
                System.out.print("\nArray after sorting (100 =>> 0) >>> ");
                bubbleSort(i, numbersArray);
                printArray(i, numbersArray);                                                                   //System.out.println("Your numbers: " + Arrays.toString(numbersArray));
                System.out.println(String.format("Congratulations, %s! You guess the number %d on the %d try!",
                        playerName, playerNumber, i));
                break;
            } else if (playerNumber > randNumber) {
                System.out.println(String.format("%d is too BIG. Please, try again..", playerNumber));
            } else {
                System.out.println(String.format("%d is too SMALL. Please, try again..", playerNumber));
            }
        }

    }

    static void printWrongEnter() {
        System.out.println("Wrong! Please enter an integer from 0 to 100!!!");
    }

    static void addNumberToArray(int i, int num, int[] array) {
        array[i] = num;
    }

    static void printArray(int length, int[] intArray) {
        System.out.print("\nYour numbers: [");
        for (int i = 0; i < length; i++) {
            if (i == length - 1) {
                System.out.println(intArray[i] + "]");
            } else {
                System.out.print(intArray[i] + ", ");
            }
        }
    }

    static void bubbleSort(int length, int[] intArray) {
        int temp = 0;
        for (int i = 0; i < length; i++) {
            for (int j = 1; j < (length - i); j++) {

                if (intArray[j - 1] < intArray[j]) {
                    temp = intArray[j - 1];
                    intArray[j - 1] = intArray[j];
                    intArray[j] = temp;
                }
            }
        }
    }

}
