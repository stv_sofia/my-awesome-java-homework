/*
Написать программу "стрельба по площади".

Технические требования:
+ Дан квадрат 5х5, где программа случайным образом ставит цель.
+ Перед началом игры на экран выводится текст: All set. Get ready to rumble!.
+ Сам процесс игры обрабатывается в бесконечном цикле.
+ Игроку предлагается ввести линию для стрельбы; программа проверяет что бьло введено число,
  и введенная линия находится в границах игрового поля (1-5).
  В случае, если игрок ошибся предлагает ввести число еще раз.
+ Игроку предлагается ввести столбик для стрельбы (должен проходить аналогичную проверку).
+ После каждого выстрела необходимо отображать обновленное игровое поле в консоли.
  Клетки, куда игрок уже стрелял, необходимо отметить как *.
+ Игра заканчивается при поражении цели. В конце игры вывести в консоль фразу You have won!,
  а также игровое поле. Пораженную цель отметить как x.
Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).

Пример вывода в консоль:
 0 | 1 | 2 | 3 | 4 | 5 |
 1 | - | - | - | - | - |
 2 | - | * | * | - | - |
 3 | * | - | - | * | - |
 4 | - | - | - | - | * |
 5 | * | - | * | - | - |

Необязательное задание продвинутой сложности:
+ Доработайте "стрельбу по площади" так, чтобы "цель" занимала 3 клетки по горизонтали (или вертикали),
а пораженные отсеки отмечались x.
*/

package com.javabasic.hw2;
import java.util.Scanner;

public class ShootingArea {
    public static void main(String[] args) {
        System.out.println("HIT 3 CELLS (horizontally). All set. Get ready to rumble!");
        Scanner scan = new Scanner(System.in);
        int randI = 1 + (int) (Math.random() * 5);
        int randJ = 2 + (int) (Math.random() * 3);
        System.out.print("Must be deleted! !FOR TEST ONLY! random row " + randI + " , random column " + randJ);
        String playerStr = "";
        int userI, userJ, hitCounter = 0;
        int length = 6;
        char[][] shootingArea = new char[length][length];;
        setMatrix(shootingArea, length);
        printArray(shootingArea, length);

        while (true){
            System.out.println();
            printMessageToEnter("row");
            playerStr = scan.nextLine().toLowerCase();
            if (exitGame(playerStr)) {
                break;
            }
            if(!isValidString(playerStr)){
                printErrorMessage();
                continue;
            }

            userI = Integer.parseInt(String.valueOf(playerStr));
            if(!isValidNumber(userI)){
                printErrorMessage();
                continue;
            }

            printMessageToEnter("column");
            playerStr = scan.nextLine().toLowerCase();
            if (exitGame(playerStr)) {
                break;
            }
            if(!isValidString(playerStr)){
                printErrorMessage();
                continue;
            }

            userJ = Integer.parseInt(String.valueOf(playerStr));
            if(!isValidNumber(userJ)){
                printErrorMessage();
                continue;
            }

            System.out.println("Your shoot: row " + userI + ", column " + userJ);
            hitCounter = setShootOnMatrix(shootingArea, userI, userJ, randI, randJ, hitCounter);

            if (hitCounter == 3){
                printArray(shootingArea, length);
                System.out.print("\nYOU HAVE WON! GAME OVER!");
                break;
            }
            printArray(shootingArea, length);
        }
    }

    static void printMessageToEnter(String str){
        System.out.print(String.format("To exit: enter exit || To play: enter a number of %s [1 to 5] =>> ", str));
    }

    static void setMatrix(char[][] matrix, int length) {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if(i == 0){
                    matrix[i][j] = (char)(j+'0');
                } else if(j == 0){
                    matrix[i][j] = (char)(i+'0');
                } else {
                    matrix[i][j] = '-';
                }
            }
        }
    }

    static boolean exitGame(String str){
        if (str.equals("exit")) {
            System.out.println("Exit game. Goodbye!!!");
            return true;
        }
        return false;
    }

    static void printErrorMessage(){
        System.out.println("Input Error!");
    }

    static boolean isValidString(String str){
        if (str.matches("\\d+") || str.length() == 1) {
           return true;
        }
        return false;
    }

    static boolean isValidNumber(int numb){
        if(numb < 1 || numb > 5){
            return false;
        }
        return true;
    }

    static int setShootOnMatrix(char[][] matrix, int userI, int userJ, int randI, int randJ, int hitCounter) {
        if(userI == randI && userJ == randJ
                || userI == randI && userJ == randJ+1
                || userI == randI && userJ == randJ-1){
            matrix[userI][userJ] = 'X';
            hitCounter++;
            System.out.print("Congratulations! You hit the target!");
        } else {
            matrix[userI][userJ] = '*';
            System.out.print("Missed!");
        }
        return hitCounter;
    }

    static void printArray(char[][] matrix, int length) {
        System.out.println();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                System.out.print(matrix[i][j] + " | ");
            }
            System.out.println();
        }
    }
}